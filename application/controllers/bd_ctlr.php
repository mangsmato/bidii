<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Bd_ctlr extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}
	function index(){
     $data['loan_types'] = $this->bd_model->get_loans();
		$this->load->view('heda',$data);
    $this->load->view('loan_app/applicant',$data);
    // $this->session->sess_destroy();
    // $this->db->empty_table('clicks'); 

	}
  function end(){
     $data['loan_types'] = $this->bd_model->get_loans();
    $this->load->view('heda',$data);
    $this->load->view('loan_app/applicant',$data);
    $this->session->sess_destroy();
    $this->db->empty_table('clicks'); 

  }
	function repay(){
		$this->load->view('heda');
		$this->load->view('loan_app/repayment');
	}
	function rates(){
		$this->load->view('heda');
		$this->load->view('loan_app/rates');
	}
	function applicant(){
    $data['loan_types'] = $this->bd_model->get_loans();
		$this->form_validation->set_rules('search', 'Search Field', 'required|xss_clean|trim');
		if ($this->form_validation->run() == FALSE) {
			// $this->index();
      $this->load->view('heda',$data);
    $this->load->view('loan_app/applicant',$data);
		}
		else{       
			$data['applicant'] = $this->bd_model->applicant();
      if ($data['applicant'] ) {
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
        $data['error'] = 'No record found';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
			
       
		}
	}
    function load_refs(){
       $data['loan_types'] = $this->bd_model->get_loans();
      $applicant = $this->session->userdata('applicant');
      $member_no = $applicant['member_no'];
      if (empty($member_no)) {

        $data['error'] = 'unable to complete the request, loan applicant must be selected';
        $data['tab'] = 'applicant';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
      $data['tab'] = 'referees';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant',$data);
    }
    }
  function g1(){
     $data['loan_types'] = $this->bd_model->get_loans();
    $loan_session = $this->session->userdata('loan_session');
    $applicant = $this->session->userdata('applicant');
      $member_no = $applicant['member_no'];
      if (empty($member_no) || empty($loan_session)) {
        $data['g_error'] = 'unable to complete the request, ensure previous steps are completed successfully';
        $data['tab'] = 'guarantors';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{

    $this->form_validation->set_rules('search', 'Search Field', 'required|xss_clean|trim');
    if ($this->form_validation->run() == FALSE) {
      
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
    }
    else{
      $search = $this->input->post('search');
      $data['guarantor1'] = $this->bd_model->guarantor($search);
      $gua1 = $data['guarantor1'] ;

      if (is_array($gua1)) {
        foreach ($gua1 as $row) {
        $sess_array1  = array(
          'name' => $row->fname ." ". $row->lname,
            'employer' => $row->employer,
            'department' => $row->department,
            'address' => $row->address,
            'county' => $row->county,
            'sub_county' => $row->sub_county,
            'id_no' => $row->id_no,
            'pf_no' => $row->pf_no,
            'g_no' => $row->member_no,
            'shares' => $row->shares
          );
        }
        $this->session->set_userdata('gua1', $sess_array1);
      }
      else{
        $data['g_error'] = 'No record found';
      }
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
    }
  }
  }
  function g2(){
     $data['loan_types'] = $this->bd_model->get_loans();
    $loan_session = $this->session->userdata('loan_session');
    $applicant = $this->session->userdata('applicant');
      $member_no = $applicant['member_no'];
      if (empty($member_no) || empty($loan_session)) {
        $data['g_error'] = 'unable to complete the request, ensure previous steps are completed successfully';
        $data['tab'] = 'guarantors';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
    $this->form_validation->set_rules('g1_search', 'Search Field', 'required|xss_clean|trim');
    if ($this->form_validation->run() == FALSE) {
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
    }
    else{
      $g1_search = $this->input->post('g1_search');
      
      $data['guarantor2'] = $this->bd_model->guarantor($g1_search);
      $gua2 = $data['guarantor2'] ;
      if (is_array($gua2)) {
        foreach ($gua2 as $row) {
        $sess_array2  = array(
          'name' => $row->fname ." ". $row->lname,
            'employer' => $row->employer,
            'department' => $row->department,
            'address' => $row->address,
            'county' => $row->county,
            'sub_county' => $row->sub_county,
            'id_no' => $row->id_no,
            'pf_no' => $row->pf_no,
            'g_no' => $row->member_no,
            'shares' => $row->shares
          );
        }
        $this->session->set_userdata('gua2', $sess_array2);
      }
      else{
        $data['g_error'] = 'No record found';
      }
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
      
      }
    }
  }
  function g3(){
     $data['loan_types'] = $this->bd_model->get_loans();
    $loan_session = $this->session->userdata('loan_session');
    $applicant = $this->session->userdata('applicant');
      $member_no = $applicant['member_no'];
      if (empty($member_no) || empty($loan_session)) {
        $data['g_error'] = 'unable to complete the request, ensure previous steps are completed successfully';
        $data['tab'] = 'guarantors';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
    $this->form_validation->set_rules('g2_search', 'Search Field', 'required|xss_clean|trim');
    if ($this->form_validation->run() == FALSE) {
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
    }
    else{
      $g2_search = $this->input->post('g2_search');
      
      $data['guarantor3'] = $this->bd_model->guarantor($g2_search);
      $gua3 = $data['guarantor3'] ;
      if (is_array($gua3)) {
        foreach ($gua3 as $row) {
        $sess_array3  = array(
          'name' => $row->fname ." ". $row->lname,
            'employer' => $row->employer,
            'department' => $row->department,
            'address' => $row->address,
            'county' => $row->county,
            'sub_county' => $row->sub_county,
            'id_no' => $row->id_no,
            'pf_no' => $row->pf_no,
            'g_no' => $row->member_no,
            'shares' => $row->shares
          );
        }
        $this->session->set_userdata('gua3', $sess_array3);
      }
      else{
        $data['g_error'] = 'No record found';
      }
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
      }
    }
  }
  function witness(){
     $data['loan_types'] = $this->bd_model->get_loans();
    $loan_session = $this->session->userdata('loan_session');
    $applicant = $this->session->userdata('applicant');
      $member_no = $applicant['member_no'];
      if (empty($member_no) || empty($loan_session)) {
        $data['g_error'] = 'unable to complete the request, ensure previous steps are completed successfully';
        $data['tab'] = 'guarantors';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
    $this->form_validation->set_rules('witness', 'Search Field', 'required|xss_clean|trim');
    if ($this->form_validation->run() == FALSE) {
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
    }
    else{
      $wit_name = $this->input->post('witness');
      
      $data['witness'] = $this->bd_model->guarantor($wit_name);
      $wit = $data['witness'] ;
      if (is_array($wit)) {
        foreach ($wit as $row) {
        $sess_wit  = array(
          'name' => $row->fname ." " . $row->lname,
          // 'wit_lname' => $row->lname,
           'w_no' => $row->member_no
          );
        }
        $this->session->set_userdata('witness_session', $sess_wit);
      }
      else{
        $data['g_error'] = 'No record found';
      }
      $data['tab'] = 'guarantors';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant', $data);
    }
  }
  }
	function complete(){
	    $this->load->model('bd_model');
	    if (isset($_GET['term'])){
	      $q = strtolower($_GET['term']);
	      $this->bd_model->complete($q);
	    }
  	}
 
  
  	function ref_reg(){
       $data['loan_types'] = $this->bd_model->get_loans();

  		$this->form_validation->set_rules('fname','First Name','trim|xss_clean|required|');
  		$this->form_validation->set_rules('lname','Last Name','trim|xss_clean|required|');
  		$this->form_validation->set_rules('county','County','trim|xss_clean|required|');
  		$this->form_validation->set_rules('sub_county','Sub-County','trim|xss_clean|required|');
  		$this->form_validation->set_rules('dob','Date of Birth','trim|xss_clean|required|');
  		$this->form_validation->set_rules('phone','Phone Number','trim|xss_clean|required|is_numerric');
  		$this->form_validation->set_rules('relation','Relationship','trim|xss_clean|required|');
  		$this->form_validation->set_rules('member_no','Member Number','trim|xss_clean');
  		$this->form_validation->set_rules('id_no','ID Number','trim|xss_clean|required|is_numerric');

  		$this->form_validation->set_rules('fname1','First Name','trim|xss_clean|required|');
  		$this->form_validation->set_rules('lname1','Last Name','trim|xss_clean|required|');
  		$this->form_validation->set_rules('county1','County','trim|xss_clean|required|');
  		$this->form_validation->set_rules('sub_county1','Sub-County','trim|xss_clean|required|');
  		$this->form_validation->set_rules('dob1','Date of Birth','trim|xss_clean|required|');
  		$this->form_validation->set_rules('phone1','Phone Number','trim|xss_clean|required|is_numerric');
  		$this->form_validation->set_rules('relation1','Relationship','trim|xss_clean|required|');
  		$this->form_validation->set_rules('member_no1','Member Number','trim|xss_clean');
  		$this->form_validation->set_rules('id_no1','ID Number','trim|xss_clean|required|is_numerric');

  		if ($this->form_validation->run() == FALSE) {
  			$this->index();
  		}
  		else{

  			$insert = $this->bd_model->ref_reg();
  			if ($insert) {
          // $data['loan_types'] = $this->bd_model->get_loans();
  				$data['tab'] = 'loan';
  				$this->load->view('heda');
  				$this->load->view('loan_app/applicant',$data);
  			}
  			else{
  				$data['ref_error'] = "unable to complete the request, ensure previous steps are completed successfully";
          $data['tab'] = 'referees';
  				$this->load->view('heda');
  				$this->load->view('loan_app/applicant', $data);
  			}
  			

  		}
  	}
    function loan_combo(){

      $data['loans'] = $this->bd_model->loan_combo();
      $loans=$data['loans'];
      
      if ($loans) {
         foreach ($loans as $row) {
        $loan = array(
          'rate' => $row->rate,
          'period' => $row->period,
          'loan_type' => $row->type,
          'penalty' => $row->penalty
           );
      }
     
      echo json_encode($loans,true);
      }
      else{
        echo "<script>alert('unable to complete the request, ensure previous steps are completed successfully');</script>";
      }
     
        
    }
    function loan_disp(){
       $data['loan_types'] = $this->bd_model->get_loans();

      $ref1=$this->session->userdata('ref1');
      $ref2=$this->session->userdata('ref2');
      $applicant = $this->session->userdata('applicant');
      $member_no = $applicant['member_no'];
      if (empty($member_no) || empty($ref1) || empty($ref2)) {
        $data['l_insert_error'] = 'unable to complete the request, ensure previous steps are completed successfully';
        $data['tab'] = 'loan';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
        $this->form_validation->set_rules('amt_applied','Last Name','trim|xss_clean|required|is_numeric');

      if ($this->form_validation->run() == FALSE) {
        $data['tab'] = 'loan';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{


          $amt_applied = $this->input->post('amt_applied');
          $pro_fee=$amt_given="";

          $loan_rate = $this->session->userdata('loan_load');
          $rate = $loan_rate['rate'];
          $period = $loan_rate['period'];
          $loan_type = $loan_rate['loan_type'];

          $months = $period/30;
          $installments = ((100+$rate)*$amt_applied)/(100*$months);

          if ($amt_applied>=50000) {
              $pro_fee = 0.01*$amt_applied;
              $amt_given = $amt_applied-$pro_fee;
          }
          else{
               $pro_fee = 0.02*$amt_applied;
               $amt_given = $amt_applied-$pro_fee;
          }
          
           $loan = array(
            'amt_applied' => $amt_applied,
            'pro_fee' => $pro_fee,
            'installments' => $installments,
            'amt_given' => $amt_given
            );
           $this->db->select('loan_type');
           $this->db->distinct();
           $this->db->where('loanee_no' ,$member_no);
           $query = $this->db->get('loans');
           $db_type="";
           if ($query->num_rows()>1) {
              foreach ($query->result() as $row) {
                $db_type[] = $row->loan_type;
              }
              if (($loan_type != $db_type[0]) && ($loan_type != $db_type[1])) {
                 $data['l_insert_error'] = "the types of loans applied exceed the limit, ";
                 $data['l_insert_error'] .= "you are eligible for : " . $db_type[0] ." and ". $db_type[1];
                  $data['tab'] = 'loan';
                  $this->load->view('heda', $data);
                  $this->load->view('loan_app/applicant', $data);
              }
               else{
                  $this->session->set_userdata('loan_session',$loan);

                  $data['tab'] = 'guarantors';
                  $this->load->view('heda', $data);
                  $this->load->view('loan_app/applicant', $data);
               }           
           }
           else{
              $this->session->set_userdata('loan_session',$loan);
              $data['tab'] = 'guarantors';
              $this->load->view('heda', $data);
              $this->load->view('loan_app/applicant', $data);
        }
      }
      }
      
    }
    function gua_reg(){
       $data['loan_types'] = $this->bd_model->get_loans();
        $insert = $this->bd_model->gua_reg();
        if ($insert) {
          $data['tab'] = 'appraisal';
          $this->load->view('heda');
          $this->load->view('loan_app/applicant',$data);
        }
        else{
          $data['g_insert_error'] = "unable to complete the request, ensure previous steps are completed successfully";
          $data['tab'] = 'guarantors';
          $this->load->view('heda');
          $this->load->view('loan_app/applicant', $data);
        }
      
    }
    function load_disburse(){
       $data['loan_types'] = $this->bd_model->get_loans();

      $garanta1 = $this->session->userdata('gua1');
      $garanta2 = $this->session->userdata('gua2');
      $garanta3 = $this->session->userdata('gua3');
      
    if (empty($garanta1) || empty($garanta2) || empty($garanta3)) {
        $appraisal_error = 'unable to complete the request, ensure previous steps are completed successfully';
        $data['tab'] = 'appraisal';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
      $data['tab'] = 'disbursement';
      $this->load->view('heda');
      $this->load->view('loan_app/applicant',$data);
    }
    }
    function save_disburse(){
       $data['loan_types'] = $this->bd_model->get_loans();

      $this->form_validation->set_rules('method','Payment Method','trim|xss_clean|required|');
      $this->form_validation->set_rules('details','Payment Details','trim|xss_clean|');
      $this->form_validation->set_rules('account','Account Number','trim|xss_clean|');
      $this->form_validation->set_rules('officer','Approving Officer','trim|xss_clean|required|');
      $this->form_validation->set_rules('bank','Bank Name','trim|xss_clean|');

      if ($this->form_validation->run() == FALSE) {
        $data['tab'] = 'disbursement';
        $this->load->view('heda');
        $this->load->view('loan_app/applicant', $data);
      }
      else{
        $loan = $this->session->userdata('loan_session');
        $amt_applied = $loan['amt_applied'];
        $pro_fee = $loan['pro_fee'];
        $amt_given = $loan['amt_given'];
        $installments = $loan['installments'];


        $applicant = $this->session->userdata('applicant');
        $member_no = $applicant['member_no'];

        $loan_loaded = $this->session->userdata('loan_load');
        $loan_type = $loan_loaded['loan_type'];

        $method = $this->input->post('method');
        $bank = $this->input->post('bank');
        $account = $this->input->post('account');
        $details = $this->input->post('details');
        $officer = $this->input->post('officer');
        $date = date('Y-m-d H:i:s');
         $tra_type = "loan disbursement";
         $status = "UNPAID";

        $disbursement = array(
          'member_no' => $member_no, 
          'type' => $tra_type,
          'method' => $method, 
          'bank' => $bank,
          'account' => $account,   
          'details' => $details, 
          'officer' => $officer, 
          'date' => $date
        );
        $loan_save = array(
          'loanee_no' => $member_no, 
          'app_date' => $date, 
          'loan_type' => $loan_type, 
          'amount_applied' => $amt_applied, 
          'processing_fee' => $pro_fee,
          'amount_given' => $amt_given,
          'principal' => $amt_applied,
          'installments' => $installments,
          'total' => $amt_applied
          // 'status' => $status
        );
        $save_clicks = array('times' => 1);
        $garanta1 = $this->session->userdata('gua1');
      $garanta2 = $this->session->userdata('gua2');
      $garanta3 = $this->session->userdata('gua3');
       $ref1=$this->session->userdata('ref1');
      $ref2=$this->session->userdata('ref2');
      $witness = $this->session->userdata('witness_session');
       if (empty($applicant) || empty($loan) || empty($loan_loaded) || empty($garanta1) || empty($garanta2) || empty($garanta3) || empty($ref1) || empty($ref2) || empty($witness)) {
          $data['d_insert_error'] = "unable to complete the request, ensure previous steps are compled successfully";
          $data['tab'] = 'disbursement';
          $this->load->view('heda');
          $this->load->view('loan_app/applicant', $data);
       }
       else{
          $query = $this->db->get('clicks');

         if ($query->num_rows()>0) {
          $data['d_insert_error'] = 'information already submitted, <a href="index"> Click Here </a> to start a new application';
          $data['tab'] = 'disbursement';
          $this->load->view('heda');
          $this->load->view('loan_app/applicant', $data);
        }
        else{
        $insert = $this->bd_model->save_disburse($disbursement, $loan_save,$save_clicks);
        if ($insert) {
        //   $disbursement = array(
        //   'member_no' => $member_no, 
        //   'type' => $tra_type,
        //   'method' => $method, 
        //   'bank' => $bank,
        //   'account' => $account,   
        //   'details' => $details, 
        //   'officer' => $officer, 
        //   'date' => $date
        // );
          $dis_ses = array(
            'method' => $this->input->post('method'), 
            'officer' => $this->input->post('officer')
            );
          $this->session->set_userdata('disburs_session',$dis_ses);
          $data['tab'] = 'report';
          $this->load->view('heda');
          $this->load->view('loan_app/applicant',$data);
         
        }
        else{
          $data['d_insert_error'] = "unable to complete the request, ensure previous steps are compled successfully";
          $data['tab'] = 'disbursement';
          $this->load->view('heda');
          $this->load->view('loan_app/applicant', $data);
        }
      }
      }
    }
    }
    public function loanee_pdf()
    {
      $app_name =$loan_type = $period =$date =$officer=$ref1_name =$ref2_name=$gara1_name= $gara2_name=$gara3_name=$wit_name=$method=$report_error = "";
      $installments = $amt_applied =0;
      $applicant = $this->session->userdata('applicant');
      $loan_load = $this->session->userdata('loan_load');
      $loan = $this->session->userdata('loan_session');
      $ref2 = $this->session->userdata('ref2');
      $ref1 = $this->session->userdata('ref1');
      $garanta2 = $this->session->userdata('gua2');
      $garanta1 = $this->session->userdata('gua1');
      $garanta3 = $this->session->userdata('gua3');
      $witness = $this->session->userdata('witness_session');
          
        
              $app_name = $applicant['name'];
              $member_no = $applicant['member_no'];
              $loan_type = $loan_load['loan_type'];
              $period = $loan_load['period'] . " days";
             $amt_applied = "Ksh. " . number_format($loan['amt_applied'],2);
             $installments = "Ksh. " . number_format($loan['installments'],2);
              $date = date('d/m/Y H:i:s');
             

              $ref1_name = $ref1['fname'] . " " . $ref1['lname'];
              $ref2_name = $ref2['fname'] . " " . $ref2['lname'];
             $gara1_name = $garanta1['name'];
             $gara2_name = $garanta2['name'];
             $gara3_name = $garanta3['name'];
             $wit_name = $witness['wit_fname'] . " " .$witness['wit_lname'];

             $data[] = $this->db->query("SELECT * FROM disbursement WHERE member_no = '$member_no' AND date >= CURDATE()");

             $method = $data['method'];
             $officer = $data['officer'];
     
        prep_pdf(); 

      $this->cezpdf->ezText('BIDII WESTERN SACCO', 28, array('justification' => 'center'));
      $this->cezpdf->ezText("MAXIMIZING YOUR INVESTMENT OPPORTUNITIES", 8, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-30);//white space

      $this->cezpdf->ezText("LOAN APPLICATION REPORT", 18, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-20);//white space

      $this->cezpdf->ezText('LOANEE DETAILS', 16, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(-10);
      $this->cezpdf->ezText('Applicant Name', 14, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Member Number ', 14, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Date Applied', 14, array('justification' => 'right'));
      $this->cezpdf->ezSetDy(-5);
      $this->cezpdf->ezText($app_name,11,array('justification' => 'left'));
       $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($member_no, 11, array('justification' => 'center'));      
      $this->cezpdf->ezSetDy(10);
      $this->cezpdf->ezText($date, 11, array('justification' => 'right'));

       $this->cezpdf->ezSetDy(-25);//white space

      $this->cezpdf->ezText('LOAN DETAILS', 16, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(-10);
      $this->cezpdf->ezText('Type of Loan', 14, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Amount Awarded', 14, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Repayment Period', 14, array('justification' => 'right'));
      $this->cezpdf->ezSetDy(-5);
      $this->cezpdf->ezText($loan_type,11,array('justification' => 'left'));
       $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($amt_applied, 11, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($period, 11, array('justification' => 'right'));

       $this->cezpdf->ezText('Monthly Installments', 14, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(15);
      $this->cezpdf->ezText('Method of Payment', 14, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-5);
      $this->cezpdf->ezText($installments,11,array('justification' => 'left'));
       $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($pay_method, 11, array('justification' => 'center'));

      $this->cezpdf->ezSetDy(-25);//white space

      $this->cezpdf->ezText('REFEREES', 16, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(-10);
      $this->cezpdf->ezText('Referee 1', 14, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Referee 2', 14, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-5);
      $this->cezpdf->ezText($ref1_name,11,array('justification' => 'left'));
       $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($ref2_name,11,array('justification' => 'center'));

       $this->cezpdf->ezSetDy(-25);//white space

      $this->cezpdf->ezText('GUARANTORS', 16, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(-10);
      $this->cezpdf->ezText('Guarantor 1', 14, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Guarantor 2', 14, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Guarantor 3', 14, array('justification' => 'right'));
      $this->cezpdf->ezSetDy(-5);
      $this->cezpdf->ezText($gara1_name,11,array('justification' => 'left'));
       $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($gara2_name, 11, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($gara3_name, 11, array('justification' => 'right'));

      $this->cezpdf->ezSetDy(-25);//white space

      $this->cezpdf->ezText('APPROVAL', 16, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(-10);
      $this->cezpdf->ezText('Witness', 14, array('justification' => 'left'));
      $this->cezpdf->ezSetDy(15);//white space
      $this->cezpdf->ezText('Approving Officer', 14, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-5);
      $this->cezpdf->ezText($wit_name,11,array('justification' => 'left'));
       $this->cezpdf->ezSetDy(10);//white space
      $this->cezpdf->ezText($official,11,array('justification' => 'center'));

       $this->cezpdf->ezSetDy(-100);//white space
        $this->cezpdf->line(350,100,250,100);//left margin, bottom, length, top
       $this->cezpdf->ezText('Officer Sign', 14, array('justification' => 'center'));

       $this->cezpdf->ezSetDy(15);//white space
        $this->cezpdf->line(550,100,450,100);//left margin, bottom, length, top
       $this->cezpdf->ezText('Applicant Sign', 14, array('justification' => 'right'));

      $this->cezpdf->ezStream();
      // $this->session->sess_destroy();
      }

     public function print_pdf()
    {
      
      
        prep_pdf(); 

      $this->cezpdf->ezText('BIDII WESTERN SACCO', 16, array('justification' => 'center','bold' => TRUE));
      $this->cezpdf->ezText("MAXIMIZING YOUR INVESTMENT OPPORTUNITIES", 8, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-20);//white space

      $this->db->select("*");
      $this->db->from("loans");
      $this->db->join('members','loans.loanee_no=members.member_no','left');
      $query = $this->db->get();

    if ($query->num_rows()>0) {
       $new_data =array();
      foreach ($query->result() as $row) {
        $status = $row->loan_status;
        if ($status=='PAID') {
          $total = 0.00;
        }
        else{
          $total = $row->total;
        }
        $new_data[] = array(
          'member_no' => $row->member_no,
          'name' => $row->fname ." ". $row->lname,
          'type' =>$row->loan_type,
          'amount_applied' => $row->amount_applied,
          'interest' =>$row->interest,
          'principal' => $row->principal,
          'overdue' =>$row->overdue,
          'penalty' =>$row->penalty,
          'installments' =>$row->paid_installments,
          'balance' =>$total,
          'status' =>$row->loan_status
          );
        
      }
      $col_names = array(
        'member_no' => 'Member No.',
        'name' => 'Name',
        'type' => 'Loan Type',
        'amount_applied' => 'Amount Applied (Ksh)',
        'interest' => 'Interest (Ksh)',
        'principal' => 'Principal (Ksh)',
        'overdue' => 'Overdue (days)',
        'penalty' => 'Penalty (Ksh)',
        'installments' => 'Installments (Ksh)',
        'balance' => 'Total (Ksh)',
        'status' => 'Status'
    );
         $this->cezpdf->ezTable($new_data, $col_names, 'Members with Loans', array('width'=>550));
      $this->cezpdf->ezStream();
      }

    }
    
    public function ros()
    {
     
     $this->load->library('cezpdf');//load library
     $this->load->helper('pdf');
    
    prep_pdf(); 

     $this->cezpdf->ezText('Looking At Bidii System', 12, array('justification' => 'center'));// ezText function displays Title, size, config
     $this->cezpdf->ezSetDy(-20);//white space


     $content = 'Mangoli the coder';

     $this->cezpdf->ezText($content, 10);//ezText function displays ahtever added

     $this->cezpdf->ezStream();//creates pdf and prompts to save or view
   }
      public function tables1()
    {
       $this->load->library('cezpdf');//load library
        $this->load->helper('pdf');
        prep_pdf(); 

      $this->cezpdf->ezText('BIDII WESTERN SACCO', 16, array('justification' => 'center'));
      $this->cezpdf->ezText("MAXIMIZING YOUR INVESTMENT OPPORTUNITIES", 8, array('justification' => 'center'));
      $this->cezpdf->ezSetDy(-20);//white space
      $this->cezpdf->ezText('Applicant Details', 14);
      $this->cezpdf->ezSetDy(-10);
      $this->cezpdf->ezText('Applicant Name: ' . "Mang'oli Martin" , 12);
      $this->cezpdf->ezSetDy(-10);
      $this->cezpdf->ezText('Loan Details', 14);
      $this->cezpdf->ezSetDy(-10);
      $query = $this->db->get('members');

    if ($query->num_rows()>0) {
       $new_data =array();
      foreach ($query->result() as $row) {
        $new_data[] = array(
          'member_no' => $row->member_no,
          'name' => $row->fname ." ". $row->lname,
          'shares' =>$row->shares
          );
        
      }
      $col_names = array(
        'member_no' => 'Member Number',
        'name' => 'Name',
        'shares' => 'Shares'
    );
         $this->cezpdf->ezTable($new_data, $col_names, 'Sacco Members', array('width'=>550));
      $this->cezpdf->ezStream();
      }
    }
   public function tables()
    {
      // 
       
     $this->load->library('cezpdf');//load library

    $db_data[] = array('name' => 'Jon Doe', 'phone' => '111-222-3333', 'email' => 'jdoe@someplace.com');
    $db_data[] = array('name' => 'Jane Doe', 'phone' => '222-333-4444', 'email' => 'jane.doe@something.com');
    $db_data[] = array('name' => 'Jon Smith', 'phone' => '333-444-5555', 'email' => 'jsmith@someplacepsecial.com');

    $col_names = array(
        'name' => 'Name',
        'phone' => 'Phone Number',
        'email' => 'E-mail Address'
    );

    $this->cezpdf->ezTable($db_data, $col_names, 'Contact List', array('width'=>550));//takes data array, asscociative array for column names, title, configs
    $this->cezpdf->ezStream();
    }
    public function loans_report()
    {
      // $data['title'] ='l';
      $this->load->view('heda');
      $this->load->view('loan_app/loans_report');
    }
    public function load_loans()
    {
      $radio = $this->input->post('search');
      $loanee_no = $this->input->post('loanee_no');
      $data['loanees'] =$this->bd_model->loans_report();
      if ($data['loanees']) {
      $total=0.00;
      if ($radio=='all') {
        $query=$this->db->get('loans');
        foreach ($query->result() as $row) {
           $total+=$row->total;
        }
        $data['heading'] = "<br><br><strong>All Loans Borrowed" . "<br>" ."TOTAL LOANS BORROWED:</strong> KSH. " .number_format($total,2) ."<br><br>";
      }
      else if ($radio=='ind') {
        
          $this->db->join('members','loans.loanee_no=members.member_no');
        $query=$this->db->get_where('loans',array('loanee_no' =>$loanee_no));
        foreach ($query->result() as $row) {
           $name=$row->fname . " " . $row->lname;
           $total+=$row->total;
        }
        $data['heading'] = "<br><br><strong>Loans for: </strong>" .strtoupper($name) ."<br>" . "<strong>TOTAL BALANCE:</strong> KSH. " .number_format($total,2) ."</strong><br><br>";
      }
    }
    else{
      $data['error']='no record found for the specified criteria';
    }
       
      $this->load->view('heda');
      $this->load->view('loan_app/loans_report',$data);
    }
     public function disburse_report()
    {
      $this->load->view('heda');
      $this->load->view('loan_app/disburse_report');
    }
    public function dis_report()
    {
        $radio = $this->input->post('search');
      $loanee_no = $this->input->post('loanee_no');
      $data['disburse'] =$this->bd_model->dis_report($radio,$loanee_no);
      if ($data['disburse']) {
      $total=0.00;
      if ($radio=='all') {
        $query=$this->db->get('loans');
        foreach ($query->result() as $row) {
           $total+=$row->total;
        }
        $data['heading'] = "<br><br><strong>All Loans Borrowed" . "<br>" ."TOTAL LOANS BORROWED:</strong> KSH. " .number_format($total,2) ."<br><br>";
      }
      else if ($radio=='ind') {
        
          $this->db->join('members','loans.loanee_no=members.member_no');
        $query=$this->db->get_where('loans',array('loanee_no' =>$loanee_no));
        foreach ($query->result() as $row) {
           $name=$row->fname . " " . $row->lname;
           $total+=$row->total;
        }
        $data['heading'] = "<br><br><strong>Loans for: </strong>" .strtoupper($name) ."<br>" . "<strong>TOTAL BALANCE:</strong> KSH. " .number_format($total,2) ."</strong><br><br>";
      }
    }
    else{
      $data['error']='no record found for the specified criteria';
    }
       
      $this->load->view('heda');
      $this->load->view('loan_app/disburse_report',$data);
    }
}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Home extends CI_Controller

{
	public function __construct()
	 {
		parent::__construct();
		// $this->load->model('insert_model');
		$this->load->helper('url');
		
	}

	function index(){
		$this->load->view('login');
	}
	function register(){
		$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration');
	}
	function updateshares(){

		$this->load->model('insert_model');  
        $deptresult = $this->insert_model->updateshares();           
        $data['deptlist'] = $deptresult;
        $data['title'] = 'update shares';
        $this->load->view('heda',$data);
		$this->load->view('reg/updateshares',$data);
	}

	function membersreport(){
		$this->load->model('insert_model');  
        $deptresult = $this->insert_model->membersreport();           
        $data['deptlist'] = $deptresult;
        $data['title'] = 'members report';
        $this->load->view('heda',$data);
		$this->load->view('reg/membersreport',$data);
	}
	
	function login()
	{
		if($this->session->userdata('logged_in'))
    {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
      $data['title'] = 'Register Member';
        $this->load->view('heda',$data);
      $this->load->view('reg/registration', $data);
    }
    else
    {
      //If no session, redirect to login page
      header('location:login', 'refresh');
	}
		
	}
	
	function save(){


	$this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

 
 	$this->form_validation->set_rules('fname','First name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('mname','Middle name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('lname','Last name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('phone1','Phone number 1', 'trim|required|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[members.phone_no1]');
	$this->form_validation->set_rules('phone2','Phone number 2', 'trim|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[members.phone_no2]');
	$this->form_validation->set_rules('phone3','Phone number 3', 'trim|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[members.phone_no3]');
	
	$this->form_validation->set_rules('email','Email', 'trim|required|xss_clean|valid_email|is_unique[members.email]');
	$this->form_validation->set_rules('address','Address', 'trim|required|xss_clean|min_length[5]|max_length[50]');
	$this->form_validation->set_rules('date1','Date of birth', 'trim|required|xss_clean');
	$this->form_validation->set_rules('id','Id Number', 'trim|required|xss_clean|is_unique[members.id_no]');
	$this->form_validation->set_rules('county','County', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('subcounty','Subcounty', 'trim|required|xss_clean|min_length[3]|max_length[50]');


	if ($this->form_validation->run() == FALSE){

		$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration');

	}
	else {
		
		$succes = $this->insert_model->save();
		if ($succes) {
			$data1['tab'] = 'tab2';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration', $data1);
		}
		else{
			$data['save_error'] = "unable to complete request,try again later";
			$data['tab'] = 'tab1';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration', $data);
		}
	}

	}

	function saveemployeedperson(){

		$this->load->helper(array('form','url'));
	    $this->load->library('form_validation');
	 	
	 	$this->form_validation->set_rules('admission','Date of Admission','trim|required|xss_clean');
		$this->form_validation->set_rules('employer','Employer','trim|required|xss_clean');
		$this->form_validation->set_rules('department','Department','trim|required|xss_clean');
		$this->form_validation->set_rules('pfno','PF.NO','trim|required|xss_clean|is_unique[employed_personnel.pfno]');
		$this->form_validation->set_rules('station','Station','trim|required|xss_clean');

		$this->form_validation->set_rules('address','Address','trim|required|xss_clean|min_length[5]|max_length[50]');
		
		$this->form_validation->set_rules('county','County');
		$this->form_validation->set_rules('subcounty','Subcounty');

		if ($this->form_validation->run() == FALSE){

			$data['tab'] = 'tab2';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration',$data);

		}
		else {
			$member = $this->session->userdata('personal_details');
			$member_no=$member['member_no'];
			if (empty($member_no)) {
			$data['error'] = 'please complete the previous steps';
				$data['tab'] = 'tab2';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
		}

		else{

		$data = array(
		'member_no'=> $member_no,
		'doa'=> $this->input->post('admission'),	
		'employer'=>$this->input->post("employer"),
		'department'=>$this->input->post("department"),
		'pfno'=>$this->input->post("pfno"),
		'station'=>$this->input->post("station"),
		'address'=>$this->input->post("address"),
		'county'=>$this->input->post("county"),
		'subcounty'=>$this->input->post("subcounty"));

		$success = $this->db->insert('employed_personnel',$data);
		if ($success) {
			$this->session->set_userdata('employed',$data);
			$data['tab'] = 'tab3';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration',$data);
		}
		else{
			$data['error'] = 'unable to complete request';
				$data['tab'] = 'tab2';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
		}
		}
			
			
		}
	}

	function nextofkin(){

	$this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
 	
 	// next of kin
 	$this->form_validation->set_rules('fname','First Name','trim|required|xss_clean');
	$this->form_validation->set_rules('mname','Middle Name','trim|required|xss_clean');
	$this->form_validation->set_rules('lname','Last Name','trim|required|xss_clean');
	$this->form_validation->set_rules('id','Id Number','trim|required|xss_clean|is_unique[next_of_kin.id_no]');
	$this->form_validation->set_rules('phone','Phone Number 1', 'trim|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[next_of_kin.phone_no1]');
	$this->form_validation->set_rules('phone1','Phone Number 2', 'trim|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[next_of_kin.phone_no2]');
	$this->form_validation->set_rules('relationship','Relationship','trim|required|xss_clean');

	if ($this->form_validation->run() == FALSE){
		$data1['tab'] = 'tab3';
		$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration', $data1);

	}
	else {
		$member = $this->session->userdata('employed');
		$member_no = $member['member_no'];
		if (empty($member_no)) {
			$data['error'] ="Please Complete the previous Step ";
			$data['tab'] ='tab3';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration',$data);
		}
		else {

			$insert = $this->insert_model->nextofkin();
			if ($insert) {
				$data['tab'] = 'tab4';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
			}
			else{
				$data['error'] = 'Unable to complete the process';
				$data['tab'] = 'tab3';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
			}
		}
		
	}

}
	function shares(){

		$this->load->library('form_validation');
		$this->form_validation->set_rules('search','Search Field','required|xss_clean|trim');
		if ($this->form_validation->run()==FALSE) {
			$data['title'] = 'update shares';
        $this->load->view('heda',$data);
			$this->load->view('reg/updateshares');
			

		}
		else{
			$data['shares1']=$this->insert_model->shares();
			if ($data['shares1']) {
				$data['title'] = 'update shares';
        $this->load->view('heda',$data);
				$this->load->view('reg/updateshares',$data);
				# code...
			}
			else{
			$data['shares_error']="Record not found";
			$data['title'] = 'update shares';
        $this->load->view('heda',$data);
			$this->load->view('reg/updateshares');

			}
		}
	}

	function saveselfemployed(){

		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	 	
	 	$this->form_validation->set_rules('tob','Type of business','trim|required|xss_clean');
		$this->form_validation->set_rules('date1','Startdate','trim|required|xss_clean');
		$this->form_validation->set_rules('phone','Phone Number','trim|required|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[self_employed_persons.phone]');
		$this->form_validation->set_rules('address','Address','trim|required|xss_clean|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('county','County','trim|required|xss_clean');
		$this->form_validation->set_rules('subcounty','Subcounty','trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE){

			$data['tab'] = 'tab2';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration', $data);

		}
		else {
			$data['tab'] = 'tab3';
			$this->insert_model->selfemployed();
			$this->load->view('reg/registration', $data);
		}
	}

	function approvingofficer(){

		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	 	
	 	$this->form_validation->set_rules('fname','First Name','trim|required|xss_clean');
		$this->form_validation->set_rules('mname','Middle Name','trim|required|xss_clean');
		$this->form_validation->set_rules('lname','Last Name','trim|required|xss_clean');
		$this->form_validation->set_rules('designation','Designation','trim|required|xss_clean');
		$this->form_validation->set_rules('date2','Date_approved','trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE){

			$data['tab'] = 'tab7';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration', $data);

		}
		else {
			$insert = $this->insert_model->approvingofficer();
			if ($insert) {
				$data['tab'] = 'tab9';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
			}
			else{
				$data['error6'] = 'Insert data';
				$data['tab'] = 'tab7';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
			}
		}

	}

	function nominee1(){


	$this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

 
 	$this->form_validation->set_rules('fname','First name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('mname','Middle name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('lname','Last name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('phone1','Phone number', 'trim|required|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[nominee.phone_no]');
	$this->form_validation->set_rules('date1','Date of birth', 'trim|required|xss_clean');
	$this->form_validation->set_rules('id','Id Number', 'trim|required|xss_clean|is_unique[members.id_no]');
	$this->form_validation->set_rules('county','County', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('subcounty','Subcounty', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('relationship','Relationship', 'trim|required|xss_clean|min_length[3]|max_length[50]');

	


	if ($this->form_validation->run() == FALSE){

		$data1['tab'] = 'tab4';
		$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration',$data1);

	}
	else {
		$member = $this->session->userdata('nexts_of_kin');
		$member_no = $member['member_no'];
		if (empty($member_no)) {
			$data['nomerror'] = "Please complete the previous steps";
			$data['tab'] = 'tab4';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration',$data);
		}
		else{
			$succes = $this->insert_model->nominee1();
			if ($succes) {
				$data1['tab'] = 'tab5';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data1);
			}
				else{
					$data['nomerror'] = "unable to complete request,try again later";
					$data['tab'] = 'tab4';
					$data['title'] = 'new member';
        $this->load->view('heda',$data);
					$this->load->view('reg/registration', $data);
				}
			}

		}
	

	}

	function nominee2(){


	$this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

 
 	$this->form_validation->set_rules('fname','First name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('mname','Middle name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('lname','Last name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('phone1','Phone number', 'trim|required|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[nominee.phone_no]');
	$this->form_validation->set_rules('date1','Date of birth', 'trim|required|xss_clean');
	$this->form_validation->set_rules('id','Id Number', 'trim|required|xss_clean|is_unique[members.id_no]');
	$this->form_validation->set_rules('county','County', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('subcounty','Subcounty', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('relationship','Relationship', 'trim|required|xss_clean|min_length[3]|max_length[50]');

	


	if ($this->form_validation->run() == FALSE){

		$data1['tab'] = 'tab5';
		$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration',$data1);

	}
	else {
		$member = $this->session->userdata('nominee1');
		$member_no = $member['member_no'];
		if (empty($member_no)) {
			$data['nom2error'] ="Please Fill in the previous content";
			$data['tab'] = 'tab5';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration', $data);
		}
		else{

		$succes = $this->insert_model->nominee2();
		if ($succes) {
			$data1['tab'] = 'tab6';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration', $data1);
		}
		else{
			$data['nom2error'] = "Unable to complete the request";
			$data['tab'] = 'tab5';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration', $data);
		}
	}

	}
}

	function nominee3(){


	$this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

 
 	$this->form_validation->set_rules('fname','First name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('mname','Middle name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('lname','Last name', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('phone1','Phone number', 'trim|required|xss_clean|min_length[10]|max_length[50]|is_numeric|is_unique[nominee.phone_no]');
	$this->form_validation->set_rules('date1','Date of birth', 'trim|required|xss_clean');
	$this->form_validation->set_rules('id','Id Number', 'trim|required|xss_clean|is_unique[members.id_no]');
	$this->form_validation->set_rules('county','County', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('subcounty','Subcounty', 'trim|required|xss_clean|min_length[3]|max_length[50]');
	$this->form_validation->set_rules('relationship','Relationship', 'trim|required|xss_clean|min_length[3]|max_length[50]');

	


	if ($this->form_validation->run() == FALSE){

		$data1['tab'] = 'tab6';
		$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration',$data1);

	}
	else {
		$member = $this->session->userdata('nominee2');
		$member_no = $member['member_no'];
		if (empty($member_no)) {
			$data['nom3error'] = "Please Fill the previous tab";
			$data['tab'] = 'tab6';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration',$data);
		}
		else{
			$succes = $this->insert_model->nominee3();
			if ($succes) {
				$data1['tab'] = 'tab7';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data1);
			}
			else{
				$data['nom3error'] = "Unable to complete the request. Try again";
				$data['tab'] = 'tab6';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
			}
		}
	}

	}
	function save_regfees(){


	$this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

 
 	$this->form_validation->set_rules('method','Payment Methods', 'trim|required|xss_clean');
	$this->form_validation->set_rules('bank','Bank Type', 'trim|required|xss_clean');
	$this->form_validation->set_rules('account','Account number', 'trim|required|xss_clean');
	$this->form_validation->set_rules('details','Payment Details', 'trim|required|xss_clean');
	$this->form_validation->set_rules('officer','Approving Officer', 'trim|required|xss_clean');
	


	if ($this->form_validation->run() == FALSE){

		$data1['tab'] = 'tab7';
		$data['title'] = 'new member';
        $this->load->view('heda',$data);
		$this->load->view('reg/registration',$data1);

	}
	else {
		$member = $this->session->userdata('nominee3');
		$member_no = $member['member_no'];
		if (empty($member_no)) {
			$data['regfee_error'] ="Please Fill in the previous tab";
			$data['tab'] ='tab7';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration',$data);
		}
		else{
			$succes = $this->insert_model->save_regfees();
			if ($succes) {
				$data1['tab'] = 'tab8';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data1);
			}
			else{
				$data['regfee_error'] ="unable to complete the request";
				$data['tab'] = 'tab8';
				$data['title'] = 'new member';
        $this->load->view('heda',$data);
				$this->load->view('reg/registration', $data);
			}
		}
	}
}
	function ushares(){

		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	 	
	 	$this->form_validation->set_rules('shares','Shares', 'trim|required|xss_clean');
	
	

		if ($this->form_validation->run() == FALSE){
			$data['title'] = 'update shares';
        $this->load->view('heda',$data);
			$this->load->view('reg/updateshares');

		}
		else {
			
			$insert = $this->insert_model->ushares();
			if ($insert) {
				$this->load->view('login');
			}
			else{
				$data['shares_error1'] = 'Input shares';
				$data['title'] = 'update shares';
        $this->load->view('heda',$data);
				$this->load->view('reg/updateshares', $data);
			}
		}
	}

	function report(){
		$this->load->library('form_validation');
		if ($this->form_validation->run()==FALSE) {
			$data['tab'] = 'tab5';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration', $data);

		}
		else{
			$data['report'] = $this->insert_model->report();
			$data['tab'] = 'tab5';
			$data['title'] = 'new member';
        $this->load->view('heda',$data);
			$this->load->view('reg/registration', $data);
		}
	}

	// pdf functions

	function memdersreport(){
		$this->load->library('cezpdf');
		$this->load->helper('pdf');

		prep_pdf();




		$this->cezpdf->ezText('BIDII WESTERN SACCO', 30, array('justification' => 'center' ));
		$this->cezpdf->ezSetDy(-10);
		$this->cezpdf->ezStream();
	}

	function saveshares(){

       $member = $this->session->userdata('shares');
		$member_no = $member['member_no'];

		$data =  array(
			'member_no' =>$member_no, 
			'shares' =>$this->input->post('shares')
			);

		$this->db->insert('shares');
	}
	public function password()
	{
		$this->load->view('heda');
		$this->load->view('password');
	}
	public function logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}
}



?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bidii extends CI_Controller {

 function _construct(){
 	parent::_construct();
 }
 function index()
	{
		//$this->load->view('heda');
		//$this->load->view('repayment/loanrepay');

		//$this->listRepayment();
	$result['resy']= $this->repayment_model->listRepayment();

	if ($result) {
		$this->load->view('heda');
		$this->load->view('repayment/loanrepay',$result);
	}
	else{
		$data['error'] = 'no record';
		$this->load->view('heda');
		$this->load->view('repayment/loanrepay',$data);
	}
	}
function rate22(){
	$this->load->view("heda");
	$this->load->view('repayment/rate');
	
}
function next1(){
	$data['tab']= 'loanreport';
	$this->load->view('heda');
	$this->load->view('repayment/loanrepay', $data);
}	

 function rates()
	{	
		$this->load->view("heda");	
		
		$this->form_validation->set_rules('typeloan', 'Type of loan', 'required|xss_clean|trim|is_unique[rates.type]');
		$this->form_validation->set_rules('paymentperiod', 'Payment period', 'required|is_numeric|xss_clean|trim');
		$this->form_validation->set_rules('ratemonthly', 'Percentage rate', 'required|xss_clean|is_numeric|trim');
		$this->form_validation->set_rules('penalty', 'penalty', 'required|is_numeric|xss_clean|trim');
		if ($this->form_validation->run() == FALSE) {			 
		    $data['query'] = $this->db->query('SELECT * FROM rates');
			$this->load->view('heda');		
        $this->load->view('repayment/rate',$data);

			// $this->rate();
			
			

		}
		else{
			$result = $this->repayment_model->insert_rates();
		    if ($result) {
		    $data['query'] = $this->db->query('SELECT * FROM rates');
		    $data['error1']= "added successfully!!";
			// $this->load->view("heda");
			// $this->load->view('repayment/rate',$data);
		    $this->load->view("heda");	
			$this->load->view("repayment/rate",$data);
			
						
		}
		else{
			$data['error']= "Please confirm your entries!!";
			redirect("rate", $data);
				
		}
		}	
			
	}
		
function repay_loan()
	{	
		
		$this->form_validation->set_rules('amountgiven', 'Amount given', 'required|is_numeric|xss_clean|trim');
		$numb = $this->session->userdata('member');
        $member_no = $numb['loanee_no'];
		
		if ($this->form_validation->run() == FALSE) {

			 // redirect('updateloan');
			 $this->load->view("heda");
			 $this->load->view('repayment/loanrepay');

		}
		else{
			if ($member_no == " ") {
			$data['success']= "No member is selected";
		   			// redirect('index', $data);
		   	$this->load->view("heda",$data);
			$this->load->view('repayment/loanrepay', $data);	
			}
			else{
				$result = $this->repayment_model->repay_loan();
		    if ($result) {
		   			$data['success']= "successfully added";
		   			// redirect('index', $data);
		   			$this->load->view("heda",$data);
					$this->load->view('repayment/loanrepay', $data);

		}
		else{
			$data['success']= "Please correct the required fields";
			$this->load->view("heda");
			$this->load->view('repayment/loanrepay', $data);
			
		}
			}
			
		}	
			
	}
		

function repaymentdetails(){
	
		$this->form_validation->set_rules('search1', 'Search Field', 'required|xss_clean|trim');
		if ($this->form_validation->run() == FALSE) {
			$this->index();
		}
		else{
			$loanee_no = $this->input->post('search1');
			$data['loans_taken'] = $this->repayment_model->repaymentdetails($loanee_no);
			$success = $data['loans_taken'];
			// $this->repcombo(); 
			if ($success) {
				$data['error']= "Record  found!!";
				// $data['tab'] = 'loanrepay';
				$this->load->view('heda');
			    $this->load->view('repayment/loanrepay', $data);
			
			   
                			    	    
		        }				
			else{
				$data['error']= "Record not found!!";
				// $data['tab'] = 'loanrepay';
				$this->load->view('heda');
				$this->load->view('repayment/loanrepay',$data);
			}
		}
			
		
	}


	function loan_combo1(){

      $data['loans'] = $this->repayment_model->loan_combo1();
      $loans = $data['loans'];
      
      if ($loans) {
         foreach ($loans as $row) {
        $loan = array(
          'loanee_no' => $row->loanee_no,
          'date' => $row->date,
          'amount' => $row->amount,
          'balance' => $row->balance,
          'status' => $row->loan_status
           );
      }
     
      echo json_encode($loans,true);
      }
      else{
        echo "<script>alert('unable to complete the request, ensure previous steps are completed successfully');</script>";
      }
     
        
    }




	function repaymentsearch(){
	
		$this->form_validation->set_rules('search', 'Search Field', 'required|xss_clean|trim');
		if ($this->form_validation->run() == FALSE) {
			$this->report();
		}
		else{
			$loanee_no = $this->input->post('search');
			$data['loans_taken'] = $this->repayment_model->repaymentdetails($loanee_no);
			$success = $data['loans_taken'];
			$this->repcombo(); 
			if ($success) {
				$data['error']= "Record  found!!";
				// $data['tab'] = 'loanrepay';
				$this->load->view('heda');
			    $this->load->view('repayment/repayment_report', $data);
			
			   
                			    	    
		        }				
			else{
				$data['error']= "Record not found!!";
				$this->load->view('heda');
				$this->load->view('repayment/repayment_report',$data);
			}
		}
			
		
	}
function repcombo(){
	$numb = $this->session->userdata('memeber');
    $loanee_no = $numb['loanee_no'];
	$data['loans_given']= $this->repayment_model->repcombo($loanee_no);
	$given  = $data['loans_given'];
	if ($given) {
		 $data['y'] = "iko!";
		 echo $data;
		// $this->load->view('repayment/loanrepay', $data);
	}
}

function dynamic_combobox(){
     // retrieve the album and add to the data array
     
        
   }

function update_details(){
	$this->form_validation->set_rules("amountgiven1", "amountgiven1", "required|is_numeric|xss_clean|trim");
	$this->form_validation->set_rules("member_no" ,'' , "required|is_numeric|xss_clean|trim");
	if ($this->form_validation->run()==FALSE) {
		$this->index();
	}
	else{
		$data1["update_details"]=$this->repayment_model->update_details();
		if($data1){

		$this->loan->view("heda");
		$this->load->view("repayment/loanrepay", $data1);
		}
		else{
			redirect('bidii');
		}
		
	}
}
// public function rate(){
// 		$data['clearedstudents']=$this->repayment_model->clearedstudents();
// 		$result=$data['clearedstudents'];
		
// 		if ($result) {
// 			# code...   
// 		$this->load->view('heda');
// 		$this->load->view('repayment/rate',$data);
// 		}
// 		else{
// 		 echo '<script type="text/javascript">alert("its not working pls");</script>';	 
// 		}
// 	}
public function rate(){
		$data['query'] = $this->db->query('SELECT * FROM rates');

		$this->load->view("heda");
        $this->load->view('repayment/rate',$data);
        	
		
	}

public function report(){
	$this->load->view("heda");
	$this->load->view('repayment/repayment_report');
}
public function rat(){
	$this->load->view("heda");
	$this->load->view('repayment/updateloan1');
}

public function rep(){
$numb = $this->session->userdata('member');
$loanee_no = $numb['loanee_no'];	

	if ($loanee_no=="") {

		$list['my'] = $this->load->repayment_model->rep22();
		$this->load->view('heda');
		$this->load->view('repayment/loanrepay',$list);	
	}
	else{
		
		$list['my'] = $this->repayment_model->rep();
		$this->load->view('heda');
		$this->load->view('repayment/loanrepay',$list);
		
	     	}
}
public function listRepayment(){
	$result= $this->load->repayment_model->listRepayment();

	if ($result) {
		$this->load->view('heda');
		$this->load->view('repayment/loanrepay',$result);
	}
	else{
		$data['error'] = 'no record';
		$this->load->view('heda');
		$this->load->view('repayment/loanrepay',$data);
	}
}

public function update_pay($id){
		$data['update']=$this->repayment_model->update_pay($id);
		$data['tab'] = $this->repayment_model->list_individual($id);
		$update = $data['update'];
		$table = $data['tab'];
		if ($update and $table) {
			$myid = array(
				'id' =>$id );

			$this->session->set_userdata('myid',$myid);

			$this->load->view('heda');
			$this->load->view('repayment/i_payment',$data);
		}else{
			$data["error"]="no record please confirm..";
			$this->load->view('heda');
			$this->load->view('repayment/i_payment',$data);
		}
	}

public function all_report(){

		$result['resy']= $this->repayment_model->listRepayment();

	if ($result) {
		$this->load->view('heda');
		$this->load->view('repayment/allReport',$result);
	}
	else{
		$data['error'] = 'no record';
		$this->load->view('heda');
		$this->load->view('repayment/allReport',$data);
	}
	}


public function save_payment(){

$this->form_validation->set_rules('amountgiven', 'given Amount', 'required|xss_clean|trim|is_numeric');
$this->form_validation->set_rules('paymentmethod', 'method of payment', 'required|xss_clean|trim');
    $ide=$this->session->userdata('myid');
    $id=$ide['id'];
    $data['update']=$this->repayment_model->update_pay($id);
	$data['tab'] = $this->repayment_model->list_individual($id);

if ($this->form_validation->run() == FALSE) {
	// $data['error'] = "error on saving";
	
		$this->load->view('heda');
		$this->load->view('repayment/i_payment',$data);
	}
	else{
		$data['bidii'] = $this->repayment_model->save_payment();
		$r= $data['bidii'];
		if ($r) {
			$data['success'] = "Save successfully";

		//$this->load->view('heda');
		//$this->load->view('repayment/i_payment',$data);

		$data1['wewe'] = $this->repayment_model->payment_recipt();
		$yu=$data1['wewe'];
	if ($yu) {
	 	$this->load->view('heda');
		$this->load->view('repayment/receipt',$data1);
	 } 

		}
		else{
		$data['error'] = "error on saving";

		$this->load->view('heda');
		$this->load->view('repayment/i_payment',$data);	
		}
	}	

}

 public function search(){
		$this->form_validation->set_rules('searchfield', 'Search ', 'required|xss_clean|trim');
		if ($this->form_validation->run() == FALSE) {

			
			
			$this->load->view("heda");
            $this->load->view('repayment/updateloan1');
		}
		else{

			$search = $this->input->post('searchfield');		

			$data['found'] = $this->repayment_model->search($search);
			$success = $data['found'];
			 
			if ($success) {
				
				$data['error']= "record found preceed";
			
				$this->load->view("heda");
            $this->load->view('repayment/updateloan1');
			}
							
			else{
				$data['error']= "No record found.";
			
				$this->load->view("heda");
                $this->load->view('repayment/updateloan1');		
			}
		}
			
	}

	public function update_loans(){
		$this->form_validation->set_rules('amountgiven', 'Amount given ', 'required|xss_clean|trim');
		if ($this->form_validation->run() == FALSE) {		
			
			$this->load->view("heda");
            $this->load->view('repayment/updateloan1');
		}
		else{

			$data['saved'] = $this->repayment_model->update_loans();
			$success = $data['saved'];
			 
			if ($success) {
				
				$data['error']= "Updated successfully...";
			
				$this->load->view("heda");
                $this->load->view('repayment/new_loan');
			}
							
			else{
				$data['error']= "Error occured.";
			
				$this->load->view("heda");
                $this->load->view('repayment/updateloan1',$data);		
			}
		}

	}

}
?>
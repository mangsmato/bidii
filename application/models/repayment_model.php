<?php
class Repayment_model extends CI_Model{
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function loanrepay(){
        $pf_no = $this->input->post('search');
        $this->db->select('*');
        $this->db->where('loanee_no',$pf_no);
        
    $query=$this->db->get("repayment");

    if($query->num_rows()>0){
        foreach ($query->result() as $row) {
            
            $data[]=$row;

          $arr = array(
                    'loanee_no'=>$row->pfno,
                    'total'=>$row->total
                    );                 
        }
        
         $this->session->set_userdata("member", $arr);            
        return $data;
    }
    else{
        return 0;
    }
}
public function rep22(){
    $this->db->select('*');
    $this->db->from('repayment');

    $query= $this->db->get();
    if ($query->num_rows()> 0) {
        foreach ($query->result() as $row) {
            $data[]=$row;
        }
        return true;
       }
       else{ 
        return FALSE;
    }
}
function repaymentdetails($loanee_no){
        
        $this->db->select('loan_type');
        $this->db->distinct();
        $this->db->where('loanee_no',$loanee_no);
        
    $query=$this->db->get("loans");

    if($query->num_rows()>0){
        foreach ($query->result() as $row) {
            
            $data[]=$row;

        //             $arr = array(
        //             'loanee_no' => $row->loanee_no,
        //             'total' => $row->total
        //             // 'loan_type'=>$row->loan_type
        //             );                 
         }
        //  $this->session->set_userdata("member", $arr);            
        return $data;
    }
    else{
        return 0;
    }
}

public function loan_combo1(){

$loan_ses=$this->session->userdata("member");
if ($loan_ses) {
    $loanee_no = $loan_ses['loanee_no'];
    $loan_type = $this->input->post("loan_type");
    $this->db->where('loan_type',$loan_type) .where('loanee_no',$loanee_no);
    $query = $this->db->get("repayment");
if ($query->num_rows()>0) {
    foreach ($query->result as $row) {
        $data[] = $row;
    }
    return $data;
}
else{
    return false;
}
}
else{
    return false;
}

}

function repcombo($loanee_no){
        
    
        $this->db->select('loan_type');
        $this->db->distinct();
        $this->db->where('loanee_no',$loanee_no);
        
    $query=$this->db->get("loans");

    if($query->num_rows()>0){
        foreach ($query->result() as $row) {
            
            $data[]=$row;
                     
        }                 
        return $data;
    }
    else{
        return 0;
    }
}

function getloantype() {
        $data = array();
        $query = $this->db->get('repayment');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row){
                    $data[] = $row;
                }
        }
        $query->free_result();
        return $data;
    }


    function insert_details(){
    
    $this->name=$_POST["name"];
    $this->dtaken=$_POST["dtaken"];
    $this->lpaid=$_POST["lpaid"];
    $this->typeloan=$_POST["typeloan"];
    $this->accumloan=$_POST["accumloan"];
    $this->principalloan=$_POST["principalloan"];
    $this->currentinterest=$_POST["currentinterest"];
    $this->overdueduration=$_POST["overdueduration"];
    $this->totalamountpayable=$_POST["totalamountpayable"];
    $this->amountgiven=$_POST["amountgiven"];
    $this->paymentmethod=$_POST["paymentmethod"];
    $this->paymentdetails=$_POST["paymentdetails"];
    $this->balance=$_POST["balance"];

    $this->db->insert("repaymentdetails",$this);
    }

    function listRepayment(){

        // $stud=$this->session->userdata('studii');
        // $Reg_no = $stud['Reg_no'];      
     
        $this->db->select("*");
        $this->db->from('loans');        
        $query = $this->db->get();

        if($query->num_rows()>0){ 
         foreach ($query->result() as $row) {
            $data[]=$row;
        }
    
    return $data;
    }
    else{
        return 0;
    }
        
    }

    function update_details(){
    $pfno1=$this->input->post("member_no");
    $amount=$this->input->post("amountgiven1");
    $date=Now();  
    
        $data = array(
          'totalamountpayable'=>$amount,

            );
        $this->db->where('pfno' , $pfno1);
        $this ->db->update('repaymentdetails' , $data);
    }

    public function rep(){
    $numb= $this->session->userdata('memeber');
    $loanee_no=$numb['loanee_no'];
     
     $this->db->where('loanee_no',$loanee_no);

     $data['sql'] = $this->db->get("repayment"); 
     if ($data) {
         return true;
     }
     else
     {
        return false;
     }     
        
    }

    function insert_rates(){
        // $typpe=$this->typeloan1=$_POST["typeloan"];
        $typpe = $this->input->post('typeloan');
        // $payment=$this->paymentperiod1=$_POST["paymentperiod"];
        $payment = $this->input->post('paymentperiod');
        // $rate= $this->ratemonthly1=$_POST["ratemonthly"];
        $rate = $this->input->post('ratemonthly');
        $penalty = $this->input->post('penalty');

        $rate1 = array(
            'type' => $typpe, 
            'period' => $payment,
            'rate' => $rate,
            'penalty' => $penalty
             );

        $result= $this->db->insert("rates",$rate1);

        if ($result) {  
                
            return TRUE;            
        }
        else{
            return FALSE;
        }
    }

    function update_rates(){
       $type = $this->typeloan1=$_POST["typeloan"];
       $period = $this->paymentperiod1=$_POST["paymentperiod"];
       $rate = $this->ratemonthly1=$_POST["ratemonthly"];

        $this->db->update("rates",$this,array('id' =>$_POST["loantype_id"]));

    }
// public function repay_loan(){
       
//         $pfno=$this->input->post('membership_no');
//         $amountgiven=$this->input->post('amountgiven');
//         $p_metthod=$this->input->post('paymentmethod');
//         $balance=$this->input->post('balance');
       
                         
                    

//                     if (($balance-$amountgiven) <= 0) {
//                         $status ="PAID";
//                     }
//                     else if (($balance-$amountgiven) > 0) {
//                        $status="INCOMPLETE";
//                     }

//                     $bal=($balance-$amountgiven);  
                                     
//             $insertrepayment = array(
//                     'loanee_no'=>$pfno,
//                     'date'=>date('Y-m-d H:i:m'),
//                     'amount'=>$amountgiven,
//                     'loan_type'=>$p_metthod,
//                     'balance'=>$bal,
//                     'loan_status'=>$status
//                     );

//                    $new_intallment = ($intallment+$amountgiven);

//             $updatearry = array(
//                     'paid_installment' =>$new_intallment,
//                     'loan_status' =>$status
//                      );
//                    $this->db->where('loanee_no',$member_no);
//                    $result = $this->db->update("loans",$updatearry);
//                    $result1 = $this->db->insert('repaymet',$insertrepayment);
//                    if ($result and $result1) {
//                        return true;
//                    }
//                    else{
//                     return false;
//                    }
    
//     }

    public function list_individual($id){
    $this->db->where("loanee_no",$id);
    $this->db->from('repayment');
   
    $sql = $this->db->get();

    if($sql->num_rows() > 0){
        foreach ($sql->result() as $row) {
           $data[]=$row;
        }
        return $data;
    }
    else{
        return false;
    }
    }
    public function individual_rep(){
        $code= $this->input->post('membership_no');

         $this->db->where("loanee_no",$code);
         $this->db->from('repayment');
   
         $sql = $this->db->get();

    if($sql->num_rows() > 0){
        foreach ($sql->result() as $row) {
           $data[]=$row;
        }
        return $data;
    }
    else{
        return false;
    }

    }
    public function update_pay($id){
    
    
    $this->db->where("loanee_no",$id);
    $this->db->from('loans');
   
    $sql = $this->db->get();

    
    if ($sql->num_rows()>0) {
        foreach ($sql->result() as $row) {
            // if ($row->balance >0) {
            //     $S_status= "INCOMPLETE";
            // }
            // else{
            //     $S_status= "COMPLETE";
            // }
            $array_new = array(
                'id' =>$row->id ,
                'loanee_no' =>$row->loanee_no,
                'app_date' =>$row->app_date,
                'loan_type' =>$row->loan_type,
                'amount_applied' =>$row->amount_applied,
                'processing_fee' =>$row->processing_fee,
                'amount_given' =>$row->amount_given, 
                'date_given' =>$row->app_date,
                'paid_installment' =>$row->paid_installment, 
                'principal' =>$row->principal,
                'interest' =>$row->interest,
                'overdue' =>$row->overdue,
                'penalty' =>$row->penalty, 
                'loan_status' =>$row->loan_status,
                'total' =>$row->total,
                'new_total' =>$row->new_total
                
                 );

           
                    }

        $this->session->set_userdata("repayment",$array_new);
        return true;
    }else{
        return false;
    }
}
public function save_payment(){// doing update calculation to update repaymentdetails.

    $data= $this->session->userdata('repayment'); 
    $pfno= $data['loanee_no'];
    $loan_type= $data['loan_type'];
    $paid_instls = $data['paid_installment'];
    $balance = $data['total'];

    //$pfno=$this->input->post('membership_no');
    $a_given=$this->input->post('amountgiven');
    $p_metthod=$this->input->post('paymentmethod');
    // $loan_type=$this->input->post('loan_type');
    //$balance=$this->input->post('balance');
    //updates

    //$totalInstallments=$this->input->post('amountgivenlast');
    $paid_installment=($paid_instls+$a_given);


    $bal=($balance-$a_given);

    if ($balance > 0) {
                $S_status= "INCOMPLETE";
            }
     else{
                $S_status= "COMPLETE";
            }

    $insert_payments = array(
       
        'loanee_no' =>$pfno ,
        'date' => date('Y-m-d H:i:m'),
        'amount' => $a_given,
        'loan_type' => $loan_type ,
        'balance' => $bal,
        'loan_status' => $S_status
         );

     $insert_updates = array(
       
        'paid_installment' =>$paid_installment,        
        'total' => $bal,
        'loan_status' => $S_status
         );
    $rez=$this->db->insert('repayment',$insert_payments);

    $this->db->where('loanee_no',$pfno);
    $this->db->where('loan_type',$loan_type);

    $result=$this->db->update('loans',$insert_updates);

    if ($result and $rez) {
        
        return true;       
    }
    else{
        return false;
    }
}

 public function payment_recipt(){
       

         $this->db->select("*");
         $this->db->limit(1);
         $this->db->from('repayment');
   
         $sql = $this->db->get();

    if($sql->num_rows() > 0){
        foreach ($sql->result() as $row) {
           $data[]=$row;
        }
        return $data;
    }
    else{
        return false;
    }

    }

    public function search($search){
    
    $this->db->select("*");
    $this->db->where("id", $search);

    $query=$this->db->get("loans");

    if ($query->num_rows()>0) {
        foreach ($query->result() as $rows) {
            $data[]=$rows;

            $searchArray = array(
                'id'=>$rows->id,
                'loanee_no'=>$rows->loanee_no,
                'total'=>$rows->total               
                );  
        
                
        }
        $this->session->set_userdata("search",$searchArray);
        return $data;
    }

        else{
            return 0;
        }
    

}
public function update_loans(){
    $money=$this->input->post('amountgiven');

    $amt=$this->session->userdata("search");
    $id=$amt['id'];
    $loanee_no=$amt['loanee_no'];
    $total=$amt['total'];

    $searchArray = array(
                
                'loanee_no'=>$loanee_no,
                'total'=>$money ,
                'new_total'=>$total              
                ); 
    $this->session->set_userdata("trys",$searchArray);
        $this->db->where('id',$id);
    $result=$this->db->update('loans',$searchArray);
    if ($result) {
        return true;
    }
    else{
        return false;
    }
}

   // function clearedstudents(){
   //      $this->db->select('*');
   //      $query = $this->db->get('rates');
   //      if($query->num_rows()>0){
   //      foreach ($query->result() as $row) {
   //          # code...
   //          $data[]=$row;
   //      }
    
   //  return $data;
   //  }
   //  else{
   //      return 0;
   //  }
        
   //  }


}
?>
<?php


class Insert_model extends CI_Model{

	public function __construct(){

		$this->load->database();
 	}
	public function save() {
		$this->db->order_by("id", "desc");
		$this->db->limit(1);
		$query=$this->db->get('members');
		$id=$zeros=$member_no="";

		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$id=1+$row->id;
			}
		}
		else{
			$id=1;
		}
		if (strlen($id)==1) {
			$zeros='000';
		}
		else if (strlen($id)==2) {
			$zeros='00';
		}
		else if (strlen($id)==3) {
			$zeros='0';
		}
		else{
			$zeros='';
		}
		$member_no='BWS/'.$zeros.$id.'/'.date('Y');
		$data = array(
		'member_no'=>$member_no,
		'dor'=>date('Y-m-d H:i:s'),
		'fname'=>$this->input->post("fname"),	
		'mname'=>$this->input->post("mname"),
		'lname'=>$this->input->post("lname"),
		'phone_no1'=>$this->input->post("phone1"),
		'phone_no2'=>$this->input->post("phone2"),
		'phone_no3'=>$this->input->post("phone3"),
		'email'=>$this->input->post("email"),
		'address'=>$this->input->post("address"),
		'dob' => $this->input->post('date1'),
		'id_no'=>$this->input->post("id"),
		'county'=>$this->input->post("county"),
		'subcounty'=>$this->input->post("subcounty"));


		 $success = $this->db->insert('members',$data);
		 if ($success) {
		 	$this->session->set_userdata('personal_details',$data);
		 	return TRUE;
		 }
		 else{
		 	return FALSE;
		 }
	} 

	public function selfemployed() {
		$member_no = $this->session->userdata('member_no');
		if (empty($member_no)) {
			return FALSE;
		}
		else{
		$data = array(
		'member_no' => $member_no,
		'tob'=>$this->input->post("tob"),
		'startdate'=> date('d-m-y', strtotime($this->input->post('date1'))),	
		'phone'=>$this->input->post("phone"),
		'address'=>$this->input->post("address"),
		'county'=>$this->input->post("county"),
		'subcounty'=>$this->input->post("subcounty"));

		 $success = $this->db->insert('self_employed_persons',$data);
		 if ($success) {
		 	$this->session->set_userdata('self_employed',$data);
		 	return TRUE;
		 }
		 else{
		 	return FALSE;
		 }
		 
	}
	}

	public function employed() {
		 $member_no = $this->session->userdata('member_no');
		if (empty($member_no)) {
			return FALSE;
		}

		else{

		$data = array(
		'member_no'=> $member_no,
		'doa'=> $this->input->post('admission'),	
		'employer'=>$this->input->post("employer"),
		'department'=>$this->input->post("department"),
		'pfno'=>$this->input->post("pfno"),
		'station'=>$this->input->post("station"),
		'address'=>$this->input->post("address"),
		'county'=>$this->input->post("county"),
		'subcounty'=>$this->input->post("subcounty"));

		$success = $this->db->insert('employed_personnel0',$data);
		if ($success) {
			$this->session->set_userdata('employed',$data);
			return TRUE;
		}
		else{
			return FALSE;
		}
		}
	}
		public function nextofkin(){
			$member = $this->session->userdata('employed');
			$member_no = $member['member_no'];
			$data  = array(
				'member_no'=> $member_no,
				'fname'=> $this->input->post('fname'),	
				'mname'=>$this->input->post("mname"),
				'lname'=>$this->input->post("lname"),
				'id_no'=>$this->input->post("id"),
				'phone_no1'=>$this->input->post("phone"),
				'phone_no2'=>$this->input->post("phone2"),
				'relationship'=>$this->input->post("relationship"));


			$success = $this->db->insert('next_of_kin', $data);
			if ($success) {
				$this->session->set_userdata('nexts_of_kin',$data);
				return TRUE;
			}
			else{
				return FALSE;
				}
	}

		public function approvingofficer() {
			$member_no = $this->session->userdata('member_no');
			if (empty($member_no)) {
				return FALSE;
			}
			else{

		$data = array(
		'member_no'=> $member_no,
		'fname'=>$this->input->post("fname"),
		'mname'=>$this->input->post("mname"),
		'lname'=>$this->input->post("lname"),
		'designation'=>$this->input->post("designation"),
		'date_approved'=> date('d-m-y', strtotime($this->input->post('date2')))
		);

		$success = $this->db->insert('approving_officer',$data);
		if ($success) {
			$this->session->set_userdata('approvingofficer',$data);
					return TRUE;
				}
				else{
					return FALSE;
				}
			}
	}



	public function nominee1() {
		$member = $this->session->userdata('nexts_of_kin');
		$member_no = $member['member_no'];
		$data = array(
		'member_no'=>$member_no,
		'fname'=>$this->input->post("fname"),	
		'mname'=>$this->input->post("mname"),
		'lname'=>$this->input->post("lname"),
		'phone_no'=>$this->input->post("phone1"),
		'dob' => $this->input->post('date1'),
		'id_no'=>$this->input->post("id"),
		'county'=>$this->input->post("county"),
		'relationship'=>$this->input->post("relationship"),
		'subcounty'=>$this->input->post("subcounty"));


		 $success = $this->db->insert('nominee',$data);
		 if ($success) {
		 	$this->session->set_userdata('nominee1',$data);
		 	return TRUE;
		 }
		 else{
		 	return FALSE;
		 }
	
}


	public function nominee2() {
		$member = $this->session->userdata('nominee1');
		$member_no = $member['member_no'];
		
		$data = array(
		'member_no'=>$member_no,
		'fname'=>$this->input->post("fname"),	
		'mname'=>$this->input->post("mname"),
		'lname'=>$this->input->post("lname"),
		'phone_no'=>$this->input->post("phone1"),
		'dob' => $this->input->post('date1'),
		'id_no'=>$this->input->post("id"),
		'county'=>$this->input->post("county"),
		'relationship'=>$this->input->post("relationship"),
		'subcounty'=>$this->input->post("subcounty"));


		 $success = $this->db->insert('nominee',$data);
		 if ($success) {
		 	$this->session->set_userdata('nominee2',$data);
		 	return TRUE;
		 }
		 else{
		 	return FALSE;
		 }
	
}



	public function nominee3() {
		$member = $this->session->userdata('nominee2');
		$member_no = $member['member_no'];
		$data = array(
		'member_no'=>$member_no,
		'fname'=>$this->input->post("fname"),	
		'mname'=>$this->input->post("mname"),
		'lname'=>$this->input->post("lname"),
		'phone_no'=>$this->input->post("phone1"),
		'dob' => $this->input->post('date1'),
		'id_no'=>$this->input->post("id"),
		'county'=>$this->input->post("county"),
		'relationship'=>$this->input->post("relationship"),
		'subcounty'=>$this->input->post("subcounty"));


		 $success = $this->db->insert('nominee',$data);
		 if ($success) {
		 	$this->session->set_userdata('nominee3',$data);
		 	return TRUE;
		 }
		 else{
		 	return FALSE;
		 }
	} 

public function save_regfees() {
		$member = $this->session->userdata('nominee3');
		$member_no = $member['member_no'];

		$data = array(
		'member_no'=>$member_no,
		'type'=>'Registartion',
		'date'=>date('Y-m-d H:i:s'),
		'method'=>$this->input->post("method"),	
		'bank'=>$this->input->post("bank"),
		'account'=>$this->input->post("account"),
		'details'=>$this->input->post("details"),
		'officer' => $this->input->post('officer'));


		 $success = $this->db->insert('disbursement',$data);
		 if ($success) {
		 	$this->session->set_userdata('regfees',$data);
		 	return TRUE;
		 }
		 else{
		 	return FALSE;
		 }
	} 

public function shares() {

		$search = $this->input->post('search');
		$this->db->select('*');
		$this->db->from('members');

		$this->db->where('member_no', $search);
		$query = $this->db->get();

		$this->session->set_userdata('shares', $query);

		if ($query->num_rows()>0) {
			foreach ($query ->result() as $row) {
				$data[] =$row;
				# code...
			}
			return $data;
			# code...
		}
		else{
			$data['shares_error'] ='No record found';
			$this->load->view('updateshares',$data);
		}
	}

	public function updateshares(){
		 $sql = 'select * from members';
          $query = $this->db->query($sql);
          $result = $query->result();
          return $result;

          $this->session->set_userdata('shares',$query);
	}
	public function membersreport(){
		 $sql = 'select member_no,fname, lname, phone_no1, id_no from members';
          $query = $this->db->query($sql);
          $result = $query->result();
          return $result;
	}

	public function ushares() {
	
		$data = array(
		'members_no'=>$this->input->post("member"),
		'shares'=>$this->input->post("shares"));

		 $success = $this->db->insert('shares',$data);
		 if ($success) {
		 	return TRUE;
		 }
		 else{
		 	return FALSE;
		 }
	}

	public function saveshares() {
		
		
		$data = array(
		
		'member_no'=>$this->input->post("memberno"),
		'shares' => $this->input->post('shares'));


		$this->db->insert('shares',$data);
		
	
}
	

}
?>
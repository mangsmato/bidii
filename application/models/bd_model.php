<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Bd_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
		function get_loans() {    
		$loan_type=array();
	    $this->db->select('type');
	    $this->db->from('rates');
	    $this->db->order_by('type');
	    $query = $this->db->get();
	    if ($query->num_rows() > 0) {
	        foreach ($query->result_array() as $row) {
	            $loan_type[] = $row;
	        }
	    }
	    return $loan_type;
	}
	function applicant(){
		$search = $this->input->post('search');
		$this->db->select('*');
		$this->db->from('members');
		$this->db->join('employed_personnel','employed_personnel.member_no=members.member_no','left');
		$this->db->join('self_employed_persons','self_employed_persons.member_no=members.member_no','left');
		// $this->db->join('location','location.member_no=members.member_no');
		$this->db->join('loans','loans.loanee_no=members.member_no','left');
		 // $this->db->where('fname', $search);
		 // $this->db->or_where('lname', $search);
		 $this->db->where('members.member_no', $search);
		$query = $this->db->get();

		if ($query->num_rows()>0) {
			// $this->session->unset_userdata('member_no');
			foreach ($query->result() as $row) {
				$new_data = array(
					'member_no' => $search,
					'name' => $row->fname ." ". $row->lname,
					'shares' =>$row->shares
					);
				$data[] = $row;

			}
			$this->session->set_userdata('applicant',$new_data);

			return $data;
		}
		else{
			return false;	
		}
	}
	function guarantor($search){
		// $search = $this->input->post('search');
		$this->db->select('*');
		$this->db->from('members');
		$this->db->join('employment','members.member_no=employment.member_no');
		$this->db->join('location','members.member_no=location.member_no');
		 // $this->db->where('fname', $search);
		 // $this->db->or_where('lname', $search);
		 $this->db->where('members.member_no', $search);
		$query = $this->db->get();

		if ($query->num_rows()>0) {
			// $this->session->unset_userdata('member_no');
			foreach ($query->result() as $row) {
				
				$data[] = $row;

			}
			

			return $data;
		}
		else{
			return 0;		
		}
	}

  	function complete($q){
	    $this->db->select('*');
	    $this->db->like('member_no', $q);
	    $query = $this->db->get('members');
	    if($query->num_rows > 0){
	      foreach ($query->result_array() as $row){
	        $new_set['label']=htmlentities(stripslashes($row['fname'] . " " . $row['lname']));//build an array
	        $new_set['value']=htmlentities(stripslashes($row['member_No']));
	        $row_set[] = $new_set;
	      echo json_encode($row_set); //format the array into json data
	      
	    }	    
	  }
	}
	
	function ref_reg(){
		$loanee_session=$this->session->userdata('applicant');
		if ($loanee_session) {
		$loanee_no = $loanee_session['member_no'];

  			$fname = $this->input->post('fname');
  			$lname = $this->input->post('lname');
  			$county = $this->input->post('county');
  			$sub_county = $this->input->post('sub_county');
  			$dob = $this->input->post('dob');
  			$phone = $this->input->post('phone');
  			$relation = $this->input->post('relation');
  			$member_no = $this->input->post('member_no');
  			$id_no = $this->input->post('id_no');

  			$ref1 = array(
  				'member_no' =>$member_no , 
  				'loanee_no' =>$loanee_no , 
  				'fname' =>$fname , 
  				'lname' =>$lname , 
  				'phone' =>$phone , 
  				'dob' =>$dob , 
  				'id_no' =>$id_no , 
  				'relation' =>$relation , 
  				'county' =>$county , 
  				'sub_county' =>$sub_county 
  				);

  			$fname1 = $this->input->post('fname1');
  			$lname1 = $this->input->post('lname1');
  			$county1 = $this->input->post('county1');
  			$sub_county1 = $this->input->post('sub_county1');
  			$dob1 = $this->input->post('dob1');
  			$phone1 = $this->input->post('phone1');
  			$relation1 = $this->input->post('relation1');
  			$member_no1 = $this->input->post('member_no1');
  			$id_no1 = $this->input->post('id_no1');

  			$ref2 = array(
  				'member_no' =>$member_no1 , 
  				'loanee_no' =>$loanee_no , 
  				'fname' =>$fname1 , 
  				'lname' =>$lname1 , 
  				'phone' =>$phone1 , 
  				'dob' =>$dob1 , 
  				'id_no' =>$id_no1 , 
  				'relation' =>$relation1 , 
  				'county' =>$county1 , 
  				'sub_county' =>$sub_county1 
  				);

  			$result = $this->db->insert('referees',$ref1);
  			$result .= $this->db->insert('referees',$ref2);

  			if ($result) {
  				$this->session->set_userdata('ref1',$ref1);
  				$this->session->set_userdata('ref2',$ref2);
  				return TRUE;
  			}
  			else{
  				return FALSE;
  			}
  		}
  		else{
  				return FALSE;
  			}

	}
	function loan_combo(){
		$loanee_session=$this->session->userdata('applicant');
		if ($loanee_session) {
		$type = $this->input->post('type');
		$this->db->where('type',$type);
		$query = $this->db->get('rates');
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
				 $loan = array(
		          'rate' => $row->rate,
		          'period' => $row->period,
		          'loan_type' => $row->type,
		           );
			}
			$this->session->set_userdata('loan_load',$loan);
			return $data;
		}
		else{
			return FALSE;;
		}
	}
	else{
		return FALSE;
	}

	}
	function gua_reg(){
		$loanee_session=$this->session->userdata('applicant');
		if ($loanee_session) {
			$loanee_no = $loanee_session['member_no'];

		$garanta1 = $this->session->userdata('gua1');
		$g_no1 = $garanta1['name'];
		

		$garanta2 = $this->session->userdata('gua2');
		$g_no2 = $garanta2['name'];
		

		$garanta3 = $this->session->userdata('gua3');
		$g_no3 = $garanta3['name'];
		
		$wit_sess = $this->session->userdata('witness_session');
		$wit_name = $wit_sess['name'];
		// $wit_lname = $wit_sess['wit_lname'];
		$w_no = $wit_sess['w_no'];
		$witness = array(
			'wit_no' => $w_no,
			'loanee_no' =>$loanee_no , 
			'w_name' =>$wit_name,
			// 'lname' =>$wit_lname,
			'w_date' => date('Y-m-d H:i:s')
			);
		$guarantors = array(
			'loanee_no' =>$loanee_no ,
			'guarantor1' => $g_no1,
			'guarantor2' => $g_no2,
			'guarantor3' => $g_no3,
			'g_date'=> date('Y-m-d H:i:s')
			 );
		
  		$result = $this->db->insert('guarantors',$guarantors);
  		$result .= $this->db->insert('witness',$witness);

  		if ($result) {
  			$this->session->set_userdata('witness', $witness);
  			return TRUE;
  		}
  		else{
  			return FALSE;
  		}
		}
		else{
			return FALSE;
		}
		
	}
	 function save_disburse($disbursement,$loan_save,$save_clicks){
      
    	$result = $this->db->insert('disbursement',$disbursement);
    	$result .= $this->db->insert('loans',$loan_save);
    	$result .= $this->db->insert('clicks',$save_clicks);


  		if ($result) {
  			
  			return TRUE;
  		}
  		else{
  			return FALSE;
  		}
  	
  
    }
    	function loans_report(){
    		 $radio = $this->input->post('search');
      		$loanee_no = $this->input->post('loanee_no');
    		if ($radio=='all') {
    			$this->db->select('*');
	    		$this->db->from('loans');
	    		$this->db->join('members','loans.loanee_no=members.member_no');
	    		  $this->db->order_by('app_date','desc');
    		}
		else if ($radio=='ind') {
			$this->db->select('*');
	    		$this->db->from('loans');
	    		$this->db->join('members','loans.loanee_no=members.member_no');
	    		$this->db->where('loanee_no',$loanee_no);
	    		  $this->db->order_by('app_date','desc');
		}
	    $query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
		     }
			return $data;
		}
		else{
			return FALSE;;
		}
	

	}

    	function dis_report($radio,$loanee_no){
    		if ($radio=='all') {
    			$this->db->select('*');
	    		$this->db->from('loans');
	    		$this->db->join('members','loans.loanee_no=members.member_no');
	    		$this->db->join('guarantors','loans.loanee_no=guarantors.loanee_no');
	    		$this->db->join('witness','loans.loanee_no=witness.loanee_no');
	    		  $this->db->order_by('app_date','desc');
    		}
		else if ($radio=='ind') {
			$this->db->select('*');
	    		$this->db->from('loans');
	    		$this->db->join('members','loans.loanee_no=members.member_no');
	    		$this->db->where('loanee_no',$loanee_no);
	    		  $this->db->order_by('app_date','desc');
		}
	    $query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
		     }
			return $data;
		}
		else{
			return FALSE;;
		}
	

	}


}

?>



<!-- DataTables JavaScript -->
    <script src="<?php echo base_url('assets/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>

    <!-- Custom Theme JavaScript -->
        <div id="page-wrapper">

            <div class="row">
            <div class="col-lg-12">
                    <h1 class="page-header" align="center">Loan Repayment</h1>
                </div>            
           </div>
           
        
       
<div class="panel-body">

                    <h3 class="panel-header">Members Repayment </h3>
                    <font color="green"><?php echo (isset($error)) ? $error : "" ?>
                </font>
                <font color="red"><?php echo validation_errors(); ?>
                </font>
                 
<div class="table-responsive" id="printAll">
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                      <th>Membership_No</th>
                      <th>Date Recieved</th>
                      <th>Amount Given</th>
                      <th>Loan Type</th>
                      <th>Current Interest</th>
                      <th>Amount to Pay</th>
                      <th>Balance</th>
                      <th>Status</th>
                      <th>Pay</th>
                    </thead>
                    <tbody>
                      
                       <?php
                                 
                                   if ($resy){
                                  foreach ($resy as $row)
                                    { 
                                      $bal= $row->total;
                                  if($bal>0){
                                  $loan_status="UNPAID";
                                  }
                                  else{
                                  $loan_status="PAID";
                                  }
                                      ;?>

                                    <tr class="odd gradeX">
                                            
                                            
                                            <td class="center"><?php echo $row->loanee_no; ?></td>
                                            <td><?php echo $row->app_date; ?></td>
                                            <td class="center"><?php echo $row->amount_given; ?></td>
                                            <td class="center"><?php echo $row->loan_type; ?></td>                                               
                                            <td class="center"><?php echo $row->interest; ?></td>
                                            <td class="center"><?php echo $row->paid_installment; ?></td>
                                            <td class="center"><?php echo $row->total; ?></td>
                                            <td class="center"><?php echo $loan_status; ?></td>
                                             <td class="center"><a href="<?php echo base_url('bidii/update_pay/' . $row->loanee_no); ?>"class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                                            
                                        </tr>

                                    <?php }    }?>

                    </tbody>
                </table>

    </div>
<?php echo form_open('bidii/all_report') ; ?>
    <div class="form-group">
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary" >GET REPORT FOR ALL
                 <span class="glyphicon glyphicon-print"></span>
            </button>
            
        </div>

    </div>
    <?php echo form_close(); ?>
</div>



    <!-- /.row -->
    </div><!-- /#page-wrapper -->

<script type="text/javascript">
    function printDiv() {
        var headstr = "<html><head><title>BIDII WESTERN SACCO</title></head><body>";
        var cont = "<center><h1>MAXIMISING YOUR INVESTIMENTS<br> <h5>Report</<h5></center></>"
     var printContents = document.getElementById('printAll').innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML =headstr + cont + printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>


   


 <div id="page-wrapper">

            <div class="row">
            <div class="col-lg-12">
                    <h1 class="page-header" align="center">Repayment Report</h1>
                </div>            
           </div>
            <font color="green"><?php echo (isset($error)) ? $error : "" ?>
                </font>
<div class="panel-body">                
<div class="table-responsive" id="printRepay">
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                      <th>Membership_No</th>
                      <th>Date Recieved</th>
                      <th>Amount Given</th>
                      <th>Loan Type</th>
                      <th>Current Interest</th>
                      <th>Amount to Pay</th>
                      <th>Balance</th>
                      <th>Status</th>                     
                    </thead>
                    <tbody>
                      
                       <?php
                                 
                                   if ($resy){
                                  foreach ($resy as $row)
                                    { 
                                      $bal= $row->total;
                                  if($bal>0){
                                  $loan_status="UNPAID";
                                  }
                                  else{
                                  $loan_status="PAID";
                                  }
                                      ;?>

                                    <tr class="odd gradeX">
                                            
                                            
                                            <td class="center"><?php echo $row->loanee_no; ?></td>
                                            <td><?php echo $row->app_date; ?></td>
                                            <td class="center"><?php echo $row->amount_given; ?></td> 
                                            <td class="center"><?php echo $row->loan_type; ?></td>                                             
                                            <td class="center"><?php echo $row->interest; ?></td>
                                            <td class="center"><?php echo $row->paid_installment; ?></td>
                                            <td class="center"><?php echo $row->total; ?></td>
                                            <td class="center"><?php echo $loan_status; ?></td>
                                                                                        
                                        </tr>

                                    <?php }    }?>

                    </tbody>
                </table>

    </div>

    <div class="form-group">
        <div class="col-sm-4">
            
            <button name='print' class="btn btn-primary" value="Print" onClick="printDiv()">PRINT REPORT FOR ALL<span class="glyphicon glyphicon-print"></span></button>
        </div>

    </div>

</div>
</div>

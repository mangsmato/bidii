<div id="page-wrapper">
<div class="col-sm-12">
        <div class="panel panel-default">
        <div class="panel-body">
        <?php echo form_open('bidii/save_payment') ;?>
        <div class="tab-content">  
        <?php
        $data= $this->session->userdata('repayment'); 
        $member_no= $data['loanee_no'];
        $amountgiven=$data['paid_installment'];
        $type=$data['loan_type'];
        
        $bal=$data['total'];
         ?>
        
       <!--  <div class="tab-pane <?php #echo ($tab == 'loanrepay') ? 'active' : '' ;?>" id="loanrepay">  -->
      
        <div class="panel-heading">
          Loan Repayment Details
        </div>
        <div class="col-sm-12">
        <div class="panel panel-default">
         <!-- <font color="red"><?php #echo validation_errors(); ?> </font> -->
            <font color="green"><?php echo (isset($error)) ? $error :'';?></font>  
            <font color="green"><?php echo (isset($success)) ? $success :'';?></font>
        <div class="panel-body">
        <div class="tab-content">        

             
              <div class="form-group col-sm-4">
              
                <label for="inputEmail">Memership No.</label>
                 <p class="form-control-static" name="membership_no"> <?php echo $member_no; ?></p>
            </div>                                                         
            
            <div class="form-group col-sm-4">
                <label for="inputEmail">Total Installments</label>
                 <p  class="form-control-static"  name="amountgivenlast"> Ksh. <?php echo $amountgiven; ?>
            </div>

            <div class="form-group col-sm-4">
                <label for="inputEmail">Amount</label>
                 <input type="number" class="form-control" id="inputEmail" placeholder="" name="amountgiven" min='100'>
            </div><font color="red"><?php echo form_error("amountgiven"); ?> </font>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Type</label>  
                <p class="form-control-static" name="loan_type"><?php echo $type; ?></p>            
                 <!-- <select class="selectpicker" name="loan_type" >    
               <?php
                #foreach($loans_taken as $each)
                {
                    ?>
                    <option value="<?=$each#->loan_type?>"><?=$each#->loan_type?></option>
                    <?php
                }
                ?>
                
                  </select> -->   
             </div> <font color="green"></font> <br> 
              <div class=" form-group col-sm-4">
                <label for="inputEmail">Last time Method</label >                
                 <p class="form-control-static" name="lastpatmethod"> <?php echo "no now"; ?></p>   
             </div>

            <div class="form-group col-sm-4">
            
                <label for="inputEmail">Method</label >
                  <br>      
                              
                 <select class="selectpicker" name="paymentmethod" id="paymentmethod" >
                      <option></option>   
                        <option>MPESA</option>   
                        <option>Cheque</option>
                        <option>Cash</option>   
                       
                  </select><font color="red"><?php echo form_error("paymentmethod"); ?> </font>  
                  </div> 
             </div>
            
            
             
             <div class="form-group col-sm-6">
                <label for="inputEmail">Balance</label>
                 <p class="form-control-static" name="balance"> Ksh. <?php echo $bal ?></p>
            </div>
            
             <div class="form-group col-sm-4 col-sm-offset-8">
             <?php 
             if($bal > 0)  
             {
             echo "<label for='inputEmail'>Loan Status</label>                              
                      <span class='alert alert-danger'>Incomplete</span> ";
             }  
             else{
             echo "<label for='inputEmail'>Loan Status</label>                                
                      <span class='alert alert-success'>Completed</span>";
              }?>

                             
                 
            </div>
          
      <div class="form-group">
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary" >SAVE
                 <span class="glyphicon glyphicon-save"></span>
            </button>
        </div>

    </div>
    <?php echo form_close(); ?>
   
    
    <div class="form-group">
        <div class="col-sm-4">
            <!-- <button type="submit" class="btn btn-primary" >PRINT REPORT
                 <span class="glyphicon glyphicon-print"></span>
            </button> -->
            <button name='print' class="btn btn-primary" value="Print" onClick="printDiv()">PRINT REPORT<span class="glyphicon glyphicon-print"></span></button>
        </div>

    </div>
   


    </div>
 


   </div>
    </div>
  </div>


  <!-- individual repayments -->
<div class="panel-body" id="printRepay">
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                      <th>PAYMENT ID</th>
                      <th>MEMBERSHIP_NO</th>
                      <th>DATE PAID</th>  
                      <th>PAYMENT METHOD</th>                     
                      <th>AMOUNT PAID</th>
                      <th>LOAN TYPE</th>
                      <th>BALANCE</th>
                      <th>Status</th>
                     </thead>
                    <tbody>
                      
                       <?php
                                 
                                   if ($tab){
                                  foreach ($tab as $row)
                                    { 
                                      
                                      ;?>

                                    <tr class="odd gradeX">
                                            
                                            
                                            <td class="center"><?php echo $row->id; ?></td>
                                            <td><?php echo $row->loanee_no; ?></td>
                                            <td class="center"><?php echo $row->date; ?></td>
                                            <td class="center"><?php echo $row->payment_method;?></td>                                            
                                            <td class="center"><?php echo $row->amount; ?></td>
                                            <td class="center"><?php echo $row->loan_type; ?></td>
                                            <td class="center"><?php echo $row->balance; ?></td>                                            
                                            <td class="center"><?php echo $row->loan_status; ?></td>
                                            
                                            
                                        </tr>

                                    <?php }    }?>

                    </tbody>
                </table>

    </div>
<!-- DataTables JavaScript -->
    <script src="<?php echo base_url('assets/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>

    <!-- Custom Theme JavaScript -->
  </div>

  </div>
  </div>

</div>




  </div>
  </div>


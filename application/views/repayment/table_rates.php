
<div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Rates Details
                        </div>
                        <!-- /.panel-heading -->
<?php echo form_open("bidii/rate");?>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Type_ID</th>
                                            <th>Loan Type</th>
                                            <th>Payment Period</th>
                                            <th>Monthly Rate</th>
                                            <th>Penalty</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $query = $query;
                                        foreach ($query->result_array() as $row)
                                    { ;?>
                                    <tr class="odd gradeX">
                                            
                                            <td class="center"><?php echo $row['id']; ?></td>
                                            <td class="center"><?php echo $row['type']; ?></td>
                                            <td class="center"><?php echo $row['period']." "."Day's"; ?></td>
                                            <td class="center"><?php echo $row['rate']." "."%"; ?></td>
                                            <td class="center"><?php echo $row['penalty']." "."%"; ?></td>
                                        </tr>

                                    <?php } ?>
                                                                               
                                    </tbody>
                                </table>
                                <div class="">
                                <h4>BIDII RATES</h4>
                                <p>Rates at Western Bidii Sacco Society</p>
                               
                            </div>
                            </div>
                            
                        <!-- /.panel-body -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>
                            
                        </div>

                    </div><?php echo form_close();?>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
          
             
   
</div>
<div id="page-wrapper">
 <!-- individual repayments -->
<div class="panel-body" id="printRepay">
<div  class="panel-heading">RECEIPT</div>
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                      <th>PAYMENT ID</th>
                      <th>MEMBERSHIP_NO</th>
                      <th>DATE PAID</th>  
                      <th>PAYMENT METHOD</th>                     
                      <th>AMOUNT PAID</th>
                      <th>LOAN TYPE</th>
                      <th>BALANCE</th>
                      <th>Status</th>
                     </thead>
                    <tbody>
                      
                       <?php
                                 $tab=$wewe;
                                   if ($tab){
                                  foreach ($tab as $row)
                                    { 
                                      
                                      ;?>

                                    <tr class="odd gradeX">
                                            
                                            
                                            <td class="center"><?php echo $row->id; ?></td>
                                            <td><?php echo $row->loanee_no; ?></td>
                                            <td class="center"><?php echo $row->date; ?></td>
                                            <td class="center"><?php echo $row->payment_method;?></td>                                            
                                            <td class="center"><?php echo $row->amount; ?></td>
                                            <td class="center"><?php echo $row->loan_type; ?></td>
                                            <td class="center"><?php echo $row->balance; ?></td>                                            
                                            <td class="center"><?php echo $row->loan_status; ?></td>
                                            
                                            
                                        </tr>

                                    <?php }    }?>

                    </tbody>
                </table>

    </div>
<!-- DataTables JavaScript -->
    <script src="<?php echo base_url('assets/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>

    <!-- Custom Theme JavaScript -->
  </div>
 <div class="form-group">
        <div class="col-sm-4">
            <!-- <button type="submit" class="btn btn-primary" >PRINT REPORT
                 <span class="glyphicon glyphicon-print"></span>
            </button> -->
            <button name='print' class="btn btn-primary" value="Print" onClick="printDiv()">PRINT RECEIPT<span class="glyphicon glyphicon-print"></span></button>
        </div>

    </div>
</div>
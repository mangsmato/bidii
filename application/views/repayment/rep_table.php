 <div class="panel panel-default">
 <?php echo form_open('bidii/rep');?>
                        <div class="panel-heading">
                            Rates Details
                        </div>
                        <!-- /.panel-heading -->

                            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Loan ID</th>
                                            <th>Member Number</th>                                            
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Balance</th>
                                            <th>Loan Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php 
                                    $query = $my;
                                    if (isset($my)) {
                                        

                                        foreach ($query->result_array() as $row)
                                    { 
                                        echo "<tr>";
                                        echo "<td>" . $row['id'] . "</td>";
                                        echo "<td>" . $row['loanee_no'] . "</td>" ;
                                        echo "<td>" . $row['date'] . "</td>";
                                        echo "<td>" . $row['amount'] . "</td>";
                                        echo "<td>" . $row['balance'] . "</td>";
                                        echo "<td>" . $row['loan_status'] . "</td>";
                                        echo "</tr>";
                                    }
                                    }
                                            
                                     
                                     ?>
                                                                               
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="">
                                <h4>BIDII RATES</h4>
                                <p>Rates at Western Bidii Sacco Society</p>
                               
                            </div>
                        </div><?php echo form_close();?>
<!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url('new/js/jquery-1.11.0.js');?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('new/js/bootstrap.min.js');?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('new/js/plugins/metisMenu/metisMenu.min.js');?>"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url('new/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('new/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('new/js/sb-admin-2.js');?>"></script>

                        <!-- /.panel-body -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>
                                      


       
    </div>
</div>
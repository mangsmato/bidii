<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Rates Details
                        </div>
                        <!-- /.panel-heading -->

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Type_ID</th>
                                            <th>Loan Type</th>
                                            <th>Payment Period</th>
                                            <th>Monthly Rate</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                    
                                    <?php foreach ($query->result_array() as $row)
                                    { ;?>
                                    <tr class="odd gradeX">
                                    <td><?php echo $row['loantype_id'];?></td>
                                    <td class="center"><?php echo $row['loantype'];?></td>
                                    <td class="center"><?php echo $row['paymentperiod'];?></td>
                                    <td class="center"><?php echo $row['monthlyrate'];?></td>
                                    </tr>
                                    <?php }; ?>
                                    
                                                                               
                                    </tbody>
                                </table>
                                <div class="">
                                <h4>BIDII RATES</h4>
                                <p>Rates at Western Bidii Sacco Society</p>
                               
                            </div>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
<!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url('assets/js/jquery-1.11.0.js');?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('assets/js/plugins/metisMenu/metisMenu.min.js');?>"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url('assets/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('assets/js/sb-admin-2.js');?>"></script>

                        <!-- /.panel-body -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
   
</div>
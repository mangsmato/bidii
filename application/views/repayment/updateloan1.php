<?php
                    $numb = $this->session->userdata('search');
                    $member_no = $numb['loanee_no'];
                    $balance = $numb['total'];
                    ?>
<div id="page-wrapper">
<div class="col-sm-12">
        <div class="panel panel-default">
        <div class="panel-body">
        <div class="tab-content"> 
        <div class="panel-heading">
        Loan Update
        </div>         
         <font color="green"><?php echo (isset($error)) ? $error :'';?></font> 

                           <div class="control-label col-sm-5">
                                <label for="inputEmail">LOAN ID.</label> <?php echo form_open('bidii/search');?> 
                                <div class="sidebar-search">
                                        <div class="input-group custom-search-form col-sm-4">
                                            <input type="text" class="form-control" placeholder="" name="searchfield">
                                            <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                        </div><font color="red"><?php echo form_error("searchfield"); ?> </font>
                                        <!-- /input-group -->
                                    </div> <?php echo form_close(); ?> 
                            </div><?php echo form_open('bidii/update_loans');?> 
                           <div class="control-label col-sm-5">
                                <label for="inputEmail">Membership No.</label>
                                <p class="form-control-static" name="member_no"><?php echo $member_no; ?></p>
                            </div>
                          <div class="form-group col-sm-5">
                            <label for="recipient-name" class="control-label">Total Amount To Pay:</label>
                            <p class="form-control-static" name='balance'>Ksh. <?php echo $balance ?></p>
                          </div>
                          <div class="form-group col-sm-5">
                            <label for="message-text" class="control-label">Enter New Amount:</label>
                            <input type="number" class="form-control" id="inputEmail" placeholder="" name="amountgiven">
                            <font color="red"><?php echo form_error("amountgiven"); ?> 
                          </div>
                          
                           <div class="form-group col-sm-offset-6 col-sm-2">
                                
                                    <button type="submit" class="btn btn-primary" >SAVE
                                         <span class="glyphicon glyphicon-save"></span>
                                    </button>
                               

                            </div><?php echo form_close(); ?>  
                            
                                    
                   
</div>
</div>
</div>
</div>
</div>

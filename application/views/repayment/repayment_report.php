<div id="page-wrapper">
<div class="col-sm-12">
        <div class="panel panel-default">
        <div class="panel-body">
        <div class="tab-content">          
        
        <div class="panel-heading">
          Loan Repayment Details
        </div>  <font color="red"><?php echo validation_errors(); ?> </font>
            <font color="green"><?php echo (isset($error)) ? $error :'';?></font>         
              <?php echo form_open('bidii/repaymentsearch'); ?>
                                    <div class="sidebar-search">
                                        <div class="input-group custom-search-form col-sm-8">
                                            <input type="text" name="search" class="form-control" placeholder="Search by Member Number ">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <?php echo form_close();?>

                                    <?php include('table_repayment.php');?>
        <div class="form-group">
        <div class="col-sm-offset-10">
            <button type="submit" class="btn btn-primary" >Print Report
                 <span class="glyphicon glyphicon-print"></span>
            </button>
        </div>

    </div>

                                    
                   
</div>
</div>
</div>
</div>
</div>

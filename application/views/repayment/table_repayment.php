 <div class="panel panel-default">
 
                        <div class="panel-heading">
                            REPORTS
                        </div>
                        <!-- /.panel-heading -->
<?php echo form_open("bidii/rate");?>
                            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Loan ID</th>
                                            <th>Member Number</th>                                            
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Balance</th>
                                            <th>Loan Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php
                                 
                                   if (isset($my)){
                                      foreach ($my->result_array() as $row)
                                    { ;?>
                                    <tr class="odd gradeX">
                                            
                                            <td><?php echo $row['id']; ?></td>
                                            <td><?php echo $row['loanee_no']; ?></td>
                                            <td class="center"><?php echo $row['date']; ?></td>
                                            <td class="center"><?php echo $row['amount']; ?></td>
                                            <td class="center"><?php echo $row['balance']; ?></td>
                                            <td class="center"><?php echo $row['loan_status']; ?></td>
                                            
                                        </tr>

                                    <?php }    }?>
                                                 
                                 
                                    
                                                                      
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="">
                                <h4>BIDII RATES</h4>
                                <p>Rates at Western Bidii Sacco Society</p>
                               
                            </div>
                            <?php echo form_close() ;?>
                        
<!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url('new/js/jquery-1.11.0.js');?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('new/js/bootstrap.min.js');?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('new/js/plugins/metisMenu/metisMenu.min.js');?>"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url('new/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('new/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('new/js/sb-admin-2.js');?>"></script>

                        <!-- /.panel-body -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>
                                      


       
    </div>
</div>
        <script type="text/javascript">
        function show_ind() {
    document.getElementById('ind').style.display = 'block';
    
    }
      function hide_ind() {
    document.getElementById('ind').style.display = 'none';
    
}
        </script>
        <div id="page-wrapper">
            
                <div class="col-lg-12">
                    <h1 class="page-header">Loan Report</h1>
                    <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('bd_ctlr/load_loans'); ?>
                             <form role="form" class="form-horizontal" >
                             
                                    <div class="form-group col-sm-4">
                                            <label for="inputEmail">All Loans</label>
                                            <input type="radio" value="all" name="search" onclick="hide_ind()" required>
                                           
                                        </div>
                                        <div class="form-group col-sm-4">
                                             <label for="inputEmail">Search Individual</label>
                                            <input type="radio" value="ind" name="search" onclick="show_ind()" required>
                                             <div id="ind" style="display:none;">
                                             <input type="text" name="loanee_no" id="a_complete" class="form-control ui-autocomplete-input" placeholder="enter member number">
                                             </div>
                                        </div>
                                         <div class="form-group">
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary">Search
                                                        <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </div>
                                        </div>
                            </form>
                            <?php echo form_close();?>
                </div>
               <div class="col-sm-12">
                    
                                    <?php
                                        if (isset($loanees)) {
                                            if (is_array($loanees)) {
                                                # code...
                                            
                                           echo (isset($heading)) ? $heading : "" ;
                                            echo '<div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Name</th>
                                            <th>Member No</th>
                                            <th>Date Borrowed</th>
                                            <th>Type of Loan</th>
                                            <th>Amount Borrowed (Ksh)</th>
                                            <th>Interest (Ksh)</th>
                                            <th>Overdue (days)</th>
                                            <th>Penalty (Ksh)</th>
                                            <th>Installments (Ksh)</th>
                                            <th>Balance (Ksh)</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                                            $no=0;

                                            foreach ($loanees as $row) {
                                                $no++;
                                                 echo "<tr>";
                                                    echo "<td>" . $no . "</td>";
                                                    echo "<td>" . $row->fname . " " . $row->lname . "</td>";
                                                    echo "<td>" . $row->loanee_no . "</td>";
                                                    echo "<td>" . $row->app_date . "</td>";
                                                    echo "<td>" . $row->loan_type . "</td>";
                                                    echo "<td>" .  number_format($row->amount_applied,2) . "</td>";
                                                    echo "<td>" . number_format($row->interest,2) . "</td>";
                                                    echo "<td>" . $row->overdue . "</td>";
                                                    echo "<td>" . number_format($row->penalty,2) . "</td>";
                                                    echo "<td>" . number_format($row->paid_installment,2) . "</td>";
                                                    echo "<td>" . number_format($row->total,2) . "</td>";
                                                    echo "<td>" . $row->loan_status . "</td>";
                                                 echo "</tr>";
                                            }
                                            echo '</tbody>
                                        </table>
                                        </div>';
                                        echo form_open("bd_ctlr/print_pdf");
                                        echo '<div class="col-sm-offset-10 col-sm-4">
                                <button type="submit" class="btn btn-primary">PRINT
                                     <span class="glyphicon glyphicon-print"></span>
                                </button>
                            </div>';
                            
                            echo form_close();
                                          
                                        }
                                    }
                                     ?>
                                       
                                    
                            
                            
                 </div>
    </div><!-- /#page-wrapper -->

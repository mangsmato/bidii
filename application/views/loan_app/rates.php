

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Loan Rates</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
            

                <div class="col-sm-9">
                   <form role="form" class="form-horizontal">
    <div class="panel panel-default">
        <div class="panel-heading">Loan Details</div>
        <div class="panel-body">
            
            <div class="form-group col-sm-4">
                <label for="inputEmail">Type Of Loan</label>
                 <p>
                    <select class="selectpicker">    
                        <option>Overdraft</option>   
                        <optgroup label="Emergency">
                          <option>Medical</option>
                          <option>School Fees</option>      
                        </optgroup>
                        <option>Salary Advance</option>
                        <option>Development</option>
                    </select>
                 </p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Payment Period</label>
                 <input type="email" class="form-control" id="inputEmail" placeholder="e.g 90 days">
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Rates per Month</label>
                 <input type="email" class="form-control" id="inputEmail" placeholder="e.g 10%">
            </div>
           
        </div>
       <div class="col-sm-4 col-sm-offset-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">EDIT
                 <span class="glyphicon glyphicon-edit"></span>
            </button>
        </div>  
    <div class="form-group">
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">SAVE
                 <span class="glyphicon glyphicon-save"></span>
            </button>
        </div>
    </div>
    </div>    
   
</form>
           
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Edit Loan Conditions</h4>
      </div>
      <div class="modal-body">
        <form>
           <div class="form-group">
                <label for="inputEmail">Type Of Loan</label>
                 <p>
                    <select class="selectpicker">    
                        <option>Overdraft</option>   
                        <optgroup label="Emergency">
                          <option>Medical</option>
                          <option>School Fees</option>      
                        </optgroup>
                        <option>Salary Advance</option>
                        <option>Development</option>
                    </select>
                 </p>
            </div>
            <div class="form-group ">
                <label for="inputEmail">Payment Period</label>
                 <input type="email" class="form-control" id="inputEmail" placeholder="e.g 90 days">
            </div>
            <div class="form-group ">
                <label for="inputEmail">Rates per Month</label>
                 <input type="email" class="form-control" id="inputEmail" placeholder="e.g 10%">
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save Changes</button>
      </div>
    </div>
  </div>
</div>

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  
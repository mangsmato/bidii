<?php include('header.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Register</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <!-- /.row -->
        
            <div class="row">
                
               
               
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
            <div class="row">
                
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Kindly provide viable information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <?php $tab = (isset($tab)) ? $tab : 'tab1'; ?>
                            <ul class="nav nav-tabs" id="myTabs">
                                <li class="<?php echo ($tab == 'tab1') ? 'active':''; ?>">
                                    <a href="#tab_1_1" data-toggle="tab">Personal Details</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab2') ? 'active':''; ?>">
                                    <a href="#tab_1_2" data-toggle="tab">Employement Details</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab3') ? 'active':''; ?>">
                                    <a href="#tab_1_3" data-toggle="tab">Next of Kin</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab4') ? 'active':''; ?>">
                                    <a href="#tab_1_4" data-toggle="tab">Nominee 1</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab5') ? 'active':''; ?>">
                                    <a href="#tab_1_5" data-toggle="tab">Nominee 2</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab6') ? 'active':''; ?>">
                                    <a href="#tab_1_6" data-toggle="tab">Nominee 3</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab7') ? 'active':''; ?>">
                                    <a href="#tab_1_7" data-toggle="tab">Approving Officer</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <!-- tab1 -->
                            <div class="tab-content">
                                <div class="tab-pane <?php echo ($tab == 'tab1') ? 'active' : ''; ?>" id="tab_1_1">
                                    
                                    <br>

                                    <form role="form" class ="form-inline"<?php echo form_open('home/save');?>
                                        <fieldset>


                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="First Name" name="fname" type="text" autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="Middle Name" name="mname" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="last Name" name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 1</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone1" type="tel" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 2</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone2" type="tel" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 3</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No." name="phone3" type="tel"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="email" >Email Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="e.g P.o BOX 53566 kitale" name="address" type="text" autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID/passport Number</label>
                                                <div >
                                                    <input class="form-control" required placeholder="Id number" name="id" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Date of Birth</label>
                                                <div >
                                                    <input class="form-control" required placeholder="" name="date1" id="demo1" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo1')" style="cursor:pointer"/>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home County</label>
                                                <br>
                                                 <select class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" required placeholder="subcounty" name="subcounty" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        </fieldset>
                                        <?php echo form_close();?>

                                </div>
                                <!-- end of tab 1 -->
                                <!-- start of tab2 -->
                                <!-- start of employed persons -->

                                <div class="tab-pane <?php echo ($tab == 'tab2') ? 'active' : ''; ?>" id="tab_1_2">
                                    <br><br>
                                    <p>Employed Personnel</p>
                                    <hr>
                                    <form role="form" class ="form-inline"<?php echo form_open('home/saveemployeedperson');?>
                                        <fieldset>
                                            <script src="<?php echo base_url('assets/js/datetimepicker_css.js');?>"></script>


                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Date of 1st Appointment</label>
                                                <br>
                                                <div>
                                                    <input class="form-control" required placeholder="" name="date1" id="demo2" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo2')" style="cursor:pointer"/>    
                                                </div>
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Employer</label>
                                                <br>
                                                    <input class="form-control" placeholder="employer" name="employer" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Department</label>
                                                <br>
                                                    <input class="form-control" placeholder="department" name="department" type="text" id="lname" autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >PF.No </label>
                                                <div >
                                                    <input class="form-control" placeholder="pf.no" name="pfno" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Station</label>
                                                <div >
                                                    <input class="form-control" placeholder="station" name="station" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Present Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="address" name="address" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >County</label>
                                                <div >
                                                    
                                                 <select class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" placeholder="subcounty" name="subcounty" type="text"  autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <br><br>
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        </fieldset>
                                        </form>

                                         <!-- self-employed -->
                                 <br> <hr>
                                    <p>Self-Employed Personnel</p>
                                    <hr>
                                    <form role="form" class ="form-inline"<?php echo form_open('home/saveselfemployed');?>
                                        <fieldset>     
                                          
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Type of Business</label>
                                                <div >
                                                    <input class="form-control" required placeholder="bussines" name="tob" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label >start date</label>
                                                <br>
                                                    <input class="form-control" required placeholder="" name="date1" id="demo3" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo3')" style="cursor:pointer"/>       
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone" type="tel" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Present Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="address" name="address" type="text" required autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >County</label>
                                                <div >
                                                    <select class="form-control" placeholder="" required  name="county"  type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" placeholder="subcounty" name="subcounty" type="text" required autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <br><br>
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        </fieldset>
                                        </form>
                                
                                <!-- end of self-employed persons -->
                                </div>
                                <!-- end of employed personnel -->
                               <!-- end of tab2 -->
                               <!-- start of tab3 -->

                                <div class="tab-pane <?php echo ($tab == 'tab3') ? 'active' : ''; ?>" id="tab_1_3">
                                    <form role="form" class ="form-inline"<?php echo form_open('home/nextofkin');?>
                                     <br><br>
                                   <FIELDSET>
                                   
                                    <LEGEND>Next of kin
                                    </LEGEND>
                                    <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="First Name" required name="fname" type="text"  autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="Middle Name" name="mname" type="text"  autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="last Name" required name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID No/passport No</label>
                                                <div >
                                                    <input class="form-control" placeholder="id number " required name="id" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 1</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No." required name="phone" type="tel"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 2</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No." required name="phone1" type="tel" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Relationship with the member</label>
                                                <div >
                                                    <input class="form-control" placeholder="relationships" required name="relationship" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <BR>
                                        <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                    </form>
                                </div>

                                <div class="tab-pane <?php echo ($tab == 'tab4') ? 'active' : ''; ?>" id="tab_1_4">
                                    <form role="form" class ="form-inline"<?php echo form_open('home/nominee1');?>
                                        <fieldset>
                                    <LEGEND><br> Nominee 1
                                    </LEGEND>
                                    <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="First Name" name="fname1" type="text"  autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="Middle Name" name="mname1" type="text" required autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="last Name" name="lname1" type="text" required autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No." required name="phone2" type="tel"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Date of Birth</label>
                                                <div >
                                                    <input class="form-control" required placeholder="" name="date1" id="demo4" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo4')" style="cursor:pointer"/> 
                                                </div>
                                                   
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >ID/passport No</label>
                                                <div >
                                                    <input class="form-control" placeholder="id number" name="id1" type="text" required autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                          <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Shares to be granted</label>
                                                <div >
                                                    <input float='left' class="form-control" placeholder="shares in %" name="shares" type="text" required autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                    <label for="fname" >Relationship</label>
                                                <br>
                                                    <input class="form-control" placeholder="relation" name="relationship1" type="text" required autofocus>    
                                            </div>
                                        </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >County</label>
                                                <br>
                                                    <select class="form-control" placeholder="" required  name="county1"  type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Subcounty</label>
                                                <br>
                                                    <input class="form-control" placeholder="subcounty" name="subcounty1" type="text" required autofocus>     
                                            </div>
                                        </div>
                                      

                                        <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                    </form>
                                </div>

                            <div class="tab-pane <?php echo ($tab == 'tab5') ? 'active' : ''; ?>" id="tab_1_5">
                                    <form role="form" class ="form-inline"<?php echo form_open('home/nominee2');?>
                                        <fieldset>
                                    <LEGEND> <br>Nominee 2
                                    </LEGEND>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="First Name" name="fname2" type="text"  autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="Middle Name" name="mname2" type="text" required autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="last Name" name="lname2" type="text" required autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No." required name="phone3" type="tel"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Date of Birth</label>
                                                <div >
                                                    <input class="form-control" required placeholder="" name="date2" id="demo6" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo6')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID/passport No</label>
                                                <div >
                                                    <input class="form-control" placeholder="id number" name="id2" type="text" required autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Shares to be granted</label>
                                                <div >
                                                    <input class="form-control" placeholder="shares in %" name="shares2" type="text" required autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Relationship</label>
                                                <br>
                                                    <input class="form-control" placeholder="relation" name="relationship2" type="text" required autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >County</label>
                                                <br>
                                                    <select class="form-control" placeholder="" required  name="county2"  type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Subcounty</label>
                                                <br>
                                                    <input class="form-control" placeholder="subcounty" name="subcounty2" required type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>

                                    </form>
                                </div>
                                    <div class="tab-pane <?php echo ($tab == 'tab6') ? 'active' : ''; ?>" id="tab_1_6">
                                        <form role="form" class ="form-inline"<?php echo form_open('home/nominee3');?>
                                        <fieldset>

                                    <LEGEND> <br> Nominee 3
                                    </LEGEND>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control"  placeholder="First Name" name="fname3" type="text"  autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="Middle Name" name="mname3" type="text"  autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="last Name" name="lname3" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No."  name="phone4" type="tel"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Date of Birth</label>
                                                <div >
                                                    <input class="form-control"  placeholder="" name="date3" id="demo7" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo7')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >ID/passport No</label>
                                                <div >
                                                    <input class="form-control" placeholder="id number" name="id3" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Shares to be granted</label>
                                                <div >
                                                    <input class="form-control" placeholder="shares in %" name="shares3" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Relationship</label>
                                                <br>
                                                    <input class="form-control" placeholder="relation" name="relationship3" type="text"  autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >County</label>
                                                <br>
                                                    <select class="form-control" placeholder=""  name="county3"  type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Subcounty</label>
                                                <br>
                                                    <input class="form-control" placeholder="subcounty" name="subcounty3" type="text"  autofocus>     
                                            </div>
                                        </div>

                                        
                                        <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        <br>
                                        
                                        <button type="submit" class="btn btn-lg btn-success btn-block">Next</button>
                                         
                                         
                                   </FIELDSET>
                                </form>
                                   
                                    
                                </div>
                                <!-- end of tab3 -->
                                <!-- start of tab4 -->
                                <div class="tab-pane <?php echo ($tab == 'tab7') ? 'active' : ''; ?>" id="tab_1_7">
                                    <br>
                                    <p>Approving officer personal details</p>
                                    <br>
                                    <form role="form" class ="form-inline"<?php echo form_open('home/approvingofficer');?>
                                        <fieldset>


                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="First Name" required name="fname" type="text"  autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="Middle Name" required name="mname" type="text"  autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" placeholder="last Name" name="lname" type="text" required autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Designation</label>
                                                <select class="form-control" placeholder="" name="designation" type="text" required autofocus>
                                                    <OPTION value="">----------</OPTION>
                                                    <OPTION value="CEO">CEO</OPTION>
                                                    <OPTION value="Sacco Manager">Sacco Manager</OPTION>
                                                    <OPTION value="Sacco Market Officer">Sacco Market Officer</OPTION>
                                                    <OPTION value="Sacco Officer">Sacco Officer</OPTION>
                                                </select>
                                                
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Date approved</label>
                                                <div >
                                                    <input class="form-control"  placeholder="" name="date" required id="demo8" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo8')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <BR>
                                        <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                    </form>
                                    </fieldset>
                                </div>
                                <!-- end of tab4 -->
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                
                <!-- /.col-lg-6 -->
               
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
          
            <!-- /.row -->
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

</body>

</html>

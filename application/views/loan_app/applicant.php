<?php
$name=$employer=$department=$address=$county=$subcounty=$id_no=$pfno=$loan_type=$loan_amt=$app_date="";
    if (isset($applicant)) {
        if (is_array($applicant)) {
            # code...
        
         foreach ($applicant as $row) {
            $name = $row->fname ." ". $row->lname;
            $employer = $row->employer;
            $department = $row->department;
            $address = $row->address;
            $county = $row->county;
            $subcounty = $row->subcounty;
            $id_no = $row->id_no;
            $pfno = $row->pfno;
            $member_no = $row->member_no;
            $shares = $row->shares;
            $loan_type = $row->loan_type;
            $app_date = $row->app_date;
            
           
            if (empty($loan_amt)) {
                $loan_amt = "not applicable";
            }
            else{
                $loan_amt = "Ksh. " . number_format($row->total,2);
            }
            if (empty($loan_type)) {
                $loan_type="None";
            }

            if (empty($app_date)) {
                $app_date ="not applicable";
            }
        }
    }
}
   
?>

<script type="text/javascript">

$(document).ready(function(){
  $("#a_complete").autocomplete({
    source: "bd_ctlr/complete", 
    minlength: 1,
    delay: 500,

  });
});

</script>



        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Loan Application</h1>
                 
                </div>
               
            </div>
           
            <div class="row">
            <?php $tab = (isset($tab)) ? $tab : 'applicant'; ?>
            <ul class="nav nav-pills">
                <li class="<?php echo ($tab == 'applicant') ? 'active':''; ?>">
                    <a href="#applicant" data-toggle="tab">APPLICANT DETAILS</a>
                </li>
                <li class="<?php echo ($tab == 'referees') ? 'active':''; ?>">
                    <a href="#referees" data-toggle="tab">REFEREES</a>
                </li>
                <li class="<?php echo ($tab == 'loan') ? 'active':''; ?>">
                    <a href="#loan" data-toggle="tab">LOAN </a>
                </li>
                <li class="<?php echo ($tab == 'guarantors') ? 'active':''; ?>">
                    <a href="#guarantors" data-toggle="tab">GUARANTORS</a>
                </li>
                <li class="<?php echo ($tab == 'appraisal') ? 'active':''; ?>">
                    <a href="#appraisal" data-toggle="tab">LOAN APPRAISAL</a>
                </li>
                <li class="<?php echo ($tab == 'disbursement') ? 'active':''; ?>">
                    <a href="#disbursement" data-toggle="tab">LOAN DISBURSEMENT</a>
                </li>
                <li class="<?php echo ($tab == 'report') ? 'active':''; ?>">
                    <a href="#report" data-toggle="tab">LOAN REPORT</a>
                </li>
            </ul>

                <div class="col-sm-9">
                    <div class="panel panel-default">
                       
                       
                        <div class="panel-body">
                               <div class="tab-content">
                                <div class="tab-pane <?php echo ($tab == 'applicant') ? 'active' : ''; ?>" id="applicant">
                                    
                                    <div class="sidebar-search">
                                    <?php echo form_open('bd_ctlr/applicant');?>
                                    <font color="red">
                                        <?php 
                                        echo validation_errors();
                                        echo (isset($error)) ? $error: " ";
                                        ?>
                                    </font>
                                    
                                        <div class="input-group custom-search-form col-sm-4 col-sm-offset-4">
                                            <input type="text" name="search" id="a_complete" class="form-control" placeholder="Search Member Number ">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    <?php echo form_close(); ?>
                                        <!-- /input-group -->
                                    </div>
                                    <br>
                                    
                                    <form role="form" class="form-horizontal">
                                    
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">NAME:</label>
                                            <div class="col-sm-4">
                                              <p class="form-control-static"><?php echo $name; ?></p>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="col-sm-4 control-label">EMPLOYER:</label>
                                            <div class="col-sm-4">
                                              <p class="form-control-static"><?php echo $employer; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Department:</label>
                                            <div class="col-sm-4">
                                              <p class="form-control-static"><?php echo $department; ?></p>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="col-sm-4 control-label">PRESENT ADDRESS:</label>
                                            <div class="col-sm-4">
                                              <p class="form-control-static"><?php echo $address; ?></p>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="col-sm-4 control-label">COUNTY:</label>
                                            <div class="col-sm-4">
                                              <p class="form-control-static"><?php echo $county; ?></p>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="col-sm-4 control-label">SUB-COUNTY:</label>
                                            <div class="col-sm-4">
                                              <p class="form-control-static"><?php echo $subcounty; ?></p>
                                            </div>
                                        </div>
                                         </form>
                                         <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Date Applied</th>
                                                    <th>Type of Loan</th>
                                                    <th>Balance</th>
                                                    
                                                </tr>
                                            </thead>
                                            <?php
                                            $no=0;
                                            if (isset($applicant)) {
                                                if (is_array($applicant)) {
                                                    # code...
                                                
                                                foreach ($applicant as $row) {
                                                if (!empty($row->app_date) || !empty($row->loan_type) || !empty($row->total)) {
                                                    
                                                
                                                    $no++;
                                                    echo "<tr>";
                                                    echo "<td>" . $no . "</td>";
                                                    echo "<td>" .  $row->app_date . "</td>";
                                                     echo "<td>" . $row->loan_type . "</td>";
                                                      echo "<td>" .  number_format($row->total,2) . "</td>";
                                                       
                                                       echo "</tr>";
                                                   }

                                                }
                                            }
                                        }
                                            ?>
                                        </table>
                                     
                                        <div class="form-group">
                                             <div class="col-sm-offset-8 col-sm-6">
                                              <a href="<?=base_url('loans_app')?>" type="button" class="btn btn-default">NEW APPLICANT</a>
                                                <a href="load_refs"  class="btn btn-primary">NEXT
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            </div>
                                        </div>
                                        
                                   
                                   
                                    
                                </div>
                                <div class="tab-pane <?php echo ($tab == 'referees') ? 'active' : ''; ?>" id="referees">
                                    <p><?php include 'referees.php'; ?></p>
                                </div>
                                 <div class="tab-pane <?php echo ($tab == 'loan') ? 'active' : ''; ?>" id="loan">
                                    
                                    <p><?php include 'loan.php'; ?></p>
                                </div>
                                <div class="tab-pane <?php echo ($tab == 'guarantors') ? 'active' : ''; ?>" id="guarantors">
                                    
                                    <p><?php include 'guarantors.php'; ?></p>
                                </div>
                                <div class="tab-pane <?php echo ($tab == 'appraisal') ? 'active' : ''; ?>" id="appraisal">
                                        <p><?php include 'appraisal.php'; ?></p>
                                </div>
                                <div class="tab-pane <?php echo ($tab == 'disbursement') ? 'active' : ''; ?>" id="disbursement">
                                    
                                    <p><?php include 'disbursement.php'; ?></p>
                                    
                                </div>
                                <div class="tab-pane <?php echo ($tab == 'report') ? 'active' : ''; ?>" id="report">
                                      <p><?php include 'report.php'; ?></p>                                
                                </div>
                            </div>
                        </div>
                    
            </div>
           
        </div>
        

    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  
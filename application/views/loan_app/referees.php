<font color="red"><?php  echo (isset($ref_error)) ? $ref_error: " ";?></font> 
<?php echo form_open('bd_ctlr/ref_reg');?>
<form>
    <div class="panel panel-default">
        <div class="panel-heading">REFEREE 1</div>
        <div class="panel-body">
            <div class="form-group col-sm-4">
                <label for="fname">First Name</label>
                <input type="text" class="form-control"  placeholder="Walikhe" name="fname" required>
            </div>
            <div class="form-group col-sm-4">
                <label for="lname">Last Name</label>
                <input type="text" class="form-control" placeholder="Sikoli" name="lname" required>
            </div>
             <div class="form-group col-sm-4">
                <label for="county">County</label>
                 <select class="selectpicker" name="county" required> 
                    <option>----------</option>
                    <option value="Baringo">Baringo</option>
                    <option value="Bungoma">Bungoma</option>
                    <option value="Bomet">Bomet</option>
                    <option value="Busia">Busia</option>
                    <option value="Elgeyo Marakwet">Elgeyo Marakwet</option>
                    <option value="Embu">Embu</option>
                    <option value="Garissa">Garissa</option>
                    <option value="Homa Bay">Homa Bay</option> 
                    <option value="Isiolo">Isiolo</option>
                    <option value="Kajiado">Kajiado</option>
                    <option value="Kakamega">Kakamega</option>
                    <option value="Kericho">Kericho</option>
                    <option value="Kiambu">Kiambu</option>
                    <option value="Kilifi">Kilifi</option>
                    <option value="Kirinyage">Kirinyage</option>
                    <option value="Kisii">Kisii</option>
                    <option value="Kisumu">Kisumu</option>
                    <option value="kitui">Kitui</option>
                    <option value="Kwale">Kwale</option>
                    <option value="Laikipia">Laikipia</option>
                    <option value="Lamu">Lamu</option>
                    <option value="Machakos">Machakos</option>
                    <option value="Makueni">Makueni</option>
                    <option value="Mandera">Mandera</option>
                    <option value="marsabit">Marsabit</option>
                    <option value="Meru">Meru</option>
                    <option value="Migori">Migori</option>
                    <option value="Mombasa">Mombasa</option>
                    <option value="Murang'a">Murang'a</option>
                    <option value="Nairobi">Nairobi</option>
                    <option value="Nakuru">Nakuru</option>
                    <option value="Nandi">Nandi</option>
                    <option value="Narok">Narok</option>
                    <option value="Nyamira">Nyamira</option>
                    <option value="Nyandarua">Nyandarua</option>
                    <option value="Nyeri">Nyeri</option>
                    <option value="Samburu">Samburu</option>
                    <option value="siaya">Siaya</option>
                    <option value="Taita Taveta">Taita Taveta</option>
                    <option value="Tana River">Tana River</option>
                    <option value="Tharaka Nithi">Tharaka Nithi</option>
                    <option value="Trans Nzoia">Trans Nzoia</option>
                    <option value="Turkana">Turkana</option>
                    <option value="Uasin Gishu">Uasin Gishu</option>
                    <option value="Vihiga">Vihiga</option>
                    <option value="Wajir">Wajir</option>
                    <option value="West Pokot">West Pokot</option>
                  </select> 
            </div>
            <div class="form-group col-sm-4">
                <label for="sub_county">Sub-County</label>
                <input type="text" class="form-control" placeholder="Embakasi" name="sub_county" required>
            </div>
             <div class="form-group col-sm-4">
                <label for="dob">Date of Birth</label>
                <input id="dob" name="dob" type="text" required readonly>
                <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('dob','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>
            </div>
             <div class="form-group col-sm-4">
                <label for="phone">Cell Phone</label>
                <input type="number" min="0" class="form-control" id="inputEmail" placeholder="0716456721" name="phone" required>
            </div>
             <div class="form-group col-sm-4">
                <label for="relation">Relationship to Member</label>
                <input type="text" class="form-control"  placeholder="Brother" name="relation" required>
            </div>
            <div class="form-group col-sm-4">
                <label for="member_no">Membership No.(optional)</label>
                <input type="text" class="form-control" placeholder="327738273" name="member_no">
            </div>
            <div class="form-group col-sm-4">
                <label for="phone">Nationa ID No.</label>
                <input type="text" class="form-control"  placeholder="78276358" name="id_no" required>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">REFEREE 2</div>
         <div class="panel-body">
            <div class="form-group col-sm-4">
                <label for="fname">First Name</label>
                <input type="text" class="form-control" placeholder="Wasquli" name="fname1" required>
            </div>
            <div class="form-group col-sm-4">
                <label for="lname">Last Name</label>
                <input type="text" class="form-control"  placeholder="Doliba" name="lname1" required>
            </div>
             <div class="form-group col-sm-4">
                <label for="county">County</label>
                <select class="selectpicker" name="county1" required> 
                    <option>----------</option>
                    <option value="Baringo">Baringo</option>
                    <option value="Bungoma">Bungoma</option>
                    <option value="Bomet">Bomet</option>
                    <option value="Busia">Busia</option>
                    <option value="Elgeyo Marakwet">Elgeyo Marakwet</option>
                    <option value="Embu">Embu</option>
                    <option value="Garissa">Garissa</option>
                    <option value="Homa Bay">Homa Bay</option> 
                    <option value="Isiolo">Isiolo</option>
                    <option value="Kajiado">Kajiado</option>
                    <option value="Kakamega">Kakamega</option>
                    <option value="Kericho">Kericho</option>
                    <option value="Kiambu">Kiambu</option>
                    <option value="Kilifi">Kilifi</option>
                    <option value="Kirinyage">Kirinyage</option>
                    <option value="Kisii">Kisii</option>
                    <option value="Kisumu">Kisumu</option>
                    <option value="kitui">Kitui</option>
                    <option value="Kwale">Kwale</option>
                    <option value="Laikipia">Laikipia</option>
                    <option value="Lamu">Lamu</option>
                    <option value="Machakos">Machakos</option>
                    <option value="Makueni">Makueni</option>
                    <option value="Mandera">Mandera</option>
                    <option value="marsabit">Marsabit</option>
                    <option value="Meru">Meru</option>
                    <option value="Migori">Migori</option>
                    <option value="Mombasa">Mombasa</option>
                    <option value="Murang'a">Murang'a</option>
                    <option value="Nairobi">Nairobi</option>
                    <option value="Nakuru">Nakuru</option>
                    <option value="Nandi">Nandi</option>
                    <option value="Narok">Narok</option>
                    <option value="Nyamira">Nyamira</option>
                    <option value="Nyandarua">Nyandarua</option>
                    <option value="Nyeri">Nyeri</option>
                    <option value="Samburu">Samburu</option>
                    <option value="siaya">Siaya</option>
                    <option value="Taita Taveta">Taita Taveta</option>
                    <option value="Tana River">Tana River</option>
                    <option value="Tharaka Nithi">Tharaka Nithi</option>
                    <option value="Trans Nzoia">Trans Nzoia</option>
                    <option value="Turkana">Turkana</option>
                    <option value="Uasin Gishu">Uasin Gishu</option>
                    <option value="Vihiga">Vihiga</option>
                    <option value="Wajir">Wajir</option>
                    <option value="West Pokot">West Pokot</option>
                  </select> 
            </div>
            <div class="form-group col-sm-4">
                <label for="sub_county">Sub-County</label>
                <input type="text" class="form-control"  placeholder="Bumula" name="sub_county1" required>
            </div>
            <div class="form-group col-sm-4">
                <label for="dob">Date of Birth</label>
                <input id="dob1" name="dob1" type="text" required readonly>
                <img src="<?php echo base_url();?>images/cal.gif" onclick="javascript:NewCssCal('dob1','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>
            </div>
             <div class="form-group col-sm-4">
                <label for="phone">Cell Phone</label>
                <input type="number" min="0" class="form-control"  placeholder="0756123689" name="phone1" required>
            </div>
             <div class="form-group col-sm-4">
                <label for="relation">Relationship to Member</label>
                <input type="text" class="form-control" placeholder="Sister" name="relation1" required>
            </div>
            <div class="form-group col-sm-4">
                <label for="member_no">Membership No.(optional)</label>
                <input type="text" class="form-control" placeholder="327738273" name="member_no1">
            </div>
             <div class="form-group col-sm-4">
                <label for="phone">Nationa ID No.</label>
                <input type="text" class="form-control"  placeholder="78276358" name="id_no1" required>
            </div>
        </div>
    </div>
   
    <div class="form-group">
        <div class="col-sm-offset-10 col-sm-4">
            <button type="submit" class="btn btn-primary">NEXT
                 <span class="glyphicon glyphicon-chevron-right"></span>
            </button>
        </div>
    </div>
</form>
<?php echo form_close();?>

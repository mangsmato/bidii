

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Loan Repayment</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">  
                 <div class="col-sm-9">
                   <div class="panel panel-default">
        <div class="panel-heading">Loan Details</div>
        <div class="panel-body">
             <div class="sidebar-search">
                                        <div class="input-group custom-search-form col-sm-4">
                                            <input type="text" class="form-control" placeholder="Search Name or PF No ...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <br>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Name:</label>
                <p class="form-control-static">Walucho Wekesa John</p>
            </div>
              <div class="form-group col-sm-4">
                <label for="inputEmail">Type of Loan</label>
                <p class="form-control-static">Overdraft</p>
            </div>
              <div class="form-group col-sm-4">
                <label for="inputEmail">Date Taken</label>
                <p class="form-control-static">12/06/2014</p>
            </div>
             <div class="form-group col-sm-4">
                <label for="inputEmail">Last Paid</label>
                <p class="form-control-static">28/08/2014</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Principal Loan</label>
                 <p class="form-control-static">Ksh. 200000</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Accumulated Loan</label>
                 <p class="form-control-static">Ksh. 250000</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Current Interest</label>
                 <p class="form-control-static">Ksh. 25000</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Overdue Duration</label>
                 <p class="form-control-static">85 days</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Overdue Interest</label>
                 <p class="form-control-static">Ksh. 5000</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Total Amount To Pay:</label>
                 <p class="form-control-static">Ksh. 280000</p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Amount Given</label>
                 <input type="email" class="form-control" id="inputEmail" placeholder="e.g 100000">
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Payment Method</label>                
                 <select class="selectpicker">    
                        <option>MPESA</option>   
                        <option>Cheque</option>
                        <option>Cash</option>      
                  </select>   
             </div>
             <div class="form-group col-sm-8">
                <label for="inputEmail" class="col-sm-3">Payment Details:</label>                
                 <textarea rows="4" placeholder="e.g MPESA Message, Cheque number..." class="col-sm-6"></textarea>   
             </div>
             <div class="form-group col-sm-4">
                <label for="inputEmail">Balance</label>
                 <p class="form-control-static">Ksh. 180000</p>
            </div>
            
             <div class="form-group col-sm-4 col-sm-offset-8">
                <label for="inputEmail">Loan Status</label>                                
                      <span class="alert alert-danger">Incomplete</span>              
                 
            </div>
          
             <div class="form-group">
        <div class="col-sm-4 col-sm-offset-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">EDIT
                 <span class="glyphicon glyphicon-edit"></span>
            </button>
        </div>
    </div>
      <div class="form-group">
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary" >SAVE
                 <span class="glyphicon glyphicon-save"></span>
            </button>
        </div>
    </div>
        </div>
        
   
    </div> 
           
        </div>
        
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Edit Loan</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Total Amount To Pay:</label>
            <p class="form-control-static">Ksh. 280000</p>
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Enter New Amount:</label>
            <input type="email" class="form-control" id="inputEmail" placeholder="e.g 100000">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save Changes</button>
      </div>
    </div>
  </div>
</div>


    </div> <!-- /.row -->
    </div><!-- /#page-wrapper -->
  
<?php
$app_name =$loan_type = $period =$date =$officer=$ref1_name =$ref2_name=$gara1_name= $gara2_name=$gara3_name=$wit_name=$method = $report_error = $member_no = "";
$installments = $amt_applied =0;

$applicant = $this->session->userdata('applicant');
$loan_load = $this->session->userdata('loan_load');
$loan = $this->session->userdata('loan_session');
$disburs = $this->session->userdata('disburs_session');
$ref2 = $this->session->userdata('ref2');
$ref1 = $this->session->userdata('ref1');
$garanta2 = $this->session->userdata('gua2');
$garanta1 = $this->session->userdata('gua1');
$garanta3 = $this->session->userdata('gua3');
$witness = $this->session->userdata('witness_session');
    
    if ($applicant) {
    # code...

    $app_name = $applicant['name'];
    $member_no = $applicant['member_no'];

    $loan_type = $loan_load['loan_type'];
    
    $period = $loan_load['period'] . " days";

     
     $amt_applied = $loan['amt_applied'];
     $installments = $loan['installments'];


    $date = date('d/m/Y H:i:s');

     
    $method = $disburs['method'];
    $officer = $disburs['officer'];

    
    $ref1_name = $ref1['fname'] . " " . $ref1['lname'];

     
    $ref2_name = $ref2['fname'] . " " . $ref2['lname'];

     
     $gara1_name = $garanta1['name'];

     
     $gara2_name = $garanta2['name'];

     
     $gara3_name = $garanta3['name'];

     
     $wit_name = $witness['name'] ;
 }
 else{
    $report_error = "unable to complete request, ensure previous steps are successfully completed";
 }



?>
<form role="form">
    <div class="panel panel-default">
        <div class="panel-heading">Bidii Sacco Loan Report</div>
        <div class="panel-body">
            <font color="red"><?php echo $report_error;?></font>
            <div id="printable">
            <fieldset>
                <legend>Loanee Details</legend>
                <div class="form-group col-sm-4">
                    <label for="inputEmail">Applicant Name</label>
                    <p class="form-control-static"><?php echo ucwords($app_name);?></p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="inputEmail">Membership No.</label>
                    <p class="form-control-static"><?php echo $member_no;?></p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="inputEmail">Date Awarded</label>
                     <p class="form-control-static"><?php echo $date; ?></p>
                </div> 
            </fieldset>
            <fieldset>
                <legend>Loan Details</legend>
                <div class="form-group col-sm-4">
                <label for="inputEmail">Type of Loan Awarded</label>
                 <p class="form-control-static"><?php echo ucwords($loan_type) ;?></p>
                </div>
                <div class="form-group col-sm-4 col-sm-offset-1">
                    <label for="inputEmail">Amount of Loan Awarded</label>
                     <p class="form-control-static"><?php echo "Ksh. " . number_format($amt_applied,2);?></p>
                </div>
                <div class="form-group col-sm-4">
                <label for="inputEmail">Repayment Period</label>
                 <p class="form-control-static"><?php echo $period;?></p>
                </div>
                 <div class="form-group col-sm-4 col-sm-offset-1">
                    <label for="inputEmail">Monthly Installments</label>
                     <p class="form-control-static"><?php echo "Ksh. " . number_format($installments,2); ?></p>
                </div>   
                    
               <div class="form-group col-sm-4">
                    <label for="inputEmail">Payment Method</label>
                     <p class="form-control-static"><?php echo ucwords($method);?></p>
                </div>
            </fieldset>
            <fieldset>
                <legend>Referees</legend>
                <div class="form-group col-sm-4">
                <label for="inputEmail">Referee 1</label>
                 <p class="form-control-static"><?php echo ucwords($ref1_name);?></p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="inputEmail">Referee 2</label>
                     <p class="form-control-static"><?php echo ucwords($ref2_name);?></p>
                </div>    
            </fieldset>
            <fieldset>
                <legend>Guarantors</legend>
                <div class="form-group col-sm-4">
                <label for="inputEmail">Guarantor 1</label>
                 <p class="form-control-static"><?php echo ucwords($gara1_name);?></p>
                </div>
                <div class="form-group col-sm-4">
                    <label for="inputEmail">Guarantor 2</label>
                     <p class="form-control-static"><?php echo ucwords($gara2_name);?></p>
                </div>            
               <div class="form-group col-sm-4">
                    <label for="inputEmail">Guarantor 3</label>
                     <p class="form-control-static"><?php echo ucwords($gara3_name);?></p>
                </div>
            </fieldset>
             <fieldset>
                 <legend>Approval</legend>
                 <div class="form-group col-sm-4">
                <label for="inputEmail">Witness</label>
                 <p class="form-control-static"><?php echo ucwords($wit_name);?></p>
                </div>
                 <div class="form-group col-sm-4">
                    <label for="inputEmail">Approving Officer</label>
                     <p class="form-control-static"><?php echo $officer;?></p>
                </div>
             </fieldset>       
            </div>
            
             <div class="form-group">
             
        <!-- <div class="col-sm-offset-10 col-sm-4">
           
        </div> -->
        <div class="col-sm-offset-8 col-sm-6">
        <a href="<?=base_url('loanee_pdf')?>" type="button" class="btn btn-default">NEW APPLICANT</a>
         <a href="<?=base_url('loanee_pdf')?>" type="button" class="btn btn-primary">PRINT
                 <span class="glyphicon glyphicon-print"></span>
            </a>
            <!-- <button type="button" class="btn btn-primary" onclick="printDiv()">PRINT
                 <span class="glyphicon glyphicon-print"></span>
            </button> -->
        </div>
       
       
    </div>
        </div>
        
   
    </div>    
   
</form>
 <script type="text/javascript">
            $(document).ready(function() {
                $('#datetimepicker5').datepicker();
            })
        </script>
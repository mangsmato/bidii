<?php
    $loan_session = $this->session->userdata('loan_session');
    $amt_applied = $loan_session['amt_applied'];
    $pro_fee= $loan_session['pro_fee'];
    $amt_given= $loan_session['amt_given'];
    
?>
<script type="text/javascript">
    // function disable () {
    //     document.getElementById('one_click').disabled = 'true';
    // }
</script>
<font color="red"><?php  echo (isset($d_insert_error)) ? $d_insert_error: " ";?></font> 
 <?php echo form_open('bd_ctlr/save_disburse');?>
<form>
    <div class="panel panel-default">
        <div class="panel-heading">Loan Issuance</div>
        <div class="panel-body">
         
            <div class="form-group col-sm-4">
                <label for="inputEmail">Amount Applied</label>
                <p class="form-control-static"><?php echo "Ksh. " . number_format($amt_applied,2);?></p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Processing Fee</label>
                 <p class="form-control-static"><?php echo "Ksh. " . number_format($pro_fee,2) ;?></p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Amount to be Given</label>
                 <p class="form-control-static"><?php echo "Ksh. " . number_format($amt_given,2) ;?></p>
            </div>
             <div class="form-group col-sm-4">
                <label for="inputEmail">Payment Method</label>                
                 <select class="selectpicker" name="method" required> 
                        <option></option>
                        <option value="Bank">Bank</option> 
                        <option value="Cash">Cash</option> 
                        <option value="Cheque">Cheque</option>  
                        <option value="MPESA">MPESA</option>                          
                        <option value="RTGS">RTGS</option>

                  </select>   
             </div>
             <div class="form-group col-sm-8">
                <label for="inputEmail" class="col-sm-3">Payment Details:</label>                
                 <textarea rows="4" placeholder="e.g MPESA Message, Cheque number..." class="col-sm-6" name="details"></textarea>   
             </div>
             <div class="form-group col-sm-4">
                <label for="inputEmail">Bank</label>                
                 <select class="selectpicker" name="bank"> 
                        <option></option>
                        <option value="Barclays">Barclays</option>  
                        <option value="Co-Operative">Co-operative</option>   
                        <option value="Equity">Equity</option> 
                        <option value="Faulu">Faulu</option> 
                        <option value="KCB">KCB</option>
                        <option value="NBK">NBK</option>   
                  </select>   
             </div>
              <div class="form-group col-sm-4">
                <label for="inputEmail">Account Number</label>
                <input type="number" class="form-control" id="inputEmail" placeholder="289378722938" min="0" name="account">
            </div>
             <div class="form-group col-sm-4">
                <label for="inputEmail">Approving Officer</label>                
                 <select class="selectpicker" name="officer" required>  
                        <option></option>
                        <option>CEO</option>   
                        <option>Sacco Manager</option>
                        <option>Sacco Marketing Officer</option>  
                        <option>Sacco Officer</option>     
                  </select>   
             </div>
             <br>
              <div class="form-group">
       		 <div class="col-sm-offset-10 col-sm-4 save_disb">
            <button type="submit" class="btn btn-primary" onclick="setTimeout(disable, 2)" id="one_click">NEXT
                 <span class="glyphicon glyphicon-chevron-right"></span>
            </button>
        </div>
    </div>
        </div>
        
  
    </div>    
   
</form>
<?php echo form_close(); ?>
 <script type="text/javascript">
            $(document).ready(function() {
                $('#datetimepicker5').datepicker();
            })
        </script>
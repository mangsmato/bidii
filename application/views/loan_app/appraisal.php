<?php 
    $garanta1 = $this->session->userdata('gua1');
    $shares1 = $garanta1['shares'];

    $garanta2 = $this->session->userdata('gua2');
    $shares2 = $garanta2['shares'];

    $garanta3 = $this->session->userdata('gua3');
    $shares3 = $garanta3['shares'];

    $applicant = $this->session->userdata('applicant');
    $app_shares = $applicant['shares'];

    $gua_shares = $shares1+$shares2+$shares3;

    $total_shares = $shares1+$shares2+$shares3+$app_shares;

    $loan_session = $this->session->userdata('loan_session');
    $amt_applied = $loan_session['amt_applied'];

    
?>
  <font color="red"><?php  echo (isset($appraisal_error)) ? $appraisal_error: " ";?></font>
<?php echo form_open('bd_ctlr/load_disburse');?>
<form role="form" class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">Loan Details</div>
        <div class="panel-body">
            
            <div class="form-group col-sm-4">
                <label for="inputEmail">Applicant Shares</label>
                <p class="form-control-static"><?php echo "Ksh. " . number_format($app_shares,2) ;?></p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Total Guarantor Shares</label>
                 <p class="form-control-static"><?php echo "Ksh. " . number_format($gua_shares,2) ;?></p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Total Shares</label>
                 <p class="form-control-static"><?php echo "Ksh. " . number_format($total_shares,2) ;?></p>
            </div>
            <div class="form-group col-sm-4">
                <label for="inputEmail">Amount Applied</label>
                 <p class="form-control-static"><?php echo "Ksh. " . number_format($amt_applied,2) ;?></p>
            </div>
           <div class="form-group col-sm-12">
                <label for="inputEmail">Status</label>
                    <?php 
                         // $status = "";
                        if ($amt_applied>($total_shares/4)) {
                            // $status = "Loan Application Failed";
                            echo '<span class="alert alert-danger">Loan Application Failed.
                                                               
                            Maximum Loan: Ksh.' . number_format(($total_shares/4),2)
                            .'<span class="glyphicon glyphicon-remove"></span>'
                            .'</span>';
                        }
                        else{
                            // $status = "Loan Application Sucessfull";
                            echo '<span class="alert alert-success">Loan Application Sucessfull
                                <span class="glyphicon glyphicon-ok"></span>
                                </span>  ';
                        }        
                    ?>
                 
            </div>
             <div class="form-group">
        <div class="col-sm-offset-10 col-sm-4">
            <?php
                 if ($amt_applied>($total_shares/4)) {
                     echo '<button type="submit" class="btn btn-primary disabled" >NEXT
                             <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>';
                 }
                 else{
                    echo '<button type="submit" class="btn btn-primary" >NEXT
                             <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>';
                 }
            ?>
            
        </div>
    </div>
        </div>
        
   
    </div>    
   
</form>
<?php echo form_close(); ?>
 <script type="text/javascript">
            $(document).ready(function() {
                $('#datetimepicker5').datepicker();
            })
        </script>
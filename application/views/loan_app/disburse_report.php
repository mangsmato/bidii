        <script type="text/javascript">
        function show_ind() {
    document.getElementById('ind').style.display = 'block';
    
    }
      function hide_ind() {
    document.getElementById('ind').style.display = 'none';
    
}
        </script>
        <div id="page-wrapper">
            
                <div class="col-lg-12">
                    <h1 class="page-header">Disbursement Reports</h1>
                    <font color="green">
                                <?php echo (isset($success)) ? $success : "" ?>
                            </font>
                            <font color="red">
                                <?php echo (isset($error)) ? $error : "" ?>
                                <?php echo validation_errors(); ?>
                            </font>       
                            <?php echo form_open('bd_ctlr/dis_report'); ?>
                             <form role="form" class="form-horizontal" >
                             
                                    <div class="form-group col-sm-4">
                                            <label for="inputEmail">All disbursements</label>
                                            <input type="radio" value="all" name="search" onclick="hide_ind()" required>
                                           
                                        </div>
                                        <div class="form-group col-sm-4">
                                             <label for="inputEmail">Search Individual</label>
                                            <input type="radio" value="ind" name="search" onclick="show_ind()" required>
                                             <div id="ind" style="display:none;">
                                             <input type="text" name="loanee_no" id="a_complete" class="form-control ui-autocomplete-input" placeholder="enter member number">
                                             </div>
                                        </div>
                                         <div class="form-group">
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary">Search
                                                        <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </div>
                                        </div>
                            </form>
                </div>
               <div class="col-sm-12">
                    
                                    <?php
                                        if (isset($disburse)) {
                                            if (is_array($disburse)) {
                                                # code...
                                            
                                           echo (isset($heading)) ? $heading : "" ;
                                            echo '<div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Loanee Name</th>
                                            <th>Type of Loan</th>
                                            <th>Amount Borrowed (Ksh)</th>
                                            <th>Guarantor 1</th>
                                            <th>Guarantor 2</th>
                                            <th>Guarantor 3</th>
                                            <th>Witness</th>
                                             <th>Date Applied</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                                            $no=0;
                                            foreach ($disburse as $row) {
                                                $no++;
                                                 echo "<tr>";
                                                    echo "<td>" . $no . "</td>";
                                                    echo "<td>" . $row->fname . " " . $row->lname . "</td>";
                                                    echo "<td>" . ucwords($row->loan_type) . "</td>";
                                                    echo "<td>" .  number_format($row->amount_applied,2) . "</td>";
                                                    echo "<td>" . $row->guarantor1 . "</td>";
                                                    echo "<td>" . $row->guarantor2 . "</td>";
                                                    echo "<td>" . $row->guarantor3 . "</td>";
                                                     echo "<td>" . $row->w_name . "</td>";
                                                    echo "<td>" . $row->app_date . "</td>";
                                                 echo "</tr>";
                                            }
                                            echo '</tbody>
                                        </table>
                                        </div>';
                                        echo form_open("bd_ctlr/print_pdf");
                                        echo '<div class="col-sm-offset-10 col-sm-4">
                                <button type="submit" class="btn btn-primary">PRINT
                                     <span class="glyphicon glyphicon-print"></span>
                                </button>
                            </div>';
                            
                            echo form_close();
                                          
                                        }
                                    }
                                     ?>
                                       
                                    
                            
                            
                 </div>
    </div><!-- /#page-wrapper -->

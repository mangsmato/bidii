
<style type="text/css">
  
  .table thead>tr>th, .table tbody tr{
border-bottom: 0px;
}
</style>
  <script type="text/javascript">// <![CDATA[

function load_data_ajax() {
  $.ajax({
    'type': "POST",
    'data': {
      "type": $("#loan_type").val()
    },
    'url': "<?php echo base_url(); ?>bd_ctlr/loan_combo/",
    'dataType': "json",
    success: function(JSONObject) {
      var loanHTML = "";

      for (var key in JSONObject) {
        if (JSONObject.hasOwnProperty(key)) {
          loanHTML += "<tr>";
            loanHTML += "<td>" + JSONObject[key]["period"] +" days" + "</td>";
            loanHTML += "<td>" + JSONObject[key]["rate"] + "% per month" + "</td>";
            loanHTML += "<td>" + JSONObject[key]["penalty"] + "% per day" + "</td>";
          loanHTML += "</tr>";
        }
      }

      // Replace table’s tbody html with loanHTML
      $("#loans tbody").html(loanHTML);
    }
  });
}

    </script>
    <font color="red"><?php  echo (isset($l_insert_error)) ? $l_insert_error: " ";?></font> 
  <?php echo form_open('bd_ctlr/loan_disp');?>
  <form role="form" class="form-horizontal">
    <div class="panel panel-default">
        <div class="panel-heading">Loan Details</div>
        <div class="panel-body">
            
            <div class="form-group col-sm-5">
                <label for="inputEmail">Type Of Loan</label>
                 <p>
                    <select class="selectpicker" name="type" id="loan_type" onchange="load_data_ajax()" required>  
                       
                        <option></option> 
                       <!--  <option value="Development">Development</option> 
                        <option value="School Fees">School Fees</option>
                        <option value="Medical">Medical</option>
                        <option value="Overdraft">Overdraft</option>
                        <option value="Salary Advance">Salary Advance</option> -->
                        <?php
                            foreach($loan_types as $each)
                            {
                        ?>
                            <option value="<?=$each['type']?>"><?=$each['type']?></option>
                        <?php
                          }
                        ?>

                    </select>
                 </p>
            </div>
            <div id="loan_details">
                
            </div>
            <div class="form-group col-sm-7" style="margin-right:2em;">
                 <table id="loans"  class="table"  border="0" >
                    <thead>
                      <th>Payment Period</th>
                      <th>Rate</th>
                      <th>Loan Penalty</th>
                    </thead>
                    <tbody>
                      
                    </tbody>
                </table>
            </div>
           
            <div class="form-group col-sm-4">
                <label for="inputEmail">Amount Applied</label>
                 <input type="number" class="form-control" id="inputEmail" placeholder="e.g 100000" name="amt_applied" required>
            </div>
            <div class="form-group">
        <div class="col-sm-offset-8 col-sm-3">
            <button type="submit" class="btn btn-primary">NEXT
                 <span class="glyphicon glyphicon-chevron-right"></span>
            </button>
        </div>
    </div>
        </div>
        
    
    </div>    
   
</form>
<?php echo form_close();?>

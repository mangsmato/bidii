<!-- <?php #include('../header.php'); ?> -->

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Register</h1>
                    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <!-- /.row -->
        
            <div class="row">
                
               
               
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
            <div class="row">
                
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Kindly provide viable information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <?php $tab = (isset($tab)) ? $tab : 'tab1'; ?>
                            <ul class="nav nav-tabs" id="myTabs">
                                <li class="<?php echo ($tab == 'tab1') ? 'active':''; ?>">
                                    <a href="#tab_1_1" data-toggle="tab">Personal Details</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab2') ? 'active':''; ?>">
                                    <a href="#tab_1_2" data-toggle="tab">Employement</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab3') ? 'active':''; ?>">
                                    <a href="#tab_1_3" data-toggle="tab">NextofKin</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab4') ? 'active':''; ?>">
                                    <a href="#tab_1_4" data-toggle="tab">Nominee1</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab5') ? 'active':''; ?>">
                                    <a href="#tab_1_5" data-toggle="tab">Nominee2</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab6') ? 'active':''; ?>">
                                    <a href="#tab_1_6" data-toggle="tab">Nominee3</a>
                                </li>
                               
                               
                                <li class="<?php echo ($tab == 'tab7') ? 'active':''; ?>">
                                    <a href="#tab_1_9" data-toggle="tab">Registration Fees</a>
                                </li>
                                <li class="<?php echo ($tab == 'tab8') ? 'active':''; ?>">
                                    <a href="#tab_1_10" data-toggle="tab">Personal Report</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <!-- tab1 -->
                            <div class="tab-content">
                                <div class="tab-pane <?php echo ($tab == 'tab1') ? 'active' : ''; ?>" id="tab_1_1">
                                    <!-- <font color="red"><?php echo isset($save_error) ? $save_error : "";?></font> -->
                                    <font color="red">
                                        <?php echo validation_errors();?>
                                    </font>
                                   
                                    <br>
                                    <?php echo form_open('home/save');?>

                                    <form role="form" class ="form-inline">
                                    
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="First Name" name="fname" type="text" autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="Middle Name" name="mname" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="last Name" name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 1</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone1" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 2</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone2" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 3</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No." name="phone3" type="number"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="email" >Email Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="e.g P.o BOX 53566 kitale" name="address" type="text" autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID/passport Number</label>
                                                <div >
                                                    <input class="form-control" required placeholder="Id number" name="id" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Date of Birth</label>
                                                <div >
                                                    <input class="form-control" required readonly placeholder="" name="date1" id="demo1" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo1','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home County</label>
                                                <br>
                                                 <select class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" required placeholder="subcounty" name="subcounty" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        </div>
                                         </form>
                                        <?php echo form_close();?>
                                     

                                </div>
                                <!-- end of tab 1 -->
                                <!-- start of tab2 -->
                                <!-- start of employed persons -->

                                <div class="tab-pane <?php echo ($tab == 'tab2') ? 'active' : ''; ?>" id="tab_1_2">
                                    <br><br>
                                    
                                    <p>Employed Personnel</p>
                                    <font color="red">
                                        <?php echo validation_errors(); 
                                        echo (isset($error)) ? $error: ""; ?>
                                    </font>
                                    <hr>
                                    <?php echo form_open('home/saveemployeedperson');?>
                                    <form role="form" class ="form-inline">
                                        <fieldset>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Employer</label>
                                                <br>
                                                    <input class="form-control" placeholder="employer" name="employer" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Department</label>
                                                <br>
                                                    <input class="form-control" placeholder="department" name="department" type="text" id="lname" autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >PF.No </label>
                                                <div >
                                                    <input class="form-control" placeholder="pf.no" name="pfno" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Station</label>
                                                <div >
                                                    <input class="form-control" placeholder="station" name="station" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Present Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="address" name="address" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >County</label>
                                                <div >
                                                    
                                                 <select class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" placeholder="subcounty" name="subcounty" type="text"  autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Date of 1st Appointment</label>
                                                <br>
                                               <div >
                                                    <input class="form-control" required readonly placeholder="" name="admission" id="demo2" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo2','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                                                </div>
                                            </div>
                                            </div>
                                        <br><br>
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                            </div>
                                        </fieldset>
                                        </form>
                                         <?php echo form_close(); ?>
                                        

                                         <!-- self-employed -->
                                 <br> <hr>
                                    <p>Self-Employed Personnel</p>
                                    
                                    <hr>
                                    <form role="form" class ="form-inline"><?php echo form_open('home/saveselfemployed');?>
                                        <fieldset>     
                                          
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Type of Business</label>
                                                <div >
                                                    <input class="form-control" required placeholder="bussines" name="tob" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label >start date</label>
                                                <br>
                                                    <input class="form-control" required readonly placeholder="" name="date1" id="demo3" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo3','yyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>       
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Business Number</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                            <font color="red"><?php echo form_error("phone");?></font>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Present Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="address" name="address" type="text" required autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >County</label>
                                                <div >
                                                    <select class="form-control" placeholder="" required  name="county"  type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" placeholder="subcounty" name="subcounty" type="text" required autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                            </div>
                                        </fieldset>
                                        <?php echo form_close()?>
                                        </form>
                                        
                                <!-- end of self-employed persons -->
                                </div>
                                
                                <!-- end of employed personnel -->
                               <!-- end of tab2 -->
                               <!-- start of tab3 -->

                                <div class="tab-pane <?php echo ($tab == 'tab3') ? 'active' : ''; ?>" id="tab_1_3">
                                <?php echo form_open('home/nextofkin');?>
                                    <form role="form" class ="form-inline">
                                     <br><br>
                                  
                                   
                                    <LEGEND>Next of kin
                                    <font color="red">
                                    <?php echo validation_errors(); 
                                    echo (isset($error)) ? $error: "";

                                    ?>
                                        
                                    </font>
                                    </LEGEND>
                                    <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" placeholder="First Name" required name="fname" type="text"  autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="Middle Name" name="mname" type="text"  autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" placeholder="last Name" required name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID No/passport No</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" placeholder="id number " required name="id" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 1</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" placeholder="10 Digit Mobile No." required name="phone" type="number"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                          
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 2</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" placeholder="10 Digit Mobile No." required name="phone1" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Relationship with the member</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" placeholder="relationships" required name="relationship" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <BR>
                                        <div class="col-sm-3 col-sm-offset-4">
                                        <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        </div>
                                        
                                    </form>
                                    <?php echo form_close()?>
                                </div>

                  
                                 <div class="tab-pane <?php echo ($tab == 'tab4') ? 'active' : ''; ?>" id="tab_1_4">
            
                                    <LEGEND><br> Nominee 1
                                    </LEGEND>
                                    <font color="red">
                                        <?php echo validation_errors();

                                        echo (isset($nomerror)) ? $nomerror : "";

                                        ?>
                                    </font>
                                    <?php echo form_open('home/nominee1');?>
                                   <form role="form" class ="form-inline">

                                   <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="First Name" name="fname" type="text" autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="Middle Name" name="mname" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="last Name" name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number </label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="10 Digit Mobile No." name="phone1" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID/passport Number</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="Id number" name="id" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home County</label>
                                                <br>
                                                 <select style ="width:230px" class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="subcounty" name="subcounty" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Date of Birth</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required readonly placeholder="" name="date1" id="nominee1" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('nominee1','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                          <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Relationship</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="e.g Friend" name="relationship" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-sm-3 col-sm-offset-4">
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                            </div>
     
                                    </form>
                                        <?php echo form_close()?>
                                    

                                </div>



                                <div class="tab-pane <?php echo ($tab == 'tab5') ? 'active' : ''; ?>" id="tab_1_5">
                                    
                                     
                                        
                                    <LEGEND><br> Nominee 2
                                    </LEGEND>

                                    <br>
                                     <font color="red">
                                        <?php echo validation_errors();
                                        echo (isset($nom2error)) ? $nom2error : "";
                                        ?>
                                    </font>
                                    <?php echo form_open('home/nominee2');?>
                                    <form role="form" class ="form-inline">

                                           <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="First Name" name="fname" type="text" autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="Middle Name" name="mname" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="last Name" name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number </label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="10 Digit Mobile No." name="phone1" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID/passport Number</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="Id number" name="id" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home County</label>
                                                <br>
                                                 <select style ="width:230px" class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="subcounty" name="subcounty" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                          <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Date of Birth</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required readonly placeholder="" name="date1" id="nominee" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('nominee','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                          <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Relationship</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="e.g Friend" name="relationship" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-sm-offset-4">
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                            </div>
                                  
                                    </form>
                                    <?php echo form_close();?>  
                                  </div> 



                                <div class="tab-pane <?php echo ($tab == 'tab6') ? 'active' : ''; ?>" id="tab_1_6">
                                    
                                     
                                        
                                    <LEGEND><br> Nominee 3
                                    </LEGEND>

                                    <br>
                                    <font color="red">
                                        <?php echo validation_errors();
                                        echo (isset($nom3error)) ? $nom3error : "";
                                        ?>
                                    </font>
                                    <?php echo form_open('home/nominee3');?>
                                    <form role="form" class ="form-inline">

                                      <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="First Name" name="fname" type="text" autofocus>    
                                            </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="Middle Name" name="mname" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input style ="width:230px" class="form-control" required placeholder="last Name" name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number </label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="10 Digit Mobile No." name="phone1" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID/passport Number</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="Id number" name="id" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                    

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home County</label>
                                                <br>
                                                 <select style ="width:230px" class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="subcounty" name="subcounty" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Date of Birth</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required readonly placeholder="" name="date1" id="n" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('n','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                          <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Relationship</label>
                                                <div >
                                                    <input style ="width:230px" class="form-control" required placeholder="e.g Friend" name="relationship" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                              <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary">Save </button>
                               </div>
                                
                                </form>
                                 <?php echo form_close();?>
                                </div>
                              
                                <div class="tab-pane <?php echo ($tab == 'tab7') ? 'active' : ''; ?>" id="tab_1_9">
                                    
                                   <br>
                                  <?php
                                        $loan_session = $this->session->userdata('loan_session');
                                        $amt_applied = $loan_session['amt_applied'];
                                        $pro_fee= $loan_session['pro_fee'];
                                        $amt_given= $loan_session['amt_given'];
                                        
                                    ?>
                                    <font color="red"><?php  echo (isset($regfee_error)) ? $regfee_error : " ";?></font> 
                                     <?php echo form_open('home/save_regfees');?>
                                    <form>
                                    <font color="red">
                                    <?php echo validation_errors();?>
                                        
                                    </font>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Registration Fees</div>
                                            <div class="panel-body">
                                             
                                                
                                               
                                               
                                                 <div class="form-group col-sm-4">
                                                    
                                                   
                                                    <div>
                                                        <label for="inputEmail">Payment Method</label>
                                                    </div>           
                                                     <br>     
                                                     <select class="selectpicker" name="method" required> 
                                                            <option></option>
                                                            <option value="Bank">Bank</option> 
                                                            <option value="Cash">Cash</option> 
                                                            <option value="Cheque">Cheque</option>  
                                                            <option value="MPESA">MPESA</option>                          
                                                            <option value="RTGS">RTGS</option>
                                                      </select>   
                                                 </div>

                                                

                                                 <div class="form-group col-sm-4">
                                                 <div>
                                                      <label for="inputEmail">Bank</label>   
                                                 </div>
                                                   <br>

                                                     <select class="selectpicker" name="bank"> 
                                                            <option></option>
                                                            <option value="Barclays">Barclays</option>  
                                                            <option value="Co-Operative">Co-operative</option>   
                                                            <option value="Equity">Equity</option> 
                                                            <option value="Faulu">Faulu</option> 
                                                            <option value="KCB">KCB</option>
                                                            <option value="NBK">NBK</option>   
                                                      </select>   
                                                 </div>

                                                  <div class="form-group col-sm-4">
                                                    <label for="inputEmail">Account Number</label>
                                                    <input type="number" class="form-control" id="inputEmail" placeholder="289378722938" min="0" name="account">
                                                </div>

                                                 <div class="form-group col-sm-8">
                                                 <div>
                                                         <label for="inputEmail" class="col-sm-3">Payment Details:</label>
                                                 </div>
                                                 <br>
                                                
                                                <div>
                                                
                                                    <textarea rows="4" placeholder="e.g MPESA Message, Cheque number..." class="col-sm-6" name="details"></textarea>   
                                                
                                                    
                                                </div>
                                                                    
                                                 </div>


                                                 <div class="form-group col-sm-4">
                                                    <label for="inputEmail">Approving Officer</label>                
                                                     <select class="selectpicker" name="officer" required>  
                                                            <option></option>
                                                            <option>CEO</option>   
                                                            <option>Sacco Manager</option>
                                                            <option>Sacco Marketing Officer</option>  
                                                            <option>Sacco Officer</option>     
                                                      </select>   
                                                 </div>
                                                 <br>
                                                  <div class="form-group">
                                                 <div class="col-sm-4">
                                                  <button type="submit" class="btn btn-primary">Save
                                                     
                                                </button>
                                        </div>
                                        </div>
                                            </div>
                                            
                                      
                                        </div>    
                                       
                                    </form>
                                    <?php echo form_close(); ?>
                                   
                                    
                                </div>

<?php
$q = $w = $e = $r = $t = $y = $u = $i = $o = $p =  $a = $s = $d =  $f = $g = $h = $j =$k =$l =$z=$x =$c =$v =$b =$n =$m =$qa=$qw="";

$personal_details = $this->session->userdata('personal_details');
$self_employed =$this->session->userdata('self_employed');
$employed =$this->session->userdata('employed');
$nexts_of_kin =$this->session->userdata('nexts_of_kin');
$approvingofficer =$this->session->userdata('approvingofficer');
$nominee1 =$this->session->userdata('nominee1');
$nominee2 =$this->session->userdata('nominee2');
$nominee3 =$this->session->userdata('nominee3');
$regfees =$this->session->userdata('regfees');

// if ($personal_details) {

//     $q = $personal_details['fname'];
//     $w = $personal_details['mname'];
//     $e = $personal_details['lname'];
//     $r = $personal_details['phone1'];
//     $t = $personal_details['phone2'];
//     $y = $personal_details['phone3'];
//     $u = $personal_details['email'];
//     $i = $personal_details['address'];
//     $o = $personal_details['date1'];
//     $p = $personal_details['id'];
//     $a = $personal_details['county'];
//     $s = $personal_details['subcounty'];
// }
// elseif ($employed) {

//     $d = $employed['date2'];
//     $f = $employed['employer'];
//     $g = $employed['department'];
//     $h = $employed['pfno'];
//     $j = $employed['station']; 
//     $k = $employed['address']; 
//     $l = $employed['county'];
//     $z = $employed['subcounty'];
// }
// elseif ($self_employed) {

//     $x = $self_employed['tob']; 
//     $c = $self_employed['date1']; 
//     $v = $self_employed['phone']; 
//     $b = $self_employed['address']; 
//     $n = $self_employed['county']; 
//     $m = $self_employed['subcounty']; 
    // $qa= $self_employed['']; 
    // $qw= $self_employed['']; }
    

 




?>

                                 <div class="tab-pane <?php echo ($tab == 'tab8') ? 'active' : ''; ?>" id="tab_1_10">
                                 <h3>Your Records</h3>
                                 <?php echo form_open('home');?>
                                 <form role="form" class ="form-inline">
                                 <!-- personal details -->
                                 <div class="panel panel-default">
                                 <div class="panel-heading">Personal Details</div>
                                 <div class="panel-body">
                                <div class="col-xs-4">
                                 <div class="form-group">
                                    <label  >First Name</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $q; ?></p>
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Middle Name</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $w; ?></p>
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Last Name</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $e; ?></p>
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Member Number</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $r; ?></p>
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Date of Registration</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $t; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact 1</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $y; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact 2</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $u; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact 3</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $i; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Email</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $o; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >home Address</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $p; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Date of Birth</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $a; ?></p>
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >ID/Passport Number</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $s; ?></p>
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >County</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $d; ?></p>
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Subcounty</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $f; ?></p>
                                        </div>
                                </div>           



                                 </div>
                                     
                                 </div>
                                 <!-- end of personal details -->
                                 <!-- start of employed personel details -->
                                 <div class="panel panel-default">
                                 <div class="panel-heading">Employment Details</div>
                                 <div class="panel-body">
                                 <div class="col-xs-4">
                                  <div class="form-group">
                                    <label  >Date of Admission</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $g; ?></p>
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Employer</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $h; ?></p>
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Department</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $j; ?></p>
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >PF_No</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $k; ?></p>
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Station</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $l; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >County</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $z; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Sub-county</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $x; ?></p>
                                        </div>
                                </div>   
                                  
                                </div>
                                </div>

                               
                                 <!-- end of employment details -->
                                 <!-- start of self employed -->
                                  <div class="panel panel-default">
                                 <div class="panel-heading">Self-Employment Details</div>
                                 <div class="panel-body">
                                      <div class="col-xs-4">
                                  <div class="form-group">
                                    <label  >Type of Business</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $c; ?></p>
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Start Date </label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $v; ?></p>
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Phone</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $b; ?></p>
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Address</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $n; ?></p>
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label >County</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $m; ?></p>
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Sub-county</label>
                                    </div>
                                        <div>
                                            <p class="form-control-static"><?php echo $qa; ?></p>
                                        </div>
                                </div>   
                                 
                                 </div>
                                     
                                 </div>
                                 <!-- end of self employed -->
                                 <!-- start of next of kin -->
                                 <div class="panel panel-default">
                                     <div class="panel-heading">Next Of Kin</div>
                                     <div class="panel-body">
                                          <div class="col-xs-4">
                                  <div class="form-group">
                                    <label  >First Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Middle Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Last Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >ID/Passport Number</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact 1</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact 2</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Relationship</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 
                                     </div>
                                 </div>
                                 <!-- end of next of kin -->
                                 <!-- start of nominee 1 -->
                                 <div class="panel panel-default">
                                     <div class="panel-heading">Nominee 1</div>
                                     <div class="panel-body">
                                          <div class="col-xs-4">
                                  <div class="form-group">
                                    <label  >First Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Middle Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Last Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Date of Birth</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >ID/Passport Number</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Relationship</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >County</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Sub-County</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Shares</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                              
                                     </div>
                                 </div>
                                 <!-- end of nominee 1 -->

                                  <!-- start of nominee 2 -->
                                  <div class="panel panel-default">
                                     <div class="panel-heading">Nominee 2</div>
                                     <div class="panel-body">
                                                  <div class="col-xs-4">
                                  <div class="form-group">
                                    <label  >First Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Middle Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Last Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Date of Birth</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >ID/Passport Number</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Relationship</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >County</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Sub-County</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Shares</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                     </div>
                                 </div>
                                 <!-- end of nominee 2-->

                                  <!-- start of nominee 3 -->
                                  <div class="panel panel-default">
                                     <div class="panel-heading">Nominee 3</div>
                                     <div class="panel-body">
                                               <div class="col-xs-4">
                                  <div class="form-group">
                                    <label  >First Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Middle Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Last Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Contact</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Date of Birth</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >ID/Passport Number</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Relationship</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >County</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Sub-County</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Shares</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                     </div>
                                 </div>
                                 <!-- end of nominee 3 -->
                                 <!-- start of regfee -->
                                 <div class="panel panel-default">
                                     <div class="panel-heading">Registration Fees</div>
                                     <div class="panel-body">
                                          <div class="col-xs-4">
                                  <div class="form-group">
                                    <label  >Payment Method</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Bank Name</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                               <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Account number</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div> 
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Payment Details</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>  
                                 <div class="col-xs-4">
                                    <div class="form-group">
                                        <label  >Approving Officer</label>
                                    </div>
                                        <div>
                                            <!-- <p class="form-control-static"><?php echo $fname; ?></p> -->
                                        </div>
                                </div>   
                                
                                     </div>
                                 </div>
                                 <!-- end of regfee -->

                                     
                                 </form>
                                 </div>

                               
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                
                <!-- /.col-lg-6 -->
               
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
          
            <!-- /.row -->
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>

</body>

</html>

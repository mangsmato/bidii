<!-- <?php #include('header.php'); ?> -->

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Register</h1>

        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading">
                Kindly provide viable information
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">


                <!-- Nav tabs -->
                <?php $tab = (isset($tab)) ? $tab : 'tab1'; ?>
                <ul class="nav nav-tabs" id="myTabs">
                    <li class="<?php echo ($tab == 'tab1') ? 'active':''; ?>">
                        <a href="#tab_1_1" data-toggle="tab">Personal Details</a>
                    </li>
                    <li class="<?php echo ($tab == 'tab2') ? 'active':''; ?>">
                        <a href="#tab_1_2" data-toggle="tab">Employement</a>
                    </li>
                    <li class="<?php echo ($tab == 'tab3') ? 'active':''; ?>">
                        <a href="#tab_1_3" data-toggle="tab">NextofKin</a>
                    </li>
                    <li class="<?php echo ($tab == 'tab4') ? 'active':''; ?>">
                        <a href="#tab_1_4" data-toggle="tab">Nominee1</a>
                    </li>
                    <li class="<?php echo ($tab == 'tab5') ? 'active':''; ?>">
                        <a href="#tab_1_5" data-toggle="tab">Nominee2</a>
                    </li>
                    <li class="<?php echo ($tab == 'tab6') ? 'active':''; ?>">
                        <a href="#tab_1_6" data-toggle="tab">Nominee3</a>
                    </li>


                    <li class="<?php echo ($tab == 'tab7') ? 'active':''; ?>">
                        <a href="#tab_1_9" data-toggle="tab">Registration Fees</a>
                    </li>
                    <li class="<?php echo ($tab == 'tab8') ? 'active':''; ?>">
                        <a href="#tab_1_10" data-toggle="tab">Personal Report</a>
                    </li>
                </ul>






                <!-- Tab panes -->
                <!-- tab1 -->
                <div class="tab-content">

                <!-- FIRST TAB -->

                 <div class="tab-pane <?php echo ($tab == 'tab1') ? 'active' : ''; ?>" id="tab_1_1">
                                    
                                    <font color="red">
                                        <?php echo validation_errors();?>
                                    </font>
                                   
                                    <br>

                                   <?php echo form_open('home/save');?>
                                    


                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >First Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="First Name" name="fname" type="text" autofocus>    
                                                </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Middle Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="Middle Name" name="mname" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Last Name</label>
                                                <br>
                                                    <input class="form-control" required placeholder="last Name" name="lname" type="text"  autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 1</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone1" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 2</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone2" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Phone Number 3</label>
                                                <div >
                                                    <input class="form-control" placeholder="10 Digit Mobile No." name="phone3" type="number"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="email" >Email Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="e.g P.o BOX 53566 kitale" name="address" type="text" autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >ID/passport Number</label>
                                                <div >
                                                    <input class="form-control" required placeholder="Id number" name="id" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label >Date of Birth</label>
                                                <div >
                                                    <input class="form-control" required readonly placeholder="" name="date1" id="demo1" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo1','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Home County</label>
                                                <br>
                                                 <select class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                
                                                
                                            </div>
                                        </div>
                                            <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" required placeholder="subcounty" name="subcounty" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                       
                                        <?php echo form_close();?>

                                </div>


                <!-- END OF FIRST TAB -->

                <!-- START OF SECOND TAB -->

                <div class="tab-pane <?php echo ($tab == 'tab2') ? 'active' : ''; ?>" id="tab_1_2">
                                    <br><br>
                                    
                                    <p>Employed Personnel</p>
                                    <font color="red">
                                        <?php echo validation_errors(); ?>
                                    </font>
                                    <hr>
                                    <?php echo form_open('home/saveemployeedperson');?>
                                        <fieldset>
                                           

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Employer</label>
                                                <br>
                                                    <input class="form-control" placeholder="employer" name="employer" type="text" id="Mname" autofocus>
                                                </div>
                                            </div>
                                             <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Department</label>
                                                <br>
                                                    <input class="form-control" placeholder="department" name="department" type="text" id="lname" autofocus>     
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >PF.No </label>
                                                <div >
                                                    <input class="form-control" placeholder="pf.no" name="pfno" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Station</label>
                                                <div >
                                                    <input class="form-control" placeholder="station" name="station" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Present Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="address" name="address" type="text"  autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >County</label>
                                                <div >
                                                    
                                                 <select class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" placeholder="subcounty" name="subcounty" type="text"  autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label for="fname" >Date of 1st Appointment</label>
                                                <br>
                                               <div >
                                                    <input class="form-control" required readonly placeholder="" name="admission" id="demo2" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo2','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                                                </div>
                                            </div>
                                            </div>
                                        <br><br>
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        </fieldset>
                                         <?php echo form_close(); ?>

                                   



                                         <!-- self-employed -->
                                 <br> <hr>
                                    <p>Self-Employed Personnel</p>
                                    
                                    <hr>
                                   <?php echo form_open('home/saveselfemployed');?>
                                           
                                          
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Type of Business</label>
                                                <div >
                                                    <input class="form-control" required placeholder="bussines" name="tob" type="text" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label >start date</label>
                                                <br>
                                                    <input class="form-control" required readonly placeholder="" name="date1" id="demo3" type="datetime"  autofocus>
                                                    <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('demo3','yyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/>       
                                            </div>
                                        </div>
                                         <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Business Number</label>
                                                <div >
                                                    <input class="form-control" required placeholder="10 Digit Mobile No." name="phone" type="number" autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                            <font color="red"><?php echo form_error("phone");?></font>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Present Address</label>
                                                <div >
                                                    <input class="form-control" placeholder="address" name="address" type="text" required autofocus>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >County</label>
                                                <div >
                                                    <select class="form-control" placeholder="" required  name="county"  type="text" autofocus>
                                                    <OPTION value="1">----------</OPTION>
                                                    <OPTION value="Baringo">Baringo</OPTION>
                                                    <OPTION value="Bungoma">Bungoma</OPTION>
                                                    <OPTION value="Bomet">Bomet</OPTION>
                                                    <OPTION value="Busia">Busia</OPTION>
                                                    <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                                    <OPTION value="Embu">Embu</OPTION>
                                                    <OPTION value="Garissa">Garissa</OPTION>
                                                    <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                                    <OPTION value="Isiolo">Isiolo</OPTION>
                                                    <OPTION value="Kajiado">Kajiado</OPTION>
                                                    <OPTION value="Kakamega">Kakamega</OPTION>
                                                    <OPTION value="Kericho">Kericho</OPTION>
                                                    <OPTION value="Kiambu">Kiambu</OPTION>
                                                    <OPTION value="Kilifi">Kilifi</OPTION>
                                                    <OPTION value="Kirinyage">Kirinyage</OPTION>
                                                    <OPTION value="Kisii">Kisii</OPTION>
                                                    <OPTION value="Kisumu">Kisumu</OPTION>
                                                    <OPTION value="kitui">kitui</OPTION>
                                                    <OPTION value="Kwale">Kwale</OPTION>
                                                    <OPTION value="Laikipia">Laikipia</OPTION>
                                                    <OPTION value="Lamu">Lamu</OPTION>
                                                    <OPTION value="Machakos">Machakos</OPTION>
                                                    <OPTION value="Makueni">Makueni</OPTION>
                                                    <OPTION value="Mandera">Mandera</OPTION>
                                                    <OPTION value="marsabit">marsabit</OPTION>
                                                    <OPTION value="Meru">Meru</OPTION>
                                                    <OPTION value="Migori">Migori</OPTION>
                                                    <OPTION value="Mombasa">Mombasa</OPTION>
                                                    <OPTION value="Murang'a">Murang'a</OPTION>
                                                    <OPTION value="Nairobi">Nairobi</OPTION>
                                                    <OPTION value="Nakuru">Nakuru</OPTION>
                                                    <OPTION value="Nandi">Nandi</OPTION>
                                                    <OPTION value="Narok">Narok</OPTION>
                                                    <OPTION value="Nyamira">Nyamira</OPTION>
                                                    <OPTION value="Nyandarua">Nyandarua</OPTION>
                                                    <OPTION value="Nyeri">Nyeri</OPTION>
                                                    <OPTION value="Samburu">Samburu</OPTION>
                                                    <OPTION value="siaya">siaya</OPTION>
                                                    <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                                    <OPTION value="Tana River">Tana River</OPTION>
                                                    <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                                    <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                                    <OPTION value="Turkana">Turkana</OPTION>
                                                    <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                                    <OPTION value="Vihiga">Vihiga</OPTION>
                                                    <OPTION value="Wajir">Wajir</OPTION>
                                                    <OPTION value="West Pokot">West Pokot</OPTION>
                                                    
                                                </select>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label  >Subcounty</label>
                                                <div >
                                                    <input class="form-control" placeholder="subcounty" name="subcounty" type="text" required autofocus>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <br><br>
                                            <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
                                        
                                        <?php echo form_close();?>
                                
                                        
                                <!-- end of self-employed persons -->
                                </div>

                <!-- END OF SECOND TAB -->
<!-- START OF THIRD TAB -->
                <div class="tab-pane <?php echo ($tab == 'tab3') ? 'active' : ''; ?>" id="tab_1_3">
                    <?php echo form_open('home/nextofkin');?>
                    <LEGEND>Next of kin
                        <font color="red">
                            <?php echo validation_errors(); ?>

                        </font>
                    </LEGEND>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="fname" >First Name</label>
                            <br>
                            <input style ="width:230px" class="form-control" placeholder="First Name" required name="fname" type="text"  autofocus>    
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="fname" >Middle Name</label>
                            <br>
                            <input style ="width:230px" class="form-control" required placeholder="Middle Name" name="mname" type="text"  autofocus>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="fname" >Last Name</label>
                            <br>
                            <input style ="width:230px" class="form-control" placeholder="last Name" required name="lname" type="text"  autofocus>     
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >ID No/passport No</label>
                            <div >
                                <input style ="width:230px" class="form-control" placeholder="id number " required name="id" type="text"  autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >Phone Number 1</label>
                            <div >
                                <input style ="width:230px" class="form-control" placeholder="10 Digit Mobile No." required name="phone" type="number"  autofocus>
                            </div>   
                        </div>

                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >Phone Number 2</label>
                            <div >
                                <input style ="width:230px" class="form-control" placeholder="10 Digit Mobile No." required name="phone1" type="number" autofocus>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >Relationship with the member</label>
                            <div >
                                <input style ="width:230px" class="form-control" placeholder="relationships" required name="relationship" type="text"  autofocus>
                            </div>
                        </div>
                    </div>


                    <br>
                    <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>   

                    <?php echo form_close();?>

                </div>  


                <!-- END OF THIRD TAB -->
                <!-- START OF TAB FOUR -->
                <div class="tab-pane <?php echo ($tab == 'tab4') ? 'active' : ''; ?>" id="tab_1_4">
                    <font color="red">
                        <?php echo validation_errors();?>
                    </font>
                    <?php echo form_open('home/nominee1');?>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="fname" >First Name</label>
                            <br>
                            <input style ="width:230px" class="form-control" required placeholder="First Name" name="fname" type="text" autofocus>    
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="fname" >Middle Name</label>
                            <br>
                            <input style ="width:230px" class="form-control" required placeholder="Middle Name" name="mname" type="text" id="Mname" autofocus>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="fname" >Last Name</label>
                            <br>
                            <input style ="width:230px" class="form-control" required placeholder="last Name" name="lname" type="text"  autofocus>     
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >Phone Number </label>
                            <div >
                                <input style ="width:230px" class="form-control" required placeholder="10 Digit Mobile No." name="phone1" type="number" autofocus>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >ID/passport Number</label>
                            <div >
                                <input style ="width:230px" class="form-control" required placeholder="Id number" name="id" type="text" autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >Home County</label>
                            <br>
                            <select style ="width:230px" class="form-control" placeholder="" required  name="county" type="text" autofocus>
                                <OPTION value="1">----------</OPTION>
                                <OPTION value="Baringo">Baringo</OPTION>
                                <OPTION value="Bungoma">Bungoma</OPTION>
                                <OPTION value="Bomet">Bomet</OPTION>
                                <OPTION value="Busia">Busia</OPTION>
                                <OPTION value="Elgeyo Marakwet">Elgeyo Marakwet</OPTION>
                                <OPTION value="Embu">Embu</OPTION>
                                <OPTION value="Garissa">Garissa</OPTION>
                                <OPTION value="Homa Bay">Homa Bay</OPTION> 
                                <OPTION value="Isiolo">Isiolo</OPTION>
                                <OPTION value="Kajiado">Kajiado</OPTION>
                                <OPTION value="Kakamega">Kakamega</OPTION>
                                <OPTION value="Kericho">Kericho</OPTION>
                                <OPTION value="Kiambu">Kiambu</OPTION>
                                <OPTION value="Kilifi">Kilifi</OPTION>
                                <OPTION value="Kirinyage">Kirinyage</OPTION>
                                <OPTION value="Kisii">Kisii</OPTION>
                                <OPTION value="Kisumu">Kisumu</OPTION>
                                <OPTION value="kitui">kitui</OPTION>
                                <OPTION value="Kwale">Kwale</OPTION>
                                <OPTION value="Laikipia">Laikipia</OPTION>
                                <OPTION value="Lamu">Lamu</OPTION>
                                <OPTION value="Machakos">Machakos</OPTION>
                                <OPTION value="Makueni">Makueni</OPTION>
                                <OPTION value="Mandera">Mandera</OPTION>
                                <OPTION value="marsabit">marsabit</OPTION>
                                <OPTION value="Meru">Meru</OPTION>
                                <OPTION value="Migori">Migori</OPTION>
                                <OPTION value="Mombasa">Mombasa</OPTION>
                                <OPTION value="Murang'a">Murang'a</OPTION>
                                <OPTION value="Nairobi">Nairobi</OPTION>
                                <OPTION value="Nakuru">Nakuru</OPTION>
                                <OPTION value="Nandi">Nandi</OPTION>
                                <OPTION value="Narok">Narok</OPTION>
                                <OPTION value="Nyamira">Nyamira</OPTION>
                                <OPTION value="Nyandarua">Nyandarua</OPTION>
                                <OPTION value="Nyeri">Nyeri</OPTION>
                                <OPTION value="Samburu">Samburu</OPTION>
                                <OPTION value="siaya">siaya</OPTION>
                                <OPTION value="Taita Taveta">Taita Taveta</OPTION>
                                <OPTION value="Tana River">Tana River</OPTION>
                                <OPTION value="Tharaka Nithi">Tharaka Nithi</OPTION>
                                <OPTION value="Trans Nzoia">Trans Nzoia</OPTION>
                                <OPTION value="Turkana">Turkana</OPTION>
                                <OPTION value="Uasin Gishu">Uasin Gishu</OPTION>
                                <OPTION value="Vihiga">Vihiga</OPTION>
                                <OPTION value="Wajir">Wajir</OPTION>
                                <OPTION value="West Pokot">West Pokot</OPTION>
                                
                            </select>
                            
                            
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >Subcounty</label>
                            <div >
                                <input style ="width:230px" class="form-control" required placeholder="subcounty" name="subcounty" type="text" autofocus>
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label >Date of Birth</label>
                            <div >
                                <input style ="width:230px" class="form-control" required readonly placeholder="" name="date1" id="nominee1" type="datetime"  autofocus>
                                <img src="<?php echo base_url('assets/img/cal.gif');?>" onclick="javascript:NewCssCal('nominee1','yyyyMMdd','dropdown',false,'24',true,'past')" style="cursor:pointer"/> 
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label  >Relationship</label>
                            <div >
                                <input style ="width:230px" class="form-control" required placeholder="e.g Friend" name="relationship" type="text" autofocus>
                            </div>
                            
                            
                        </div>
                    </div>

                    <button type="submit" class="btn btn-lg btn-success btn-block">Register</button>
     
                                    

                    <?php echo form_close();?>

                </div>

                <!-- END OF TAB FOUR -->



                </div>
            </div>
        </div>
    </div>
</div>


                                    
                                   

                                  
                                   
                                   
                                       
                                      

                                     
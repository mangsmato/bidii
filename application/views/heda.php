 <?php 
    $admin = $this->session->userdata('logged_in');
    if (empty($admin)) {
        redirect('home/index');
    }
    // $username = $admin['username'];
    
 ?> 
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bidii Western Sacco</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>assets/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- date time picker -->
    <link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-select.css" rel="stylesheet">
     <link href="<?php echo base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">
     <link href="<?php echo base_url();?>assets/css/jquery-ui.min.css" rel="stylesheet">
     <link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet">
     <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

   

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Bidii Sacco Management System</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                             
                                   <i class="fa fa-users fa-fw"></i> Register
                                </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                
                                    <i class="fa fa-book fa-fw"></i>Update Shares
                                    
                            </a>
                        </li>
                       
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                            
                                    <p>
                                       <i class="fa fa-angle-double-left fa-fw"></i>  Loan Application
                                        
                                    </p>
                                    
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                    <p>
                                       <i class="fa fa-money fa-fw"></i> Loan Repayment
                                        
                                    </p>
                                    
                            </a>
                        </li>
                        
                        
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-certificate fa-fw"></i> Members Report
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-cloud-upload fa-fw"></i> Loans Report
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-space-shuttle fa-fw"></i> Disbursment Report
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-truck fa-fw"></i> Repayment Report
                                </div>
                            </a>
                        </li>
                        
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="<?php echo base_url()."home/password";?>"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url()."home/logout";?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      <!-- <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Bidii Sacco</a>
                        </li> -->
                        <li>
                            <a href="<?php echo base_url()."home/register";?>"><i class="fa fa-bar-chart-o fa-fw"></i> Registration</a>
                            
                        <li>
                        <li>
                            <a href="<?php echo base_url()."home/updateshares";?>"><i class="fa fa-edit fa-fw"></i> Update Shares</a>
                        </li>
                        
                        <li>
                            <a href="<?=base_url('loans_app')?>"><i class="fa fa-table fa-fw"></i> Loan Aplication</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-edit fa-fw"></i> Loan Repayment<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url()."bidii/index";?>"><i class="fa fa-bar-chart-o fa-fw"></i>Repay Loan</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('ra');?>"><i class="fa fa-edit fa-fw"></i>Update Loan Amount</a>
                                </li>
                                </ul>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="active" href="<?=base_url('loans_report')?>">Loans Report</a>
                                </li>
                                <li>
                                    <a class="active" href="<?=base_url('disburse_report')?>">Disbursement Report</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()."bidii/all_report";?>">Repayment Report</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Settings<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('rate');?>"><i class="fa fa-edit fa-fw"></i> Update Loan Rates</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()."home/password";?>"><i class="fa fa-edit fa-fw"></i> Change Password</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> About Bidii Sacco</a>
                            
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/js/sb-admin-2.js"></script>

    <!-- date time picker -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/bootstrap-select.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/datetimepicker.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/typeahead.js"></script> 
    <script src="<?php echo base_url();?>assets/js/jquery.dataTables.js"></script> 
    <script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.js"></script>
    
    <script type="text/javascript">
    function printDiv() {
        var headstr = "<html><head><title>Club Owners</title></head><body>";
        var cont = "<center><h1> The Kenya Football Federation <br> <h5>your trusted football association</<h5></center></>"
     var printContents = document.getElementById('printable').innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML =headstr + cont + printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#dataTables-example').dataTable();
    });
    </script>
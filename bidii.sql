/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50621
Source Host           : 127.0.0.1:3306
Source Database       : bidii

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-05-19 00:21:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'stella', '098f6bcd4621d373cade4e832627b4f6');
INSERT INTO `admin` VALUES ('3', 'admin', '81dc9bdb52d04dc20036dbd8313ed055');

-- ----------------------------
-- Table structure for approving_officer
-- ----------------------------
DROP TABLE IF EXISTS `approving_officer`;
CREATE TABLE `approving_officer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(255) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `date_approved` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_no` (`member_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of approving_officer
-- ----------------------------

-- ----------------------------
-- Table structure for clicks
-- ----------------------------
DROP TABLE IF EXISTS `clicks`;
CREATE TABLE `clicks` (
  `times` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of clicks
-- ----------------------------

-- ----------------------------
-- Table structure for crud
-- ----------------------------
DROP TABLE IF EXISTS `crud`;
CREATE TABLE `crud` (
  `idcrud` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `age` varchar(20) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`idcrud`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crud
-- ----------------------------
INSERT INTO `crud` VALUES ('6', 'resa', 'kira', '78', 'keno');
INSERT INTO `crud` VALUES ('10', 'marion', 'mukisu', '87', 'kiminini');
INSERT INTO `crud` VALUES ('11', 'Rhoda', 'Kimanga', '56', 'thika');

-- ----------------------------
-- Table structure for disbursement
-- ----------------------------
DROP TABLE IF EXISTS `disbursement`;
CREATE TABLE `disbursement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `account` varchar(50) DEFAULT NULL,
  `details` text,
  `officer` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of disbursement
-- ----------------------------
INSERT INTO `disbursement` VALUES ('10', '6899', 'loan disbursement', 'Cheque', 'Faulu', '', 'given to the cheque number below', 'Sacco Marketing Officer', '2015-05-02 15:57:43');
INSERT INTO `disbursement` VALUES ('11', '4546', 'loan disbursement', 'Cash', '', '', 'dhgsdh', 'CEO', '2015-05-03 11:02:20');
INSERT INTO `disbursement` VALUES ('12', '900', 'loan disbursement', 'Cash', '', '', 'given', 'Sacco Manager', '2015-05-03 11:46:05');
INSERT INTO `disbursement` VALUES ('13', '4546', 'loan disbursement', 'Bank', 'Faulu', '565387', 'account name', 'Sacco Marketing Officer', '2015-05-03 12:01:51');
INSERT INTO `disbursement` VALUES ('14', '6899', 'loan disbursement', 'Cash', '', '', 'confirmed', 'Sacco Manager', '2015-05-03 12:16:26');
INSERT INTO `disbursement` VALUES ('15', '900', 'loan disbursement', 'MPESA', '', '', 'hjjewjnbbbdbj confirmed', 'Sacco Officer', '2015-05-03 12:22:19');

-- ----------------------------
-- Table structure for employed_personnel
-- ----------------------------
DROP TABLE IF EXISTS `employed_personnel`;
CREATE TABLE `employed_personnel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `doa` date NOT NULL,
  `employer` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `pfno` int(11) NOT NULL,
  `station` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `county` varchar(255) NOT NULL,
  `subcounty` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pfno` (`pfno`),
  UNIQUE KEY `member_no` (`member_no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employed_personnel
-- ----------------------------
INSERT INTO `employed_personnel` VALUES ('1', 'BWS/0034/2015', '2015-02-11', 'jkjkgkgj', 'gjgjghjgj', '2147483647', 'fhjff', '7857587', 'Kisii', 'bgjgg');
INSERT INTO `employed_personnel` VALUES ('2', 'BWS/0035/2015', '2015-05-01', 'hhkhkjhj', 'hjkhkhkjhjh', '76755', 'jhhjfjhfhf', 'fhfhfhff', 'Homa Bay', 'fhffh');
INSERT INTO `employed_personnel` VALUES ('3', 'BWS/0036/2015', '2015-05-01', 'jjhut', 'jjijoijj', '789798', 'jbjjggk', 'huuit', 'Kajiado', 'gjhgjgjh');
INSERT INTO `employed_personnel` VALUES ('4', 'BWS/0037/2015', '1998-05-12', 'Triecom', 'ICT', '758457', 'Kisumu', '739 Kisumu', 'Homa Bay', 'Nyando');

-- ----------------------------
-- Table structure for guarantors
-- ----------------------------
DROP TABLE IF EXISTS `guarantors`;
CREATE TABLE `guarantors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loanee_no` varchar(50) NOT NULL,
  `guarantor1` varchar(50) NOT NULL,
  `guarantor2` varchar(50) NOT NULL,
  `guarantor3` varchar(50) NOT NULL,
  `g_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of guarantors
-- ----------------------------
INSERT INTO `guarantors` VALUES ('31', '4546', 'Mang\'oli Martin', 'Emmanuel Muthui', 'Stella Sikhila', '2015-05-03 11:59:46');
INSERT INTO `guarantors` VALUES ('32', '6899', 'Emmanuel Muthui', 'Mang\'oli Martin', 'Stella Sikhila', '2015-05-03 12:16:03');
INSERT INTO `guarantors` VALUES ('33', '900', 'Mang\'oli Martin', 'Stella Sikhila', 'Emmanuel Muthui', '2015-05-03 12:21:49');

-- ----------------------------
-- Table structure for loans
-- ----------------------------
DROP TABLE IF EXISTS `loans`;
CREATE TABLE `loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loanee_no` varchar(50) NOT NULL,
  `app_date` datetime NOT NULL,
  `loan_type` varchar(50) NOT NULL,
  `amount_applied` double(11,2) NOT NULL,
  `processing_fee` double(11,2) NOT NULL,
  `amount_given` double(11,2) NOT NULL,
  `principal` double(11,2) NOT NULL,
  `installments` double(11,2) NOT NULL,
  `paid_installment` double(11,2) NOT NULL,
  `interest` double(11,2) NOT NULL,
  `overdue` double(11,2) NOT NULL,
  `penalty` double(11,2) NOT NULL,
  `total` double(11,2) NOT NULL,
  `new_total` double DEFAULT NULL,
  `loan_status` varchar(50) NOT NULL DEFAULT 'UNPAID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of loans
-- ----------------------------
INSERT INTO `loans` VALUES ('15', '6899', '2015-05-03 12:16:26', 'fees', '45000.00', '900.00', '44100.00', '45002.59', '15450.00', '0.00', '2.59', '0.00', '0.00', '45002.59', null, 'UNPAID');
INSERT INTO `loans` VALUES ('16', '900', '2015-05-03 12:22:19', 'development', '60000.00', '600.00', '59400.00', '60001.15', '4981.00', '0.00', '1.15', '0.00', '0.00', '60001.15', null, 'UNPAID');

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `sub_county` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of location
-- ----------------------------
INSERT INTO `location` VALUES ('1', '6899', 'Bungoma', 'Bumula');
INSERT INTO `location` VALUES ('2', '4546', 'Uasin Gishu', 'Kitale');
INSERT INTO `location` VALUES ('3', '900', 'Kitui', 'Mlango');

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `dor` datetime NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `phone_no1` varchar(50) NOT NULL,
  `phone_no2` varchar(50) DEFAULT NULL,
  `phone_no3` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `id_no` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `subcounty` varchar(50) NOT NULL,
  `shares` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_no` (`member_no`),
  UNIQUE KEY `phone_no1` (`phone_no1`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `address` (`address`),
  UNIQUE KEY `id_no` (`id_no`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('9', 'BWS/0001/2015', '0000-00-00 00:00:00', 'cgdgdhg', 'hgddhgdhgd', 'hgdhdhgd', '64647646', '476764764', '67467467', 'ste@gmail.com', '65765765', '0000-00-00', '565765', 'Kirinyage', 'kiminini', null);
INSERT INTO `members` VALUES ('11', 'BWS/0010/2015', '0000-00-00 00:00:00', 'ghjgg', 'ggjhgjhg', 'jhgjhgjhg', '4646476', '476476476', '4647646', 'stehjkh@gmail.com', '674764', '2015-02-03', '44653653', 'Kisii', 'mhujg', null);
INSERT INTO `members` VALUES ('12', 'BWS/0012/2015', '2015-02-07 12:28:01', 'mangoli', 'bbb', 'bbbb', '464565', '56576585', '55656758', 'stellavinmaris@gmail.com', '3543535', '2015-02-03', '43543543', 'Kisii', 'hjfjghk', null);
INSERT INTO `members` VALUES ('14', 'BWS/0013/2015', '2015-02-07 12:59:28', 'ghgjhhjg', 'fhjfhjfh', 'hfjhfjhf', '0718555832', '0718555838', '0718555830', 'stella.maris32@yahoo.com', '13324534', '2015-02-03', '5456454', 'Kilifi', 'vfjhgjh', null);
INSERT INTO `members` VALUES ('15', 'BWS/0015/2015', '2015-02-10 16:04:28', 'stella', 'nanjala', 'sikhilaa', '0718554834', '0718565836', '0718555657', 'stella.hmaris32@yahoo.com', '56757', '0000-00-00', '5465465', 'Kakamega', 'kimjg', null);
INSERT INTO `members` VALUES ('16', 'BWS/0016/2015', '2015-02-10 16:28:53', 'stellash', 'jbjhbj', 'bjkbkjbjbkj', '0718565832', '0718555843', '0718575838', 'stella.hmaris52@yahoo.com', '97970', '2015-02-09', '689689', 'Kirinyage', 'jnho', null);
INSERT INTO `members` VALUES ('17', 'BWS/0017/2015', '2015-02-14 08:55:44', 'Rhoda', 'Nyathira', 'kimanga', '0715768968', '0715768969', '0715768964', 'rk.nyra@gmail.com', '7686 thika', '2015-02-09', 'thika', 'Kirinyage', 'thika', '50');
INSERT INTO `members` VALUES ('18', 'BWS/0018/2015', '2015-02-14 09:02:59', 'emmanuel', 'musee', 'muthui', '0715768638', '0715768961', '0715768966', 'rk.nyra4@gmail.com', '79709 thika', '2015-02-10', '9089786', 'Kiambu', 'thika', '50');
INSERT INTO `members` VALUES ('19', 'BWS/0019/2015', '2015-02-14 09:22:26', 'emmanuel', 'musee', 'muthui', '0715768967', '0715768995', '0715768934', 'rk.nyra43@gmail.com', '56546 thika', '2015-02-04', '675', 'Kajiado', 'thika', null);
INSERT INTO `members` VALUES ('20', 'BWS/0020/2015', '2015-02-14 10:28:01', 'emmanuel', 'musee', 'muthui', '0718555765', '0718555769', '0718555764', 'rk.nyra49@gmail.com', '453657 kitale', '2015-02-09', '758575', 'Kilifi', 'thika', null);
INSERT INTO `members` VALUES ('21', 'BWS/0021/2015', '2015-02-14 11:06:49', 'hukhgkj', 'jgkjg', 'jkg', '0718556000', '0718556008', '0718556008', 'rk.nyra4y@gmail.com', '7578669', '2015-02-11', '86876876', 'Kirinyage', 'gtiut', null);
INSERT INTO `members` VALUES ('22', 'BWS/0022/2015', '2015-02-17 11:51:58', 'cyrus', 'mangoli', 'musee', '0718689064', '0718689069', '0718689066', 'ste3@gmail.com', '123 kiminini', '2015-02-09', '29399914', 'Kajiado', 'kiminini', null);
INSERT INTO `members` VALUES ('23', 'BWS/0023/2015', '2015-02-17 11:55:47', 'stella', 'Nyathira', 'kimanga', '0718689063', '0718689065', '0718689069', 'stehjkh5@gmail.com', '134 kitale', '2015-02-02', '7686786876', 'Kiambu', 'kwale', null);
INSERT INTO `members` VALUES ('24', 'BWS/0024/2015', '2015-03-02 20:06:37', 'jgkgkgjkgkg', 'gkgkkkk', 'gkgkjgjkgkgk', '0718555834', '0718555836', '0718555837', 'stella.hmaris53@yahoo.com', 'mnbnjbjbbj', '2015-03-01', '89078706986', 'Kirinyage', 'jhjklhlkhjkhk', null);
INSERT INTO `members` VALUES ('25', 'BWS/0025/2015', '2015-03-02 20:15:30', 'hnjhjkhk', 'hjkhkhkhk', 'khhkhj', '0757885367', '0757885364', '0757885368', 'stella.maris489@yahoo.com', 'gADDGSAFH', '2015-03-01', '232526643', 'Kiambu', 'WET664', null);
INSERT INTO `members` VALUES ('26', 'BWS/0026/2015', '2015-03-02 20:19:04', 'fgdhdf', 'jbkgjkgk', 'gkgkgkg', '0757885337', '0757885387', '0757835367', 'stella.maris424@yahoo.com', 'fdsgsdghhadh', '2015-03-01', '25476458689', 'Kericho', 'gsahgfsd', null);
INSERT INTO `members` VALUES ('27', 'BWS/0027/2015', '2015-03-06 14:13:46', 'ghjfjhhjj', 'FJHFJFJHFJF', 'JFJFJFJ', '0767562457', '0767562458', '0767562459', 'stella.maris499@yahoo.com', 'THIKA', '2015-03-03', '757653542', 'Kilifi', 'NJKH', null);
INSERT INTO `members` VALUES ('28', 'BWS/0028/2015', '2015-03-14 17:37:30', 'bevs', 'ifvy', 'ftugi', '45678908766', '5678976545678983', '', 'stellavinmaris4@gmail.com', 'hhuhkjhjkh', '2015-03-03', '35365365', 'Bomet', 'cnbcnccn', null);
INSERT INTO `members` VALUES ('29', 'BWS/0029/2015', '2015-04-30 15:16:59', 'Anneglady', 'kimanga', 'Gicheru', '0718675555', '0718675557', '0718675558', 'stellavinmaris434@gmail.com', '122 kitale', '1992-12-12', '09674321', 'Trans Nzoia', 'Kiminini', null);
INSERT INTO `members` VALUES ('30', 'BWS/0030/2015', '2015-04-30 16:05:45', 'Rhoda', 'kimanga', 'Gicheru', '0789898989', '0743433443', '0754545450', 'rk.nyr8a@gmail.com', '13324534 thika', '2015-04-15', '56576578', 'Kiambu', 'thika', null);
INSERT INTO `members` VALUES ('31', 'BWS/0031/2015', '2015-04-30 16:20:19', 'stella', 'Nyathira', 'Kimanga', '0778787878', '0732222332', '0790909090', 'stellavinmaris984@gmail.com', '2356 ktl', '2015-04-21', '8757676476', 'Taita Taveta', 'Kiminini', null);
INSERT INTO `members` VALUES ('32', 'BWS/0032/2015', '2015-04-30 22:19:17', 'nanjala', 'stella', 'sikhila', '0782676767', '0789653421', '0789653420', 'stellavinmaris4344@gmail.com', '122 nairobi', '2015-04-14', '86789685', 'Kiambu', 'Kiminini', null);
INSERT INTO `members` VALUES ('33', 'BWS/0033/2015', '2015-05-01 13:56:03', 'skhjkhh', 'hjhkhkhk', 'hkhkjhkhj', '0767676767', '0767676769', '0767676766', 'stellavinmaris98@gmail.com', '6756744', '2015-01-14', '575764', 'Kirinyage', 'nbjhgkjhlkj', null);
INSERT INTO `members` VALUES ('34', 'BWS/0034/2015', '2015-05-01 14:20:24', 'hkhkhh', 'hjkhjhjhkj', 'hkjhh', '0765568790', '0765568798', '0765568795', 'st090e@gmail.com', '7687686', '2015-02-20', '75643', 'Kwale', 'gjhggjg', null);
INSERT INTO `members` VALUES ('35', 'BWS/0035/2015', '2015-05-02 00:56:12', 'ugiugui', 'guigigig', 'igiuiugiugi', '0718555675', '0718555679', '0718555670', 'stellavinmaris0@gmail.com', '7567577', '2015-05-01', '673231111', 'Laikipia', 'lkhlkhl', null);
INSERT INTO `members` VALUES ('36', 'BWS/0036/2015', '2015-05-02 01:11:09', 'jgjkggjgjgjgg', 'gjhgjhgjhgjhg', 'gjhgjgjhg', '0718555861', '0718555862', '0718555863', 'stellavinmaris09@gmail.com', '56747644', '2015-05-01', '6747434', 'Kisumu', 'hjkhkjhkjh', null);
INSERT INTO `members` VALUES ('37', 'BWS/0037/2015', '2015-05-06 09:21:14', 'Kajwang', 'otis', 'Jose', '0732389741', '0732389742', '0732388741', 'lagat@gmail.com', '739 Kisumu', '1996-05-09', '4384783748', 'Kisumu', 'Nyando', null);
INSERT INTO `members` VALUES ('38', 'BWS/0038/2015', '2015-05-09 23:05:41', 'Kajwang', 'Kiproni', 'kipyegon', '6482399894', '4378749380490', '4378749380491', 'mangsmato@gmail.com', '32 Nairobi', '1999-02-17', '87950557688', 'Kilifi', 'Nyando', null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for next_of_kin
-- ----------------------------
DROP TABLE IF EXISTS `next_of_kin`;
CREATE TABLE `next_of_kin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `id_no` varchar(50) NOT NULL,
  `phone_no1` varchar(50) NOT NULL,
  `phone_no2` varchar(50) NOT NULL,
  `relationship` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_no` (`member_no`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of next_of_kin
-- ----------------------------
INSERT INTO `next_of_kin` VALUES ('1', 'BWS/0023/2015', 'stella', 'Nyathira', 'kimanga', '6786889', '0718689061', '0', 'friend');
INSERT INTO `next_of_kin` VALUES ('2', 'BWS/0034/2015', 'gjhghghg', 'hgjhgjgjh', 'ggjghjgjh', '76766453', '0718555568', '0', 'bgkg');
INSERT INTO `next_of_kin` VALUES ('5', 'BWS/0035/2015', 'hjgjgjgj', 'gjgjgjgjgjhg', 'jgjgjhgjhgjh', '45342344657', '0718555676', '0', 'hjhkjhkjg');
INSERT INTO `next_of_kin` VALUES ('7', 'BWS/0036/2015', 'yiuyuyiyiyi', 'iuyiuyiuyiyui', 'iuyiyiuyui', '867686768', '0718555866', '0', 'jhiiutitt');
INSERT INTO `next_of_kin` VALUES ('8', 'BWS/0037/2015', 'Mang\'oli', 'otis', 'Martin', '4384783748', '0734097612', '0', 'brother');

-- ----------------------------
-- Table structure for nominee
-- ----------------------------
DROP TABLE IF EXISTS `nominee`;
CREATE TABLE `nominee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `phone_no` int(50) NOT NULL,
  `dob` datetime NOT NULL,
  `id_no` int(50) NOT NULL,
  `relationship` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `subcounty` varchar(50) NOT NULL,
  `shares` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_no` (`id_no`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nominee
-- ----------------------------
INSERT INTO `nominee` VALUES ('38', 'BWS/0037/2015', 'kiprotich', 'Kiproni', 'pumpkin', '2147483647', '1998-05-15 00:00:00', '2147483647', 'brother', 'Kisumu', 'Nyando', '', '');

-- ----------------------------
-- Table structure for nominee1
-- ----------------------------
DROP TABLE IF EXISTS `nominee1`;
CREATE TABLE `nominee1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `phone_no` int(50) NOT NULL,
  `dob` datetime NOT NULL,
  `id_no` int(50) NOT NULL,
  `relationship` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `subcounty` varchar(50) NOT NULL,
  `shares` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_no` (`id_no`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nominee1
-- ----------------------------
INSERT INTO `nominee1` VALUES ('1', '', 'jkbnn', 'nknlknkln', 'nnn', '66876', '0000-00-00 00:00:00', '76786876', 'nbcgchg', 'Kisii', 'bgjl', '7987', '1');
INSERT INTO `nominee1` VALUES ('2', '', 'septon', 'hvhvhv', 'hvhjvjhv', '547645', '0000-00-00 00:00:00', '44644', '4gfhgchcg', 'kitui', 'ghfjuhc', '4654', '2');
INSERT INTO `nominee1` VALUES ('3', '', 'naomi', 'hjbjhbjhb', 'bjbkjbjk', '567565', '0000-00-00 00:00:00', '6565656', 'jhvjvhv', 'Kisumu', 'bggjguigu', '56', '3');
INSERT INTO `nominee1` VALUES ('4', '', 'fgfdyufuiguig', 'gfghfhgfh', 'hvhfjfjhf', '43345343', '0000-00-00 00:00:00', '4354756', 'fyjo', 'Kilifi', 'fguyhfg', '34', '1');
INSERT INTO `nominee1` VALUES ('6', '', 'ghgygiugi', 'hghgigiug', 'uigiugi', '0', '0000-00-00 00:00:00', '565678', 'fgdthfth', 'Kisii', 'hfuyhfjhfhju', '89', '2');
INSERT INTO `nominee1` VALUES ('9', '', 'dfjh', 'fhjdfhk', 'qjfhdbfj', '327873992', '0000-00-00 00:00:00', '2937891', 'edfvf', 'Kiambu', 'fdfdf', '434', '2');
INSERT INTO `nominee1` VALUES ('10', '', 'serkhhj', 'wekjkq', 'dkjdkq', '3020930', '0000-00-00 00:00:00', '38239', 'Father', 'Kirinyage', 'cefwee', '30', '3');
INSERT INTO `nominee1` VALUES ('14', '', 'kjlhklh', 'klhklhlkh', 'klhklhlkh', '2143657809', '2015-02-02 00:00:00', '898990', 'ytljkhkhkl', 'kitui', 'hfjhj', '8', '3');
INSERT INTO `nominee1` VALUES ('20', 'BWS/0036/2015', 'huiiuiu', 'uyuyuiyiyi', 'yiuyiuyu', '718555868', '2015-05-01 00:00:00', '718555870', 'hghghjh', 'Kajiado', 'jgjhghjgjhg', '', '');
INSERT INTO `nominee1` VALUES ('22', 'BWS/0036/2015', 'jggjg', 'ghgjgjhg', 'gjhgjgjhgjh', '718555900', '2015-05-01 00:00:00', '765445544', 'bjgjgj', 'Kajiado', 'jhhkjhkh', '', '');
INSERT INTO `nominee1` VALUES ('23', 'BWS/0036/2015', 'hmhjhj', 'jgjgjgjgj', 'gjgjgjhgjg', '718555909', '2015-05-01 00:00:00', '654323', 'vhhvhg', 'Kajiado', 'hvjhvhv', '', '');
INSERT INTO `nominee1` VALUES ('28', 'BWS/0037/2015', 'Kajwang', 'Kiproni', 'kipyegon', '2147483647', '2006-05-09 00:00:00', '2147483647', 'brother', 'Kisii', 'Nyando', '', '');

-- ----------------------------
-- Table structure for rates
-- ----------------------------
DROP TABLE IF EXISTS `rates`;
CREATE TABLE `rates` (
  `id` mediumint(9) NOT NULL,
  `type` varchar(50) NOT NULL,
  `period` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `penalty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rates
-- ----------------------------
INSERT INTO `rates` VALUES ('1', 'overdraft', '30', '10', '15');
INSERT INTO `rates` VALUES ('2', 'medical', '90', '3', '15');
INSERT INTO `rates` VALUES ('3', 'fees', '90', '3', '15');
INSERT INTO `rates` VALUES ('4', 'salary', '180', '2', '15');
INSERT INTO `rates` VALUES ('5', 'development', '365', '1', '15');

-- ----------------------------
-- Table structure for referees
-- ----------------------------
DROP TABLE IF EXISTS `referees`;
CREATE TABLE `referees` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) DEFAULT NULL,
  `loanee_no` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `id_no` int(11) NOT NULL,
  `relation` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `sub_county` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of referees
-- ----------------------------
INSERT INTO `referees` VALUES ('1', '4392879', '6899', 'Kamau', 'Njroge', '072367809812', '2015-02-10', '2147483647', 'brother', 'Bungoma', 'nhado');
INSERT INTO `referees` VALUES ('2', '39849', '6899', 'dshjh', 'sjh', '38480903676', '2015-02-16', '43090123', 'cxij', 'Kirinyage', 'ksjk');
INSERT INTO `referees` VALUES ('3', 'mhdu4984', '6899', 'Kajwang', 'Jose', '03988478', '2015-03-12', '4637874', 'bro', 'Elgeyo Marakwet', 'Gucha');
INSERT INTO `referees` VALUES ('4', 'mer', '6899', 'mageto', 'Dennis', '587439403', '2015-03-04', '4734898', 'cousin', 'Homa Bay', 'kerio');
INSERT INTO `referees` VALUES ('5', '', '4546', 'el', 'nando', '07164322145', '1996-05-23', '473648', 'siz', 'Bungoma', 'bumula');
INSERT INTO `referees` VALUES ('6', '584', '4546', 'eman', 'muse', '435473468', '1997-03-06', '548948493', 'bro', 'Kisii', 'marani');
INSERT INTO `referees` VALUES ('7', '', '900', 'Kajwang', 'Jose', '07236780912', '1997-02-07', '4637874', 'bro', 'Bungoma', 'Gucha');
INSERT INTO `referees` VALUES ('8', '', '900', 'mageto', 'Dennis', '07236781912', '1998-03-12', '4734898', 'cousin', 'Kajiado', 'kerio');
INSERT INTO `referees` VALUES ('9', '', '6899', 'Mang\'oli', 'Jose', '07236780912', '1997-04-17', '4637874', 'bro', 'Bungoma', 'bumula');
INSERT INTO `referees` VALUES ('10', '', '6899', 'mageto', 'Dennis', '07336780912', '1973-04-11', '4734898', 'cousin', 'Kakamega', 'marani');
INSERT INTO `referees` VALUES ('11', '', '6899', 'Kajwang', 'kipyegon', '0716432212', '1996-07-12', '4637874', 'bro', 'Baringo', 'Gucha');
INSERT INTO `referees` VALUES ('12', '', '6899', 'mageto', 'Dennis', '0716472212', '1996-04-09', '4734898', 'cousin', 'Kakamega', 'kerio');
INSERT INTO `referees` VALUES ('13', '', '4546', 'Kajwang', 'kipyegon', '0734097612', '1998-03-18', '4637874', 'bro', 'Bomet', 'Gucha');
INSERT INTO `referees` VALUES ('14', '', '4546', 'mageto', 'Dennis', '0737097612', '1997-05-08', '47348983', 'cousin', 'Kakamega', 'kerio');
INSERT INTO `referees` VALUES ('15', '', '4546', 'Mang\'oli', 'Martin', '0716432212', '1999-05-12', '4637874', 'bro', 'Bungoma', 'bumula');
INSERT INTO `referees` VALUES ('16', '', '4546', 'eman', 'muse', '0716432213', '1997-08-21', '4734898', 'cousin', 'kitui', 'marani');
INSERT INTO `referees` VALUES ('17', '', '6899', 'Kajwang', 'Jose', '0734097612', '1999-03-11', '4637874', 'bro', 'Bomet', 'bumula');
INSERT INTO `referees` VALUES ('18', '584', '6899', 'mageto', 'muse', '0734097613', '2015-03-19', '47348983', 'cousin', 'Kajiado', 'kerio');
INSERT INTO `referees` VALUES ('19', '', '4546', 'kiprotich', 'Martin', '0734097612', '1998-05-28', '4637874', 'bro', 'Busia', 'bumula');
INSERT INTO `referees` VALUES ('20', '', '4546', 'mageto', 'muse', '0734098612', '1997-05-20', '4734898', 'cousin', 'Kajiado', 'marani');
INSERT INTO `referees` VALUES ('21', '', '900', 'Kajwang', 'kipyegon', '07236780912', '1997-05-14', '4637874', 'bro', 'Bungoma', 'bumula');
INSERT INTO `referees` VALUES ('22', '', '900', 'mageto', 'Dennis', '07236781912', '1998-05-06', '4734898', 'cousin', 'Homa Bay', 'kerio');
INSERT INTO `referees` VALUES ('23', '', '4546', 'Kajwang', 'kipyegon', '0716432212', '1999-05-21', '4637874', 'bro', 'Bomet', 'bumula');
INSERT INTO `referees` VALUES ('24', '', '4546', 'mageto', 'Dennis', '0716432212', '1997-05-08', '4734898', 'cousin', 'Isiolo', 'kerio');
INSERT INTO `referees` VALUES ('25', 'mhdu4984', '6899', 'Kajwang', 'Jose', '07236780912', '1999-05-06', '4637874', 'bro', 'Bomet', 'bumula');
INSERT INTO `referees` VALUES ('26', '584', '6899', 'mageto', 'Dennis', '07436780912', '1997-05-01', '4734898', 'cousin', 'Garissa', 'kerio');
INSERT INTO `referees` VALUES ('27', '', '900', 'Kajwang', 'Martin', '07236780912', '1996-05-16', '4637874', 'bro', 'Elgeyo Marakwet', 'bumula');
INSERT INTO `referees` VALUES ('28', '584', '900', 'mageto', 'Dennis', '0726780912', '1999-06-10', '4734898', 'cousin', 'Kakamega', 'marani');
INSERT INTO `referees` VALUES ('29', '', 'BWS/0037/2015', 'Mang\'oli', 'kipyegon', '071643224', '1996-05-09', '4637874', 'bro', 'Busia', 'bumula');
INSERT INTO `referees` VALUES ('30', '', 'BWS/0037/2015', 'mageto', 'Dennis', '071643222', '1996-03-13', '4734898', 'cousin', 'Busia', 'marani');

-- ----------------------------
-- Table structure for repayment
-- ----------------------------
DROP TABLE IF EXISTS `repayment`;
CREATE TABLE `repayment` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `loanee_no` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `payment_method` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `loan_type` varchar(50) NOT NULL,
  `balance` int(11) NOT NULL,
  `loan_status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of repayment
-- ----------------------------
INSERT INTO `repayment` VALUES ('1', '9000', '2015-02-04 15:24:43', 'M-Pesa', '50000', 'Emergecy', '0', 'paid');
INSERT INTO `repayment` VALUES ('2', '4000', '2015-02-10 15:58:41', 'M-Pesa', '90000', 'fee', '45000', 'incomplete');
INSERT INTO `repayment` VALUES ('3', '9000', '2015-02-17 10:07:02', 'M-Pesa', '40000', 'fee', '-40000', 'PAID');
INSERT INTO `repayment` VALUES ('4', '9000', '2015-02-17 10:14:02', 'Bank', '30000', 'Emergecy', '-30000', 'PAID');
INSERT INTO `repayment` VALUES ('5', '9000', '2015-02-17 10:21:02', 'M-Pesa', '12000', 'fee', '-12000', 'PAID');
INSERT INTO `repayment` VALUES ('6', '9000', '2015-02-17 11:47:02', 'Bank', '5623', 'Emergecy', '-5623', 'PAID');
INSERT INTO `repayment` VALUES ('7', '4000', '2015-02-25 09:22:02', 'Bank', '40000', 'fee', '5000', 'INCOMPLETE');
INSERT INTO `repayment` VALUES ('8', '4000', '2015-02-25 20:48:02', 'M-Pesa', '5000', 'Emergecy', '40000', 'INCOMPLETE');
INSERT INTO `repayment` VALUES ('9', '4000', '2015-02-25 20:54:02', 'Bank', '5000', 'Emergecy', '40000', 'INCOMPLETE');
INSERT INTO `repayment` VALUES ('10', '4000', '2015-02-25 20:54:02', 'M-Pesa', '5000', 'Emergecy', '40000', 'INCOMPLETE');
INSERT INTO `repayment` VALUES ('11', '4000', '2015-02-25 20:54:02', 'Bank', '5000', 'fee', '40000', 'INCOMPLETE');
INSERT INTO `repayment` VALUES ('12', '4000', '2015-02-25 20:55:02', 'M-Pesa', '500', 'fee', '44500', 'INCOMPLETE');
INSERT INTO `repayment` VALUES ('13', '0', '2015-05-03 23:06:05', '', '5000', '0', '-5000', 'COMPLETE');
INSERT INTO `repayment` VALUES ('16', '4000', '2015-05-03 23:18:05', '', '2300', 'School', '-2300', 'COMPLETE');
INSERT INTO `repayment` VALUES ('18', '9000', '2015-05-03 23:31:05', '', '7700', 'Emergency', '-7700', 'COMPLETE');
INSERT INTO `repayment` VALUES ('19', '4000', '2015-05-03 23:36:05', '', '1452', 'School', '-1452', 'COMPLETE');
INSERT INTO `repayment` VALUES ('20', '4000', '2015-05-03 23:38:05', '', '1452', 'School', '-1452', 'COMPLETE');
INSERT INTO `repayment` VALUES ('21', '4000', '2015-05-03 23:43:05', '', '500', 'School', '-1952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('22', '4000', '2015-05-04 00:03:05', '', '1000', 'School', '-2952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('23', '4000', '2015-05-04 00:04:05', '', '1000', 'School', '-3952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('24', '4000', '2015-05-04 00:05:05', '', '1000', 'School', '-4952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('25', '4000', '2015-05-04 00:06:05', '', '1000', 'School', '-5952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('26', '4000', '2015-05-04 00:07:05', '', '1000', 'School', '-6952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('27', '4000', '2015-05-04 00:08:05', '', '1000', 'School', '-7952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('28', '4000', '2015-05-04 00:14:05', '', '1000', 'School', '-8952', 'COMPLETE');
INSERT INTO `repayment` VALUES ('29', '4000', '2015-05-04 00:16:05', '', '1000', 'School', '-9952', 'COMPLETE');

-- ----------------------------
-- Table structure for self_employed_persons
-- ----------------------------
DROP TABLE IF EXISTS `self_employed_persons`;
CREATE TABLE `self_employed_persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_no` varchar(50) NOT NULL,
  `tob` varchar(50) NOT NULL,
  `startdate` date NOT NULL,
  `phone` int(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL,
  `subcounty` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of self_employed_persons
-- ----------------------------
INSERT INTO `self_employed_persons` VALUES ('1', '', 'dyufghijkolp', '0000-00-00', '0', 'bjblblkbnklnb', 'Kajiado', 'h;j[pkip[kp');
INSERT INTO `self_employed_persons` VALUES ('2', '', 'xreztyutgcuyuh', '0000-00-00', '2147483647', 'sghfjkjkljlk', 'Kericho', 'ggiojj');
INSERT INTO `self_employed_persons` VALUES ('3', '', 'fdfgdfdh', '0000-00-00', '0', 'nbjhb', 'Kisii', 'hgghkj');
INSERT INTO `self_employed_persons` VALUES ('4', '', 'gfhgfhgf', '2003-02-15', '2147483647', '6556756', 'Kisumu', 'hgyf');
INSERT INTO `self_employed_persons` VALUES ('5', 'BWS/0015/2015', 'bodaboda', '2002-09-15', '718555843', '5758', 'Kirinyage', 'hyio');

-- ----------------------------
-- Table structure for shares
-- ----------------------------
DROP TABLE IF EXISTS `shares`;
CREATE TABLE `shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `members_no` varchar(50) DEFAULT NULL,
  `shares` int(11) NOT NULL,
  `total` int(100) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `members_no` (`members_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shares
-- ----------------------------

-- ----------------------------
-- Table structure for witness
-- ----------------------------
DROP TABLE IF EXISTS `witness`;
CREATE TABLE `witness` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `wit_no` varchar(50) NOT NULL,
  `loanee_no` varchar(50) NOT NULL,
  `w_name` varchar(50) NOT NULL,
  `w_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of witness
-- ----------------------------
INSERT INTO `witness` VALUES ('14', '900', '6899', 'Emmanuel Muthui', '2015-05-03 12:16:03');
INSERT INTO `witness` VALUES ('15', '6899', '900', 'Mang\'oli Martin', '2015-05-03 12:21:49');

-- ----------------------------
-- Event structure for intr_event
-- ----------------------------
DROP EVENT IF EXISTS `intr_event`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `intr_event` ON SCHEDULE EVERY 1 SECOND STARTS '2015-04-30 20:44:00' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
  -- CALL proc_interest();
	UPDATE loans l
 	 INNER JOIN rates r ON l.loan_type = r.type
		SET l.interest = l.interest+l.principal*r.rate/259200000;
UPDATE loans l SET l.principal=l.amount_applied+l.interest; 
UPDATE loans l SET l.total=l.principal+l.penalty;	
END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for pen_event
-- ----------------------------
DROP EVENT IF EXISTS `pen_event`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `pen_event` ON SCHEDULE EVERY 1 SECOND STARTS '2015-04-30 17:18:00' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
  UPDATE loans l
	INNER JOIN rates r ON l.loan_type = r.type
	SET l.overdue = DATEDIFF(NOW(),l.app_date)-r.period, l.penalty = l.interest*r.penalty/8640000
	WHERE r.period < DATEDIFF(NOW(),l.app_date);
END
;;
DELIMITER ;
